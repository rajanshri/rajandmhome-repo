<?php
/*
Template Name: Front Page
*/
get_header();
?>
<!--<script type="text/javascript">
//homepage slider functions
function slideone() {
	
	// Appear
	setTimeout("jQuery('#slide1-card').animate({ 'left': '10px' }, 250, 'easeOutCubic' );", 50);
	setTimeout("jQuery('#slide1-stats').animate({ 'height': '200px', 'top': '75px', 'opacity': 'show' }, 250, 'easeOutCubic' );", 200);
	
	// Disappear
	setTimeout("jQuery('#slide1-card').animate({ 'left': '-550px' }, 250, 'easeOutCubic' );", 3500);
	setTimeout("jQuery('#slide1-stats').animate({ 'height': '0', 'top': '175px', 'opacity': 'hide' }, 250, 'easeOutCubic' );", 3500);
}

function slidetwo() {
	// Appear
	setTimeout("jQuery('#slide2-card').animate({ 'right': '10px' }, 250, 'easeOutCubic' );", 50);
	setTimeout("jQuery('#slide2-stats').animate({ 'height': '200px', 'top': '75px', 'opacity': 'show' }, 250, 'easeOutCubic' );", 200);
	
	// Disappear
	setTimeout("jQuery('#slide2-card').animate({ 'right': '-550px' }, 250, 'easeOutCubic' );", 3500);
	setTimeout("jQuery('#slide2-stats').animate({ 'height': '0', 'top': '175px', 'opacity': 'hide' }, 250, 'easeOutCubic' );", 3500);
	
}

function slidethree() {
	// Appear
	setTimeout("jQuery('#slide3-card').animate({ 'left': '10px' }, 250, 'easeOutCubic' );", 50);
	setTimeout("jQuery('#slide3-stats').animate({ 'height': '200px', 'top': '75px', 'opacity': 'show' }, 250, 'easeOutCubic' );", 200);
	
	// Disappear
	setTimeout("jQuery('#slide3-card').animate({ 'left': '-550px' }, 250, 'easeOutCubic' );", 3500);
	setTimeout("jQuery('#slide3-stats').animate({ 'height': '0', 'top': '175px', 'opacity': 'hide' }, 250, 'easeOutCubic' );", 3500);
	
}

function slidefour() {
	// Appear
	setTimeout("jQuery('#slide4-card').animate({ 'right': '10px' }, 250, 'easeOutCubic' );", 50);
	setTimeout("jQuery('#slide4-stats').animate({ 'height': '200px', 'top': '75px', 'opacity': 'show' }, 250, 'easeOutCubic' );", 200);
	
	// Disappear
	setTimeout("jQuery('#slide4-card').animate({ 'right': '-550px' }, 250, 'easeOutCubic' );", 3500);
	setTimeout("jQuery('#slide4-stats').animate({ 'height': '0', 'top': '175px', 'opacity': 'hide' }, 250, 'easeOutCubic' );", 3500);
}

function slidefive() {
	// Appear
	setTimeout("jQuery('#slide5-card').animate({ 'right': '15px' }, 250, 'easeOutCubic' );", 50);
	setTimeout("jQuery('#slide5-stats').animate({ 'height': '200px', 'top': '75px', 'opacity': 'show' }, 250, 'easeOutCubic' );", 200);
}

function slidefivetwo() {
	jQuery('#shade').animate({ 'opacity': 'show' }, 500);
	jQuery('#slider-logo').animate({ 'bottom': '300px', 'left': '176px', 'height': '64px', 'width': '319px', 'opacity': 'hide' }, 500);
	jQuery('#slider-logo-alt').animate({ 'bottom': '300px', 'left': '176px', 'height': '64px', 'width': '319px', 'opacity': 'show' }, 500);
	jQuery('#slider-logo-wrap').animate({ 'bottom': '205px', 'left': '75px', 'height': '227px', 'width': '520px', 'opacity': 'show' }, 500);
	
	jQuery('.disappear').animate({ 'opacity': 'hide' }, 50);
	jQuery('#calcLink').animate({ 'bottom': '175px', 'padding-top': '12px', 'padding-bottom': '12px', 'padding-left': '12px', 'padding-right': '12px', 'font-size': '21px', 'width': '250px', 'left': '35px' }, 500).removeClass('animation');
	jQuery('#galleryLink').animate({ 'bottom': '175px', 'padding-top': '12px', 'padding-bottom': '12px', 'padding-left': '12px', 'padding-right': '12px', 'font-size': '21px', 'width': '250px', 'right': '35px' }, 500).removeClass('animation');
	
	jQuery('#contactLink').animate({ 'opacity': 'show', 'padding-top': '12px', 'padding-bottom': '12px', 'padding-left': '12px', 'padding-right': '12px', 'font-size': '21px', 'width': '250px', 'left': '35px', 'bottom': '110px' }, 500);
	jQuery('#pricingLink').animate({ 'opacity': 'show', 'padding-top': '12px', 'padding-bottom': '12px', 'padding-left': '12px', 'padding-right': '12px', 'font-size': '21px', 'width': '250px', 'right': '35px', 'bottom': '110px' }, 500);
	jQuery('#sliderPhone').animate({ 'opacity': 'show' }, 500);
	
}

function sideSendSubmit( formData, jqForm, options ) {

	console.log(formData);
	console.log(jqForm);
	console.log(options);
	
	// Honeypot spam filter
	if( jQuery('.sideContact input.sticky').is(':checked') ) {
		alert('Are you a robot? I think so... ');
		return false;
	}
	
	jQuery('#sideSendResults').removeClass('error').addClass('loading').animate({ 'opacity': 'show', 'height': 'show' }).html('Loading...');
	
	var valErrors = new Array();
	
	if( jQuery('.sideContact .practiceName').val() == '' || jQuery('.sideContact .practiceName').val() == 'Enter practice name' ) {
		valErrors.push('Practice Name');
		jQuery('.sideContact .practiceName').addClass('error');
	}
	if( 
		jQuery('.sideContact .firstName').val() == '' || 
		jQuery('.sideContact .firstName').val() == 'Enter first' ||
		jQuery('.sideContact .lastName').val() == '' || 
		jQuery('.sideContact .lastName').val() == 'and last name'
	) {
		valErrors.push('First and Last Name');
		jQuery('.sideContact .firstName').addClass('error');
		jQuery('.sideContact .lastName').addClass('error');
	}
	if( 
		jQuery('.sideContact .phoneArea').val() == '' || 
		jQuery('.sideContact .phoneArea').val() == 'XXX' ||
		jQuery('.sideContact .phonePrefix').val() == '' ||
		jQuery('.sideContact .phonePrefix').val() == 'XXX' ||
		jQuery('.sideContact .phoneExchange').val() == '' ||
		jQuery('.sideContact .phoneExchange').val() == 'XXXX'
	) {
		valErrors.push('Phone Number');
		jQuery('.sideContact .phoneArea, .sideContact .phonePrefix, .sideContact .phoneExchange').addClass('error');
	}
	if( jQuery('.sideContact .phoneArea').val().length != 3 ||
		jQuery('.sideContact .phonePrefix').val().length != 3 ||
		jQuery('.sideContact .phoneExchange').val().length != 4 
	
	) {
		valErrors.push('Phone Number (Check Length)');
		jQuery('.sideContact .phoneArea, .sideContact .phonePrefix, .sideContact .phoneExchange').addClass('error');
	}
	if( jQuery('.sideContact .emailAddress').val() == '' || 
		jQuery('.sideContact .emailAddress').val() == 'Enter email address'
	) {
		valErrors.push('Email Address');
		jQuery('.sideContact .emailAddress').addClass('error');
	}
	var emailAddress = jQuery('.sideContact .emailAddress').val();
	if( !emailReg.test(emailAddress) ) {
		valErrors.push('Email Address (Appears Invalid)');
		jQuery('.sideContact .emailAddress').addClass('error');
	}
	if( jQuery('.sideContact input.reserve').is(':checked') ) {
		if( jQuery('.sideContact input.street').val() == '' ||
			jQuery('.sideContact input.street').val() == 'Enter street address'
		) {
			valErrors.push('Street Address');
			jQuery('.sideContact input.street').addClass('error');
		}
		if( jQuery('.sideContact input.city').val() == '' ||
			jQuery('.sideContact input.city').val() == 'Enter city'
		) {
			valErrors.push('City');
			jQuery('.sideContact input.city').addClass('error');
		}
		if( jQuery('.sideContact input.zip').val() == '' ||
			jQuery('.sideContact input.zip').val() == 'Enter zip'
		) {
			valErrors.push('Zip');
			jQuery('.sideContact input.zip').addClass('error');
		}
	}
	
	if( valErrors.length > 0 ) {
		var valMessage = 'Please fill out the following required fields: ';
		if( valErrors.length > 1 ) {
			for( i = 0; i < valErrors.length; i++ ) {
				if( i < ( valErrors.length - 1 ) ) {
					valMessage += valErrors[i] + ', ';
				} else {
					valMessage += 'and ' + valErrors[i] + '.';
				}
			}
		} else {
			valMessage += valErrors[0];
		}
		jQuery('#sideSendResults').removeClass('loading').addClass('error').html(valMessage);
		console.info('errorMessage',valMessage);
		
		_gaq.push(['_trackPageview', '/virtual/side-form/validation-errors']);
		
		return false;
	} else {
		jQuery('.sideContact input.submit').attr('disabled','disabled').addClass('disabled');
		console.log('Sidebar form submitted successfully');
	}
	
}

function sideSendResponse( data ) {

	console.info('data',data);

	jQuery('.sideContact input.submit').removeAttr('disabled').removeClass('disabled');

	_gaq.push(['_trackPageview', '/virtual/side-form/success']);
	
	jQuery('#sideSendResults').removeClass('loading').addClass(data.status).html(data.message);
	setTimeout(function(){
		jQuery('#sideSendResults').animate({ 'height': 'hide', 'opacity': 'hide' }, 
			function(){
				jQuery(this).removeClass('success').html('Loading...');
			}
		);
	}, 4000);
	
}
jQuery(window).load(function(){

	//execute slides
	if( jQuery('#featured #slider').is('*') ) {
	
		slideone();
	
		setTimeout("slidetwo();", 4000);
		
		setTimeout("slidethree();", 8500);
		
		setTimeout("slidefour();", 13000);
		
		setTimeout("slidefive();", 18000);
		
		setTimeout("slidefivetwo();", 21500);
		
	}
	
});

jQuery(document).ready(function($){

	// Slider CTA button movement
	$('#slider a').hover(
		function(){
			var bottom = parseInt( $(this).css('bottom') );
			
			var lower = bottom - 1;
			
			$(this).css({ 'bottom': lower });
			
		},
		function(){
			var lower = parseInt( $(this).css('bottom') );
			
			var bottom = lower + 1;
			
			$(this).css({ 'bottom': bottom });
		}
	);
	$('#slider a').mousedown(function(){
		var lower = parseInt( $(this).css('bottom') );
		
		var active = lower - 1;
		
		$(this).css({ 'bottom': active });
	});
	$('#slider a').mouseup(function(){
		var active = parseInt( $(this).css('bottom') );
		
		var lower = active + 1;
		
		$(this).css({ 'bottom': lower });
	});

	// Free Consultation Form Defaults
	var sideDefaults = [
		'Enter practice name',
		'Enter first',
		'and last name',
		'XXX',
		'XXXX',
		'Enter email address',
		'Enter Comments/Questions (optional)',
		'Enter street address',
		'Enter street address (cont.)',
		'Enter suite number',
		'Enter city',
		'Enter zip'
	];
	$('.sideContact').deloClear({
		fieldDef: sideDefaults,
		preColor: '#CECECE'
	});
	
	// Free Consultaion request form from sidebar
	var sideOptions = {
		dataType: 'json',
		beforeSubmit: sideSendSubmit,
		success: sideSendResponse
	};
	
	$('.sideContact form').ajaxForm(sideOptions);
	
	$('input.reserve').change(function(){
	
		if($(this).is(':checked')) {
			$('.reserve-show').slideDown();
		} else {
			$('.reserve-show').slideUp();
		}
		
	});
	
	// Homepage GA events
	$('#calcLink').click(function(){
		if( $(this).hasClass('animation') ) {
			_gaq.push(['_trackEvent', 'Clicks', 'Homepage', 'Intro Video - During animation']);
		} else {
			_gaq.push(['_trackEvent', 'Clicks', 'Homepage', 'Intro Video - After animation']);
		}
	});
	$('#galleryLink').click(function(){
		if( $(this).hasClass('animation') ) {
			_gaq.push(['_trackEvent', 'Clicks', 'Homepage', 'Postcard Gallery - During animation']);
		} else {
			_gaq.push(['_trackEvent', 'Clicks', 'Homepage', 'Postcard Gallery - After animation']);
		}
	});
	$('#contactLink').click(function(){
		_gaq.push(['_trackEvent', 'Clicks', 'Homepage', 'ROI Calculator']);
	});
	$('#pricingLink').click(function(){
		_gaq.push(['_trackEvent', 'Clicks', 'Homepage', 'Postcard Pricing']);
	});

});
</script>-->
<!----><script type="text/javascript">
//homepage slider functions
function slideone() {
	
	// Appear
	setTimeout("jQuery('#slide1-card').animate({ 'left': '10px' }, 250, 'easeOutCubic' );", 50);
	setTimeout("jQuery('#slide1-stats').animate({ 'height': '200px', 'top': '75px', 'opacity': 'show' }, 250, 'easeOutCubic' );", 200);
	
	// Disappear
	setTimeout("jQuery('#slide1-card').animate({ 'left': '-550px' }, 250, 'easeOutCubic' );", 3500);
	setTimeout("jQuery('#slide1-stats').animate({ 'height': '0', 'top': '175px', 'opacity': 'hide' }, 250, 'easeOutCubic' );", 3500);
}

function slidetwo() {
	// Appear
	setTimeout("jQuery('#slide2-card').animate({ 'right': '10px' }, 250, 'easeOutCubic' );", 50);
	setTimeout("jQuery('#slide2-stats').animate({ 'height': '200px', 'top': '75px', 'opacity': 'show' }, 250, 'easeOutCubic' );", 200);
	
	// Disappear
	setTimeout("jQuery('#slide2-card').animate({ 'right': '-550px' }, 250, 'easeOutCubic' );", 3500);
	setTimeout("jQuery('#slide2-stats').animate({ 'height': '0', 'top': '175px', 'opacity': 'hide' }, 250, 'easeOutCubic' );", 3500);
	
}

function slidethree() {
	// Appear
	setTimeout("jQuery('#slide3-card').animate({ 'left': '10px' }, 250, 'easeOutCubic' );", 50);
	setTimeout("jQuery('#slide3-stats').animate({ 'height': '200px', 'top': '75px', 'opacity': 'show' }, 250, 'easeOutCubic' );", 200);
	
	// Disappear
	setTimeout("jQuery('#slide3-card').animate({ 'left': '-550px' }, 250, 'easeOutCubic' );", 3500);
	setTimeout("jQuery('#slide3-stats').animate({ 'height': '0', 'top': '175px', 'opacity': 'hide' }, 250, 'easeOutCubic' );", 3500);
	
}

function slidefour() {
	// Appear
	setTimeout("jQuery('#slide4-card').animate({ 'right': '10px' }, 250, 'easeOutCubic' );", 50);
	setTimeout("jQuery('#slide4-stats').animate({ 'height': '200px', 'top': '75px', 'opacity': 'show' }, 250, 'easeOutCubic' );", 200);
	
	// Disappear
	setTimeout("jQuery('#slide4-card').animate({ 'right': '-550px' }, 250, 'easeOutCubic' );", 3500);
	setTimeout("jQuery('#slide4-stats').animate({ 'height': '0', 'top': '175px', 'opacity': 'hide' }, 250, 'easeOutCubic' );", 3500);
}

function slidefive() {
	// Appear
	setTimeout("jQuery('#slide5-card').animate({ 'right': '15px' }, 250, 'easeOutCubic' );", 50);
	setTimeout("jQuery('#slide5-stats').animate({ 'height': '200px', 'top': '75px', 'opacity': 'show' }, 250, 'easeOutCubic' );", 200);
}

function slidefivetwo() {
	jQuery('#shade').animate({ 'opacity': 'show' }, 500);
	jQuery('#slider-logo').animate({ 'bottom': '300px', 'left': '176px', 'height': '64px', 'width': '319px', 'opacity': 'hide' }, 500);
	jQuery('#slider-logo-alt').animate({ 'bottom': '300px', 'left': '176px', 'height': '64px', 'width': '319px', 'opacity': 'show' }, 500);
	jQuery('#slider-logo-wrap').animate({ 'bottom': '205px', 'left': '75px', 'height': '227px', 'width': '520px', 'opacity': 'show' }, 500);
	
	jQuery('.disappear').animate({ 'opacity': 'hide' }, 50);
	jQuery('#calcLink').animate({ 'bottom': '175px', 'padding-top': '12px', 'padding-bottom': '12px', 'padding-left': '12px', 'padding-right': '12px', 'font-size': '21px', 'width': '250px', 'left': '35px' }, 500).removeClass('animation');
	jQuery('#galleryLink').animate({ 'bottom': '175px', 'padding-top': '12px', 'padding-bottom': '12px', 'padding-left': '12px', 'padding-right': '12px', 'font-size': '21px', 'width': '250px', 'right': '35px' }, 500).removeClass('animation');
	
	jQuery('#contactLink').animate({ 'opacity': 'show', 'padding-top': '12px', 'padding-bottom': '12px', 'padding-left': '12px', 'padding-right': '12px', 'font-size': '21px', 'width': '250px', 'left': '35px', 'bottom': '110px' }, 500);
	jQuery('#pricingLink').animate({ 'opacity': 'show', 'padding-top': '12px', 'padding-bottom': '12px', 'padding-left': '12px', 'padding-right': '12px', 'font-size': '21px', 'width': '250px', 'right': '35px', 'bottom': '110px' }, 500);
	jQuery('#sliderPhone').animate({ 'opacity': 'show' }, 500);
	
}

function sideSendSubmit( formData, jqForm, options ) {

	console.log(formData);
	console.log(jqForm);
	console.log(options);

	// Simple math filter
	if( jQuery('#sideValidate').val() != (formnumber.x + formnumber.z) ) {
		alert('Looks like you might be a robot, in which case, my Shaolin Temple style defeat your Monkey style. And if you\'re not a robot, just try the math problem again.');
		jQuery('#sideValidate').addClass('check-error');
		return false;
	}
	
	// Honeypot spam filter
	if( jQuery('.sideContact input.sticky').is(':checked') ) {
		alert('Are you a robot? I think so... ');
		return false;
	}
	
	jQuery('#sideSendResults').removeClass('error').addClass('loading').animate({ 'opacity': 'show', 'height': 'show' }).html('Loading...');
	
	var valErrors = new Array();
	
	if( jQuery('.sideContact .practiceName').val() == '' || jQuery('.sideContact .practiceName').val() == 'Enter practice name' ) {
		valErrors.push('Practice Name');
		jQuery('.sideContact .practiceName').addClass('error');
	}
	if( 
		jQuery('.sideContact .firstName').val() == '' || 
		jQuery('.sideContact .firstName').val() == 'Enter first' ||
		jQuery('.sideContact .lastName').val() == '' || 
		jQuery('.sideContact .lastName').val() == 'and last name'
	) {
		valErrors.push('First and Last Name');
		jQuery('.sideContact .firstName').addClass('error');
		jQuery('.sideContact .lastName').addClass('error');
	}
	if( 
		jQuery('.sideContact .phoneArea').val() == '' || 
		jQuery('.sideContact .phoneArea').val() == 'XXX' ||
		jQuery('.sideContact .phonePrefix').val() == '' ||
		jQuery('.sideContact .phonePrefix').val() == 'XXX' ||
		jQuery('.sideContact .phoneExchange').val() == '' ||
		jQuery('.sideContact .phoneExchange').val() == 'XXXX'
	) {
		valErrors.push('Phone Number');
		jQuery('.sideContact .phoneArea, .sideContact .phonePrefix, .sideContact .phoneExchange').addClass('error');
	}
	if( jQuery('.sideContact .phoneArea').val().length != 3 ||
		jQuery('.sideContact .phonePrefix').val().length != 3 ||
		jQuery('.sideContact .phoneExchange').val().length != 4 
	
	) {
		valErrors.push('Phone Number (Check Length)');
		jQuery('.sideContact .phoneArea, .sideContact .phonePrefix, .sideContact .phoneExchange').addClass('error');
	}
	if( jQuery('.sideContact .emailAddress').val() == '' || 
		jQuery('.sideContact .emailAddress').val() == 'Enter email address'
	) {
		valErrors.push('Email Address');
		jQuery('.sideContact .emailAddress').addClass('error');
	}
	var emailAddress = jQuery('.sideContact .emailAddress').val();
	if( !emailReg.test(emailAddress) ) {
		valErrors.push('Email Address (Appears Invalid)');
		jQuery('.sideContact .emailAddress').addClass('error');
	}
	if( jQuery('.sideContact input.reserve').is(':checked') ) {
		if( jQuery('.sideContact input.street').val() == '' ||
			jQuery('.sideContact input.street').val() == 'Enter street address'
		) {
			valErrors.push('Street Address');
			jQuery('.sideContact input.street').addClass('error');
		}
		if( jQuery('.sideContact input.city').val() == '' ||
			jQuery('.sideContact input.city').val() == 'Enter city'
		) {
			valErrors.push('City');
			jQuery('.sideContact input.city').addClass('error');
		}
		if( jQuery('.sideContact input.zip').val() == '' ||
			jQuery('.sideContact input.zip').val() == 'Enter zip'
		) {
			valErrors.push('Zip');
			jQuery('.sideContact input.zip').addClass('error');
		}
	}
	
	if( valErrors.length > 0 ) {
		var valMessage = 'Please fill out the following required fields: ';
		if( valErrors.length > 1 ) {
			for( i = 0; i < valErrors.length; i++ ) {
				if( i < ( valErrors.length - 1 ) ) {
					valMessage += valErrors[i] + ', ';
				} else {
					valMessage += 'and ' + valErrors[i] + '.';
				}
			}
		} else {
			valMessage += valErrors[0];
		}
		jQuery('#sideSendResults').removeClass('loading').addClass('error').html(valMessage);
		
		_gaq.push(['_trackPageview', '/virtual/side-form/validation-errors']);
		
		return false;
	} else {
	}
	
}

function sideSendResponse( data ) {

	console.info('data',data);

	_gaq.push(['_trackPageview', '/virtual/side-form/success']);
	
	jQuery('#sideSendResults').removeClass('loading').addClass(data.status).html(data.message);
	setTimeout(function(){
		jQuery('#sideSendResults').animate({ 'height': 'hide', 'opacity': 'hide' }, 
			function(){
				jQuery(this).removeClass('success').html('Loading...');
			}
		);
	}, 4000);
	
}
jQuery(window).load(function(){

	//execute slides
	if( jQuery('#featured #slider').is('*') ) {
	
		slideone();
	
		setTimeout("slidetwo();", 4000);
		
		setTimeout("slidethree();", 8500);
		
		setTimeout("slidefour();", 13000);
		
		setTimeout("slidefive();", 18000);
		
		setTimeout("slidefivetwo();", 21500);
		
	}
	
});

jQuery(document).ready(function($){

	// Slider CTA button movement
	$('#slider a').hover(
		function(){
			var bottom = parseInt( $(this).css('bottom') );
			
			var lower = bottom - 1;
			
			$(this).css({ 'bottom': lower });
			
		},
		function(){
			var lower = parseInt( $(this).css('bottom') );
			
			var bottom = lower + 1;
			
			$(this).css({ 'bottom': bottom });
		}
	);
	$('#slider a').mousedown(function(){
		var lower = parseInt( $(this).css('bottom') );
		
		var active = lower - 1;
		
		$(this).css({ 'bottom': active });
	});
	$('#slider a').mouseup(function(){
		var active = parseInt( $(this).css('bottom') );
		
		var lower = active + 1;
		
		$(this).css({ 'bottom': lower });
	});

	// Free Consultation Form Defaults
	var sideDefaults = [
		'Enter practice name',
		'Enter first',
		'and last name',
		'XXX',
		'XXXX',
		'Enter email address',
		'Enter Comments/Questions (optional)',
		'Enter street address',
		'Enter street address (cont.)',
		'Enter suite number',
		'Enter city',
		'Enter zip'
	];
	$('.sideContact').deloClear({
		fieldDef: sideDefaults,
		preColor: '#CECECE'
	});
	
	// Free Consultaion request form from sidebar
	var sideOptions = {
		dataType: 'json',
		beforeSubmit: sideSendSubmit,
		success: sideSendResponse
	};
	
	$('.sideContact form').ajaxForm(sideOptions);
	
	checkForm('sideValidate',true);
	
	$('input.reserve').change(function(){
	
		if($(this).is(':checked')) {
			$('.reserve-show').slideDown();
		} else {
			$('.reserve-show').slideUp();
		}
		
	});
	
	// Homepage GA events
	$('#calcLink').click(function(){
		if( $(this).hasClass('animation') ) {
			_gaq.push(['_trackEvent', 'Clicks', 'Homepage', 'Intro Video - During animation']);
		} else {
			_gaq.push(['_trackEvent', 'Clicks', 'Homepage', 'Intro Video - After animation']);
		}
	});
	$('#galleryLink').click(function(){
		if( $(this).hasClass('animation') ) {
			_gaq.push(['_trackEvent', 'Clicks', 'Homepage', 'Postcard Gallery - During animation']);
		} else {
			_gaq.push(['_trackEvent', 'Clicks', 'Homepage', 'Postcard Gallery - After animation']);
		}
	});
	$('#contactLink').click(function(){
		_gaq.push(['_trackEvent', 'Clicks', 'Homepage', 'ROI Calculator']);
	});
	$('#pricingLink').click(function(){
		_gaq.push(['_trackEvent', 'Clicks', 'Homepage', 'Postcard Pricing']);
	});

});
</script>



<!--======= BEGIN SITE CONTAINER =======-->

<div id="container">

	<!--======= BEGIN MAIN CONTENT AREA =======-->

 <div id="content">

  <div id="content-below"></div>
   <div id="content-newBanner">
     <div class="main-quote">
     </div>
     <div class="description-part">
       <div class="description-part1">
          <div class="desc-img-part1">
          <a href="#"><img src="<?php echo THEME_IMAGES; ?>/video.png"</a>
          </div>
          <div class="desc-heading">SEE HOW IT WORKS</div>
          <div class="desc-sub-heading">Watch our short video of how easy doubling<br> your money can be!</div>
       </div>
       <div class="description-part2">
          <div class="desc-img-part2">
          <a href="#"><img src="<?php echo THEME_IMAGES; ?>/free.png"</a>
          </div>
          <div class="desc-heading">DESIGN GALLERY</div>
          <div class="desc-sub-heading">Choose from over 30 different designs that<br> can be customized to your own company!</div>
       </div>
       <div class="description-part3">
          <div class="desc-img-part3">
          <a href="#"><img src="<?php echo THEME_IMAGES; ?>/get-started.png"</a>
          </div>
          <div class="desc-heading">GET STARTED</div>
          <div class="desc-sub-heading">Our easy step form to get you set up for<br> ordering your next postcard mailing.</div>
       </div>
     </div>
   </div>
      
        <!--======= (END BANNER TRY) CONTENT AREA =======-->
        
	<!--======= BEGIN COLUMNS =======-->
	
	<div id="columnWrapper">
	
		<!--======= BEGIN FEATURED CONTENT =======-->
		<div id="featured">
		
			<div id="slider">
				
				<!-- Slide 1 -->
				<div id="slide1" class="slide hidden">
					<img id="slide1-card" src="<?php echo THEME_IMAGES; ?>/slide1-card.png" alt="Dental Postcard Marketing for My Enfield Dentist" />
					
					<div id="slide1-stats" class="slide-stats">
						<div class="headline">
							RESULTS
						</div>
						<div class="highlights">
							<span class="big brown bottom10">19 new patients</span>
							<span class="bigger orange bottom15">$17,400 in production</span>
							<span class="biggest blue shadow bold">ROI 497%</span>
						</div>
					</div>
				</div>
				
				<!-- Slide 2 -->
				<div id="slide2" class="slide hidden">
					<img id="slide2-card" src="<?php echo THEME_IMAGES; ?>/slide2-card.png" alt="Dental Postcard Marketing for Modern Dental Care" />
					
					<div id="slide2-stats" class="slide-stats">
						<div class="headline">
							CALLS
						</div>
						<div class="highlights">
							<span class="bigger blue bottom5">Over</span>
							<span class="biggest blue bold bottom10 shadow">90 Calls</span>
							<span class="big orange bottom5">In the 1st</span>
							<span class="biggest orange bold shadow">10 Days!!</span>
						</div>
					</div>
				</div>
				
				<!-- Slide 3 -->
				<div id="slide3" class="slide hidden">
					<img id="slide3-card" src="<?php echo THEME_IMAGES; ?>/slide3-card.png" alt="Dental Postcard Marketing for Hood Dental Care" />
					
					<div id="slide3-stats" class="slide-stats">
						<div class="headline">
							SUCCESS
						</div>
						<div class="highlights">
							<span class="biggest orange shadow bold bottom10">140 Calls!!</span>
							<span class="biggest blue shadow bold bottom5">Over $50K</span>
							<span class="bigger blue shadow">in Production</span>
						</div>
					</div>
				</div>
				
				<!-- Slide 4 -->
				<div id="slide4" class="slide hidden">
					<img id="slide4-card" src="<?php echo THEME_IMAGES; ?>/slide4-card.png" alt="Dental Postcard Marketing for Fossom Family Dental Care" />
					
					<div id="slide4-stats" class="slide-stats">
						<div class="headline">
							PATIENTS
						</div>
						<div class="highlights">
							<span class="big brown bold bottom5">62 Calls</span>
							<span class="big brown bold bottom10">19 New Patients</span>
							<span class="biggest orange bold shadow bottom10">$18,318</span>
							<span class="biggest blue bold shadow">522% ROI</span>
						</div>
					</div>
				</div>
				
				<!-- Slide 5 -->
				<div id="slide5" class="slide hidden">
					<img id="slide5-card" src="<?php echo THEME_IMAGES; ?>/slide5-card.png" alt="Guaranteed Dental Marketing - Direct Mail" />
					
					<div id="slide5-stats" class="slide-stats">
						<div class="headline">
							YOUR TURN
						</div>
						<div class="highlights">
							<span class="bigger orange">Low Cost</span>
							<span class="biggest orange bold bottom5">Leader</span>
							<span class="biggest brown bold shadow">Guaranteed</span>
							<span class="biggest brown bold shadow">200% ROI</span>
							<span class="bigger brown">in 3 Months</span>
						</div>
					</div>
				</div>
			
				<!-- Persistent Elements -->
				<div id="shade"></div>
				<a href="/dental-direct-mail/" id="galleryLink" class="all-slides g-button brown animation">
					<span class="disappear">See our </span>Postcard Gallery
				</a>
				<a href="/how-dental-marketing-companies-work/" id="calcLink" class="all-slides g-button orange animation">
					How it Works Video
				</a>
				<div id="slider-logo-container">
					<img id="slider-logo" class="all-slides" src="<?php echo THEME_IMAGES; ?>/slider-logo.png" height="35px" width="174px"  />
					<img id="slider-logo-alt" class="all-slides hidden" src="<?php echo THEME_IMAGES; ?>/slider-logo-alt.png" height="35px" width="174px" />
					<img id="slider-logo-wrap" class="all-slides hidden" src="<?php echo THEME_IMAGES; ?>/slider-logo-fade.png" height="124px" width="284px" />
				</div>
				
				<a href="/dental-marketing-roi/" id="contactLink" class="all-slides g-button large hidden">
					ROI Calculator
				</a>
				<a href="/dental-practice-marketing-pricing/" id="pricingLink" class="all-slides g-button large hidden">
					Postcard Pricing
				</a>
				<div id="sliderPhone">Call Now: 877-319-7772</div>
				
			</div>
			
			<div class="contactWidget sideContact" id="homeContact">
			<form action="<?php echo HOME_URI; ?>/wp-admin/admin-ajax.php">
				<h2>Request a Free Consultation</h2>
				<div class="formBody">
				<p class="practice"><label for="practice">Practice Name*</label>:<br /> 
					<input type="text" name="practice" class="text practiceName" value="Enter practice name" /></p>
				<p class="name"><label for="firstName">First and Last Name*</label>:<br />
					<input type="text" name="firstName" class="text firstName" value="Enter first"  />
					<input type="text" name="lastName" class="text lastName" value="and last name"  /></p>
				<p class="phone"><label for="phone">Phone Number*</label>:<br /> 
					(<input type="text" class="phoneArea" name="phoneArea" value="XXX" size="3" />) <input type="text" class="phonePrefix" name="phonePrefix" value="XXX" size="3" /> - <input type="text" class="phoneExchange" name="phoneExchange" value="XXXX" size="4" /></p>
				<p class="sticky"><label for="sticky">Do you want more information?</label><br />
					<input type="checkbox" class="sticky" name="sticky" /></p>
				<p class="email"><label>Email Address*</label>:<br /> 
					<input type="text" name="emailAddress" class="text emailAddress" value="Enter email address" /></p>
				<p class="practice"><label for="comments">Comments/Questions:</label><br />
					<textarea name="comments" cols="34" rows="2">Enter Comments/Questions (optional)</textarea></p>
					
				<p class="reserve"><label for="reserve">Check area availability: <input type="checkbox" name="reserve" class="reserve" value="checked" /></label></p>
				
				<div class="reserve-show">
				
					<p class="street"><label for="street">Street Address:</label><br />
						<input type="text" name="street" class="text street " value="Enter street address" /></p>
						
					<p class="street2"><label for="street2">Street Address (cont.):</label><br />
						<input type="text" name="street2" class="text street2" value="Enter street address (cont.)" /></p>
						
					<p class="suite"><label for="suite">Suite:</label><br />
						<input type="text" name="suite" class="text suite" value="Enter suite number" /></p>
						
					<p class="city"><label for="city">City, State, Zip:</label><br />
						<input type="text" name="city" class="text city" value="Enter city" />
						<?php statesDropdown(); ?>
						<input type="text" name="zip" class="text zip" value="Enter zip" />
						</p>
					
				</div>
				
				<input type="hidden" value="sendContactForm" name="action" />
				<input type="submit" value="Submit &raquo;" class="submit" /></div>
				
				<div id="sideSendResults" class="response-output"></div>
			</form>
			</div>
			
			<div class="clear"></div>
		
		</div>
		
		
		<!--======= END FEATURED CONTENT =======-->
		
		<!--======= BEGIN HOME BLOCKS =======-->
		
		<div id="homeBlocks">
			
			<?php 
	
			$args = array(
				'post_type' => 'testimonials',
				'orderby' => 'rand',
				'posts_per_page' => '1'
			);
			
			$result = new WP_Query( $args );
			
			if( $result->have_posts() ) : $result->the_post();
				
				$name	=	get_post_meta($post->ID, 'ecpt_tname', true);
				$exc	=	get_post_meta($post->ID, 'ecpt_texc', true);
				if( strlen( $exc ) > 175 ) {
					$exc = myTruncate( $exc, 175, ' ');
				}
				$auth	=	get_post_meta($post->ID, 'ecpt_tauth', true);
				$pos	=	get_post_meta($post->ID, 'ecpt_tpos', true);
				//$date	=	date( 'F j, Y', get_post_meta($post->ID, 'ecpt_tdate', true) );
				$stars	=	get_post_meta($post->ID, 'ecpt_tstar', true);
				
				?>
						
				<div id="homeTestimonial" class="homeBlock">
					<h2>Dental Marketing Testimonial</h2>
					<?php 
					if ( has_post_thumbnail() ) { // check if the testimonial has a Post Thumbnail assigned to it.
						?><a href="/dental-marketing-reviews/" class="homeLinkImg"><?php
						the_post_thumbnail( 'home-testimonial' );
						?></a><?php
					} 
					?>
					<?php echo $exc; ?>
					<div class="testimonialMeta">
						<h3 class="testimonialName">- <?php echo $name; ?>, <?php echo $auth; ?></h3>
						<img class="testimonialStars" src="<?php  echo THEME_IMAGES . '/' . $stars; ?>-stars.png" alt="<?php  echo $stars; ?> Star Rating for 123Postcards" />
					</div>
					<a href="/dental-marketing-reviews/" class="g-button large brown">More Testimonials &raquo;</a>
				</div>
					
			<?php
			
			endif; 
			
			?>
			
			<div class="homeBlock">
				<h2>Marketing for Dentists ROI</h2>
				<a href="/dental-marketing-roi/" class="homeLinkImg">
					<img src="<?php echo THEME_IMAGES; ?>/increasing-roi.jpg" alt="Guaranteed Dental Marketing ROI" />
				</a>
				<p>We record 100% of the calls we generate for you. In addition, we listen to and score <strong>every single call</strong> to help train your staff to turn more calls into new patients all for FREE!</p>
				<p>Your ROI matters to you, and it matters to us. Find out your guaranteed ROI.</p>
				<a href="/dental-marketing-roi/" class="g-button large brown">ROI Calculator &raquo;</a>
			</div>
			
			<div class="homeBlock last">
				<h2>Get New Patients Fast</h2>
				<a href="" class="homeLinkImg">
					<img src="<?php echo THEME_IMAGES; ?>/new-patients.jpg" alt="Dental Marketing to get New Patients" />
				</a>
				<p>Our process is designed to keep you doing what you love instead of struggling to figure out what works in marketing.</p>
				<p>With 123 Postcards, you get guaranteed ROI, a dedicated account manager, and new patients with every mailing.</p>
				<a href="/contact-dental-consultants/" class="g-button large brown">Free Consultation &raquo;</a>
			</div>
		
		<div class="clear"></div>
		</div>
		
		<!--======= END HOME BLOCKS =======-->
	
	</div>
	
	<!--======= END COLUMNS =======-->
	
	<div class="clear"></div>
	</div>

<!--======= END MAIN CONTENT AREA =======-->

<?php get_footer(); ?>