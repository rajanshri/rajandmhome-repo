<?php 
/*
template name: Pricing - Postcards
*/

get_header(); ?>

<div class="container">

<?php
$headerblock = get_post_meta($post->ID, 'ecpt_headerblock', true);
if ($headerblock != '') : ?>
<div class="subHeader">
	<div class="center sub">
		<?php echo $headerblock; ?>
	</div><!-- /center -->
</div>

<?php endif; ?>

<div class="section">
	<div class="center sub">
		<table class="pricingTable pcPricingTable">
			<thead>
				<tr>
					<th class="first noBG"></th>
					<th class="second"><img src="<?php echo THEME_IMAGES; ?>/logo-pc-w.png" alt="DentalMarketing.net" /></th>
					<th class="third">Them</th>
					<th class="fourth">DIY</th>
				</tr>
			</thead>
			<tbody>
				<tr class="light">
					<td class="first">Design</td>
					<td class="second"><span class="checked icon-check"></span></td>
					<td class="third"><span class="checked icon-check"></span></td>
					<td class="fourth"><span class="checked icon-check"></span></td>
				</tr>
				<tr class="dark">
					<td class="first">Printing</td>
					<td class="second"><span class="checked icon-check"></span></td>
					<td class="third"><span class="checked icon-check"></span></td>
					<td class="fourth"><span class="checked icon-check"></span></td>
				</tr>
				<tr class="light">
				  <td class="first">Mailing List</td>
					<td class="second"><span class="checked icon-check"></span></td>
					<td class="third"><span class="checked icon-check"></span></td>
					<td class="fourth"><span class="checked icon-check"></span></td>
				</tr>
				<tr class="dark">
					<td class="first">Postage & Mailing</td>
					<td class="second"><span class="checked icon-check"></span></td>
					<td class="third"><span class="checked icon-check"></span></td>
					<td class="fourth"><span class="checked icon-check"></span></td>
				</tr>
				<tr class="light">
					<td class="first">Call Tracking</td>
					<td class="second"><span class="checked icon-check"></span></td>
					<td class="third"><span class="checked icon-check"></span></td>
					<td class="fourth"></td>
				</tr>
				<tr class="dark">
					<td class="first">Account Manager</td>
					<td class="second"><span class="checked icon-check"></span></td>
					<td class="third"><span class="checked icon-check"></span></td>
					<td class="fourth"></td>
				</tr>
				<tr class="light">
					<td class="first">Call Recording</td>
					<td class="second"><span class="checked icon-check"></span></td>
					<td class="third"><span class="checked icon-check"></span></td>
					<td class="fourth"></td>
				</tr>
				<tr class="dark">
					<td class="first">Call Monitoring</td>
					<td class="second"><span class="checked icon-check"></span></td>
					<td class="third"></td>
					<td class="fourth"></td>
				</tr>
				<tr class="light">
					<td class="first">Call Scoring</td>
					<td class="second"><span class="checked icon-check"></span></td>
					<td class="third"></td>
					<td class="fourth"></td>
				</tr>
				<tr class="dark">
					<td class="first">Reporting Dashboard</td>
					<td class="second"><span class="checked icon-check"></span></td>
					<td class="third"></td>
					<td class="fourth"></td>
				</tr>
				<tr class="light">
					<td class="first">Staff Coaching</td>
					<td class="second"><span class="checked icon-check"></span></td>
					<td class="third"></td>
					<td class="fourth"></td>
				</tr>
				<tr class="dark">
					<td class="first">Training Videos</td>
					<td class="second"><span class="checked icon-check"></span></td>
					<td class="third"></td>
					<td class="fourth"></td>
				</tr>
				<tr class="light">
					<td class="first">Missed Call Notifications</td>
					<td class="second"><span class="checked icon-check"></span></td>
					<td class="third"></td>
					<td class="fourth"></td>
				</tr>
				<tr class="dark">
					<td class="first">Area Exclusivity</td>
					<td class="second"><span class="checked icon-check"></span></td>
					<td class="third"></td>
					<td class="fourth"></td>
				</tr>
				<tr class="light bold">
					<td class="first">200% ROI Guarantee</td>
					<td class="second"><span class="checked icon-check"></span></td>
					<td class="third"></td>
					<td class="fourth"></td>
				</tr>
				<tr class="dark pricingRow">
					<td class="first">Price Per Card</td>
					<td class="second">$.369 - $.429</td>
					<td class="third">$.349 - $.599</td>
					<td class="fourth">$.49+</td>
				</tr>
			</tbody>
			<tfoot class="subTable">
				<tr>
				</tr>
			</tfoot>
		</table>
	</div><!-- /center -->
</div><!-- /.section -->

		<div class="section">
			<div class="center sub">
				<div class="sectionWrapper">
					<div class="mainColumn">
						<?php	if (have_posts()) :	while (have_posts()) : the_post(); ?>
							<?php if($post->post_content != "") : ?>
								<?php the_content(); ?>
							<?php endif; ?>
						<?php endwhile; endif; wp_reset_postdata(); ?>
						<table style="width: 100%;">
							<tbody>
								<tr>
									<td>
										<h1>Get started with $499 down.</h1>
										<hr />
									</td>
									<td style="vertical-align: middle;">
										<img class="alignright" src="http://localhost/dm_responsive/wp-content/uploads/2013/07/Screen-Shot-2013-07-17-at-9.02.45-AM.png" alt="Guarantee Icon" width="168" height="110" />
									</td>
								</tr>
							</tbody>
						</table>
						<div class="pricingTable subPricingTable">
<!-- 							<h2>6" x 11" Postcard: $.369 - $.429<span>&#x25be;</span></h2> -->
							<h2>6" x 11" Postcard Pricing</h2>
							<table>
								<thead>
									<tr>
										<th>Quantity</th>
										<th>Price Per Card</th>
									</tr>
								</thead>
								<tbody>
									<tr class="dark">
										<td>5,000</td>
										<td>.429</td>
									</tr>
									<tr class="light">
										<td>10,000</td>
										<td>.399</td>
									</tr>
									<tr class="dark">
										<td>15,000</td>
										<td>.389</td>
									</tr>
									<tr class="light">
										<td>20,000</td>
										<td>.379</td>
									</tr>
									<tr class="dark">
										<td>25,000</td>
										<td>.369</td>
									</tr>
									<tr class="light">
										<td>Over 25,000</td>
										<td>Call for Details</td>
									</tr>
								</tbody>
							</table>
							<h3>Other sizes available, call for details.</h3>
							<h1><?php echo $phone; ?></h1>
						</div>
			
					</div><!-- /.mainColumn -->
					<div class="clear"></div>
				</div><!-- /.sectionWrapper -->
			</div><!-- /center -->
		</div><!-- /.section -->

<div id="specialPromotionHero"></div>

<!------ FORM ------>
<?php include('inc/sections/sub_form.php'); ?>

<div class="clear"></div>

<?php get_footer(); ?>

