
<?php 
/*
Template Name: Intro Video
*/
get_header(); ?>

<!--======= BEGIN SITE CONTAINER =======-->

<div id="container">

<a href="?vlaunch=1"></a>

<!--======= BEGIN MAIN CONTENT AREA =======-->

<div id="content">

<!--======= BEGIN COLUMNS =======-->

<div id="columnWrapper">

<!--======= BEGIN MAIN COLUMN =======-->

<div id="mainColumn" class="mainColumnStyle">
	
<!--======= BEGIN MAIN COLUMN CONTENT =======-->
	
	<div class="columnBody">

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<div class="page" id="post-<?php the_ID(); ?>">
		

            
        
			<h1>Dental Consultant Program by 123 Postcards</h1>
            <p>Watch the video below to learn how our dentist marketing consultant program works and then fill out the information to the right to get started.</p>
			

			<?php $load_url	= admin_url( 'admin-ajax.php' ) . '?action=cbLoadVid'; ?>
			<a class="vlaunch" href="<?php echo $load_url; ?>">
			<!-- The Colorbox Video script has been placed in footer.php file -->
			<img src="<?php echo THEME_IMAGES; ?>/youtube-vid.png" style="margin-bottom:10px;"/>
			</a>
			
			<?php the_content(); ?>
            
           
		</div>
	<?php endwhile; endif; ?>	
	
	</div>
    
    
	
	<!--======= END MAIN COLUMN CONTENT =======-->

</div>

<!--======= END MAIN COLUMN =======-->

<div class="clear"></div>
</div>

<!--======= END COLUMNS =======-->

<div class="clear"></div>
</div>

<!--======= END MAIN CONTENT AREA =======-->

<?php get_footer(); ?>