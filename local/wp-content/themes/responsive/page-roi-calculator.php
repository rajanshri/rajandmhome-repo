<?php
/*
Template Name: ROI Calculator
*/
get_header(); ?>

<div class="container">

<?php
$headerblock = get_post_meta($post->ID, 'ecpt_headerblock', true);
if ($headerblock != '') : ?>
<div class="subHeader">
	<div class="center sub">
		<?php echo $headerblock; ?>
	</div><!-- /center -->
</div>
<?php endif; ?>
		<div class="section roiCalculatorSeciton">
			<div class="center sub">
				<div class="sectionWrapper">
					<div class="mainColumn">
						<form id="roiCalculator" action="<?php echo HOME_URI; ?>/wp-admin/admin-ajax.php">
	
							<div id="roiStep1">
							
							<div class="step1">
								<h2>Enter your mailing information</h2>
								<p>Fill in the information below based on your past experience.<br/>If you are unsure about any of the fields, leave them as the default.</p>
							</div>
							
								<div class="formLabels">
									<div class="cardQty">Number of Postcards Mailed:</div>
									<div class="patVal">Average New Patient Production</div>
									<div class="callRate">Postcard Call Rate</div>
									<div class="convRate no-border">Call-to-Customer Rate</div>
								</div>
								
								<div class="formFields">
									<div class="cardQty">
										<select id="cardQty" name="cardQty" class="syncChange">
											<option value="5000">5,000</option>
											<option value="10000" selected="selected">10,000</option>
											<option value="15000">15,000</option>
											<option value="20000">20,000</option>
											<option value="25000">25,000</option>
										</select>
									</div>
									<div class="patVal">
										<select id="patVal" name="patVal" class="syncChange">
											<option value="750">$750</option>
											<option value="1000" selected="selected">$1,000</option>
											<option value="1250">$1,250</option>
											<option value ="1500">$1,500</option>
										</select>
									</div>
									<div class="callRate">
										<select id="callRate" name="callRate" class="syncChange">
											<option value=".002">Low (.2%)</option>
											<option selected="selected" value=".004">Medium (.4%)</option>
											<option value=".006">High (.6%)</option>
										</select>
									</div>
									<div class="convRate no-border">
										<select id="convRate" name="convRate" class="syncChange">
											<option value=".3">Low (30%)</option>
											<option selected="selected" value=".55">Medium (55%)</option>
											<option value=".7">High (70%)</option>
										</select>
									</div>
								</div>
								<div class="clear"></div>
							</div>
							
							<div id="roiStep2">
								
								<div class="step2">
									<button type="submit" value="submit" class="button large">Calculate ROI</button><img id="submitLoading" src="<?php echo THEME_IMAGES; ?>/roi-ajax.gif" alt="Loading" />
									<p>Find out your forecasted ROI and your guaranteed ROI.<br />200% ROI guaranteed with our 1-2-3 <a href="/guarantee-info/">Mail for Free Guarantee. </a><br />It's the best dental marketing money can buy!</p>
								</div>
								
								<div id="roiResults" class="hidden">
									<div id="forecasted">
										<h3>Forecasted Results</h3>
										<div class="wijradialgauge"></div>
										<div class="roi">Campaign ROI:</div>
										<div class="roiVal ajaxVal"><img src="<?php echo THEME_IMAGES; ?>/roi-ajax.gif" alt="Loading" /></div>
										<div class="labels">
											<div class="roiPhone">Phone Calls:</div>
											<div class="roiPatients">New Patients:</div>
											<div class="roiProduction">New Production:</div>
											<div class="roiCost">Total Cost:</div>
										</div>
										<div class="values">
											<div class="roiPhoneVal ajaxVal"><img src="<?php echo THEME_IMAGES; ?>/roi-ajax.gif" alt="Loading" /></div>
											<div class="roiPatientsVal ajaxVal"><img src="<?php echo THEME_IMAGES; ?>/roi-ajax.gif" alt="Loading" /></div>
											<div class="roiProductionVal ajaxVal"><img src="<?php echo THEME_IMAGES; ?>/roi-ajax.gif" alt="Loading" /></div>
											<div class="roiCostVal ajaxVal"><img src="<?php echo THEME_IMAGES; ?>/roi-ajax.gif" alt="Loading" /></div>
										</div>
									</div>
									<div id="guaranteed">
										<h3>Guaranteed Results</h3>
										<div class="wijradialgauge"></div>
										<div class="roi">Campaign ROI:</div>
										<div class="roiVal ajaxVal"><img src="<?php echo THEME_IMAGES; ?>/roi-ajax.gif" alt="Loading" /></div>
										<div class="labels">
											<div class="roiPhone">Phone Calls:</div>
											<div class="roiPatients">New Patients:</div>
											<div class="roiProduction">New Production:</div>
											<div class="roiCost">Total Cost:</div>
										</div>
										<div class="values">
											<div class="roiPhoneVal ajaxVal"><img src="<?php echo THEME_IMAGES; ?>/roi-ajax.gif" alt="Loading" /></div>
											<div class="roiPatientsVal ajaxVal"><img src="<?php echo THEME_IMAGES; ?>/roi-ajax.gif" alt="Loading" /></div>
											<div class="roiProductionVal ajaxVal"><img src="<?php echo THEME_IMAGES; ?>/roi-ajax.gif" alt="Loading" /></div>
											<div class="roiCostVal ajaxVal"><img src="<?php echo THEME_IMAGES; ?>/roi-ajax.gif" alt="Loading" /></div>
										</div>
									</div>
									<div class="clear"></div>
								</div>
							</div>
							<input type="hidden" name="action" id="action" value="getROI" />
							</form>
						<div id="roiStep3">
	
		<div class="step3">
			<h2>Increase your ROI Today</h2>
			<p>Fill out your contact information below for a Free Consultation.<br />
			We will contact you as soon as possible with fresh dental marketing ideas.</p>
		</div>
		
		<form id="roiSend"  action="<?php echo HOME_URI; ?>/wp-admin/admin-ajax.php">
	
		<div id="roiContact">
		
			<div id="formCover"><img src="<?php echo THEME_IMAGES; ?>/roi-ajax-loader.gif" alt="Loading" /></div>
			
			<table class="roiForm">
				<tr>
					<td class="practice label">Practice Name:</td>
					<td class="practice field"><input type="text" class="practiceName" name="practiceName" value="Enter practice name (required)" /></td>
				</tr>
				<tr>
					<td class="first label">First Name:</td>
					<td class="first field"><input type="text" class="firstName" name="firstName" value="Enter your first name (required)" /></td>
				</tr>
				<tr>
					<td class="last label">Last Name:</td>
					<td class="last field"><input type="text" class="lastName" name="lastName" value="Enter your last name (required)" /></td>
				</tr>
				<tr>
					<td class="pos label">Position:</td>
					<td>
						<select name="position" class="position">
							<option value="Doctor">Doctor</option>
							<option value="Office Manager">Office Manager</option>
							<option value="Marketing Manager">Marketing Manager</option>
							<option value="Spouse">Spouse</option>
							<option value="Other">Other</option>
						</select>
					</td>
				</tr>
				<tr>
					<td class="phoneNumber label">Phone Number:</td>
					<td class="phoneNumber field">(<input type="text" class="phoneArea" name="phoneArea" value="XXX" size="3" />) <input type="text" class="phonePrefix" name="phonePrefix" value="XXX" size="3" />-<input type="text" class="phoneExchange" name="phoneExchange" value="XXXX" size="4" /></td>
				</tr>
				<tr>
					<td class="sticky label">Do you want more information?</td>
				</tr>
				<tr>
					<td class="email label">Email Address:</td>
					<td class="email field"><input type="text" class="emailAddress" name="emailAddress" value="Enter your email (required)" /></td>
				</tr>
				<tr>
					<td class="reserve label">Check area availability:</td>
					<td class="sticky field"><input type="checkbox" class="sticky" name="sticky" /></td>
					<td class="reserve field"><input type="checkbox" class="reserve" name="reserve" value="checked" /></td>
				</tr>
				<tr class="reserve-show">
						<td class="street label">Street Address:</td>
						<td class="street field"><input type="text" class="street" name="street" value="Enter street address" /></td>
					</tr>
					<tr class="reserve-show">
						<td class="street2 label">Street Address (cont.):</td>
						<td class="street2 field"><input type="text" class="street2" name="street2" value="Enter street address (cont.)" /></td>
					</tr>
					<tr class="reserve-show">
						<td class="suite label">Suite:</td>
						<td class="suite field"><input type="text" class="suite" name="suite" value="Enter suite number" /></td>
					</tr>
					<tr class="reserve-show">
						<td class="city label">City:</td>
						<td class="city field"><input type="text" class="city" name="city" value="Enter city" /></td>
					</tr>
					<tr class="reserve-show">
						<td class="state label">State:</td>
						<td class="state field"><?php statesDropdown(); ?></td>
					</tr>
					<tr class="reserve-show">
						<td class="zip label">Zip:</td>
						<td class="zip field"><input type="text" class="zip" name="zip" value="Enter zip" /></td>
					</tr>
					<tr>
						<td class="questions no-border">Comments/Questions:</td>
						<td class="questions no-border field"><textarea class="comments" name="comments">Enter Comments/Questions (optional)</textarea></td>
					</tr>
				</table>
			
			<input type="hidden" name="cardQty" id="cardQtySend" value="5000" />
			<input type="hidden" name="patVal" id="patValSend" value="750" />
			<input type="hidden" name="callRate" id="callRateSend" value=".004" />
			<input type="hidden" name="convRate" id="convRateSend" value=".55" />
			<input type="hidden" id="interest" name="interest" value="postcards" />
			<input type="hidden" name="action" id="action" value="sendROI" />
			
			<button type="submit" value="submit" class="button large">Submit Consultation Request</button><img id="submitLoading2" src="<?php echo THEME_IMAGES; ?>/roi-ajax.gif" alt="Loading" />
			
		</div>
	
		</form>
		
	<div id="roiSendResults"></div>
	
	</div>
					</div><!-- /.mainColumn -->
					<div class="clear"></div>
				</div><!-- /.sectionWrapper -->
			</div><!-- /center -->
		</div><!-- /.section -->

<div id="specialPromotionHero"></div>

<!--====== CTA SECTION ======-->
<?php include('inc/sections/cta_section.php'); ?>

<div class="clear"></div>

<script type="text/javascript">
// ROI form functions
function roiResponse( data ){

	jQuery('#submitLoading').fadeOut();
	jQuery('#roiResults').animate({ 'opacity': 'show', 'height': 'show' }).removeClass('hidden');
	
	_gaq.push(['_trackEvent', 'Postcards', 'ROI Calculator', 'View']);
	
	function sector(cx, cy, r, startAngle, endAngle, params) {
		var x1 = cx + r * Math.cos(-startAngle * rad),
			x2 = cx + r * Math.cos(-endAngle * rad),
			y1 = cy + r * Math.sin(-startAngle * rad),
			y2 = cy + r * Math.sin(-endAngle * rad);
		return paper.path(["M", cx, cy, "L", x1, y1, "A", r, r, 0, +(endAngle - startAngle > 180), 0, x2, y2, "z"]).attr(params);
	}

	
	var sharedRadialOptions = {
		value: parseInt(data.forecasted.roi),
		height: 250,
		width: 305,
        max: 500,
        min: 0,
        startAngle: 10,
        sweepAngle: 160,
        radius: 120,
        islogarithmic: false,
        origin: {
            x: 0.5, y: 0.5
        },
        labels: {
            offset: 35, //4F6B82
            style: {
                fill: "#eb8123",
                "font-size": 12,
                "font-weight": "800"
            }
        },
        tickMinor: {
            position: "inside",
            offset: 30,
            style: {
                "height": 1,
                "width": 4,
                fill: "#eb8123",
                stroke: "none"
            },
            interval: 100/3,
            visible: true
        },
        tickMajor: {
            position: "inside",
            offset: 27,
            style: {
                fill: "#eb8123",
                "height": 2,
                "width": 8,
                stroke: "none"
            },
            interval: 100,
            visible: true
        },
        ranges: [
            {
                startWidth: 2,
                endWidth: 2,
                startValue: 0,
                endValue: 100,
                startDistance: 0.6,
                endDistance: 0.59,
                style: {
                    fill: "#fe3939",
                    stroke: "none"
                }
            },
            {
                startWidth: 2,
                endWidth: 4,
                startValue: 99,
                endValue: 250,
                startDistance: 0.59,
                endDistance: 0.58,
                style: {
                    fill: "0-#feb639-#fff115",
                    stroke: "none"
                }
            },
            {
                startWidth: 4,
                endWidth: 6,
                startValue: 249,
                endValue: 500,
                startDistance: 0.58,
                endDistance: 0.57,
                style: {
                    fill: "0-#fff115-#4ecf35",
                    stroke: "none"
                }
            }
        ],
        face: {
            template: function(ui) {
                var a = ui.canvas.path(jQuery.wijraphael.sector(ui.origin.x, ui.origin.y, ui.r, 0, 180)),
                style = {
                    fill: "90-#333333-#5f5f60",
                    stroke: "#333333"
                };
                style
                a.attr(style);
                //return ui.canvas.image(url, ui.origin.x - ui.r, ui.origin.y - ui.r, ui.r * 2, ui.r * 2);
            }
        },
        pointer: {
            length: 0.8,
            offset: 0,
            width: 6,
            style: {
                fill: "#eb8123",
                stroke: "none"
            }
        },
        cap: {
            radius: 5,
            style: {
                fill: "#eb8123",
                stroke: "none"
            }
        }

	};
	
	var foreRadialOptions = clone(sharedRadialOptions);
	var guarRadialOptions = clone(sharedRadialOptions);
	
	foreRadialOptions.value = parseInt(data.forecasted.roi);
	guarRadialOptions.value = parseInt(data.guaranteed.roi);
	
	jQuery('#forecasted .wijradialgauge').wijradialgauge(foreRadialOptions);
	jQuery('#guaranteed .wijradialgauge').wijradialgauge(guarRadialOptions);
	
	var forecastedRev = asMoney(data.forecasted.rev);
	var campaignCost = asMoney(data.campaignCost)
	var guaranteedRev = asMoney(data.guaranteed.rev);
	
	jQuery('#forecasted .roiVal').html(data.forecasted.roi);
	jQuery('#forecasted .roiPhoneVal').html(data.forecasted.calls);
	jQuery('#forecasted .roiPatientsVal').html(data.forecasted.patients);
	jQuery('#forecasted .roiProductionVal').html(forecastedRev);
	jQuery('#forecasted .roiCostVal').html(campaignCost);
	
	jQuery('#guaranteed .roiVal').html(data.guaranteed.roi);
	jQuery('#guaranteed .roiPhoneVal').html(data.guaranteed.calls);
	jQuery('#guaranteed .roiPatientsVal').html(data.guaranteed.patients);
	jQuery('#guaranteed .roiProductionVal').html(guaranteedRev);
	jQuery('#guaranteed .roiCostVal').html(campaignCost);
	
	var zopimNotes = 'ROI Calculator: Forecasted( ROI: ' + data.forecasted.roi + ', Calls: ' + data.forecasted.calls + ', Patients: ' + data.forecasted.patients + ', Revenue: ' + forecastedRev + ', Cost: ' + campaignCost + ')';
	
	$zopim.livechat.setNotes(zopimNotes);
	
}
function roiSubmit( formData, jqForm, options ){
	
	var loadingImg	=	'<img src="<?php echo THEME_IMAGES; ?>/roi-ajax.gif" alt="Loading" />';
	
	jQuery('#submitLoading').show();
	jQuery('.ajaxVal').html(loadingImg);
}

function roiSendSubmit( formData, jqForm, options ) {
	
	// Honeypot spam filter
	if( jQuery('#roiSend input.sticky').is(':checked') ) {
		alert('Are you a robot? I think so... ');
		return false;
	}

	jQuery('#roiSendResults').removeClass('error').addClass('loading').animate({ 'opacity': 'show', 'height': 'show' }).html('Loading...');
	
	var valErrors = new Array();
	
	if( jQuery('#roiSend .practiceName').val() == '' || jQuery('#roiSend .practiceName').val() == 'Enter practice name (required)' ) 	{
		valErrors.push('Practice Name');
		jQuery('#roiSend .practiceName').addClass('error');
	}
	if( 
		jQuery('#roiSend .firstName').val() == '' || 
		jQuery('#roiSend .firstName').val() == 'Enter your first name (required)' ||
		jQuery('#roiSend .lastName').val() == '' || 
		jQuery('#roiSend .lastName').val() == 'Enter your last name (required)'
	) {
		valErrors.push('First and Last Name');
		jQuery('#roiSend .firstName').addClass('error');
		jQuery('#roiSend .lastName').addClass('error');
	}
	if( 
		jQuery('#roiSend .phoneArea').val() == '' || 
		jQuery('#roiSend .phoneArea').val() == 'XXX' ||
		jQuery('#roiSend .phonePrefix').val() == '' ||
		jQuery('#roiSend .phonePrefix').val() == 'XXX' ||
		jQuery('#roiSend .phoneExchange').val() == '' ||
		jQuery('#roiSend .phoneExchange').val() == 'XXXX'
	) {
		valErrors.push('Phone Number');
		jQuery('#roiSend .phoneArea, #roiSend .phonePrefix, #roiSend .phoneExchange').addClass('error');
	}
	if( jQuery('#roiSend .phoneArea').val().length != 3 ||
		jQuery('#roiSend .phonePrefix').val().length != 3 ||
		jQuery('#roiSend .phoneExchange').val().length != 4 
	
	) {
		valErrors.push('Phone Number (Check Length)');
		jQuery('#roiSend .phoneArea, #roiSend .phonePrefix, #roiSend .phoneExchange').addClass('error');
	}
	if( jQuery('#roiSend .emailAddress').val() == '' || 
		jQuery('#roiSend .emailAddress').val() == 'Enter email address' 
	) {
		valErrors.push('Email Address');
		jQuery('#roiSend .emailAddress').addClass('error');
	}
	var emailAddress = jQuery('#roiSend .emailAddress').val();
	if( !emailReg.test(emailAddress) ) {
		valErrors.push('Email Address (Appears Invalid)');
		jQuery('#roiSend .emailAddress').addClass('error');
	}
	if( jQuery('#roiSend input.reserve').is(':checked') ) {
		if( jQuery('#roiSend input.street').val() == '' ||
			jQuery('#roiSend input.street').val() == 'Enter street address'
		) {
			valErrors.push('Street Address');
			jQuery('#roiSend input.street').addClass('error');
		}
		if( jQuery('#roiSend input.city').val() == '' ||
			jQuery('#roiSend input.city').val() == 'Enter city'
		) {
			valErrors.push('City');
			jQuery('#roiSend input.city').addClass('error');
		}
		if( jQuery('#roiSend input.zip').val() == '' ||
			jQuery('#roiSend input.zip').val() == 'Enter zip'
		) {
			valErrors.push('Zip');
			jQuery('#roiSend input.zip').addClass('error');
		}
	}
	
	if( valErrors.length > 0 ) {
		var valMessage = 'Please fill out the following required fields: ';
		if( valErrors.length > 1 ) {
			for( i = 0; i < valErrors.length; i++ ) {
				if( i < ( valErrors.length - 1 ) ) {
					valMessage += valErrors[i] + ', ';
				} else {
					valMessage += 'and ' + valErrors[i] + '.';
				}
			}
		} else {
			valMessage += valErrors[0];
		}
		jQuery('#roiSendResults').removeClass('loading').addClass('error').html(valMessage).animate({ 'opacity': 'show', 'height': 'show' });
		
		_gaq.push(['_trackPageview', '/virtual/lead-form/validation-errors/postcards/roi-calculator']);
		
		var setChatName = jQuery('#roiSend .firstName').val() + ' ' + jQuery('#roiSend .lastName').val();
		
		$zopim.livechat.setName(setChatName);
		
		return false;
	} else {
		return true;
	}
	
}

function roiSendResponse( data ) {

	_gaq.push(['_trackPageview', '/virtual/lead-form/success/postcards/roi-calculator']);
	
	//jQuery('#roiSend').animate({ 'height': 'hide' }, 250);
	
	jQuery('#roiSendResults').removeClass('loading').addClass(data.status).html(data.message);
	setTimeout(function(){
		jQuery('#roiSendResults').animate({ 'height': 'hide', 'opacity': 'hide' }, 
			function(){
				jQuery(this).removeClass('success').html('Loading...');
			}
		);
	}, 4000);
	
}
jQuery(document).ready(function($){
	// AJAX ROI calculator form
	var calcOptions = {
		dataType: 'json',
		beforeSubmit: roiSubmit,
		success: roiResponse
	};
	
	$('#roiCalculator').ajaxForm(calcOptions);
	
	// Free Consultaion request form from ROI calculator
	var sendOptions = {
		dataType: 'json',
		beforeSubmit: roiSendSubmit,
		success: roiSendResponse
	};
	
	$('#roiSend').ajaxForm(sendOptions);
	
	// Request More Info ROI Calculator Form Defaults
	var roiDefaults = [
		'Enter practice name (required)',
		'Enter your first name (required)',
		'Enter your last name (required)',
		'XXX',
		'XXXX',
		'Enter your email (required)',
		'Enter Comments/Questions (optional)',
		'Enter street address',
		'Enter street address (cont.)',
		'Enter suite number',
		'Enter city',
		'Enter zip'
	];
	$('#roiSend').deloClear({
		fieldDef: roiDefaults,
		preColor: '#CECECE'
	});
	
	// Synchronize fields for email request and step summary
	$('.syncChange').change(function(){
		var newSelector = '#' + $(this).attr('id') + 'Send';
		$(newSelector).val($(this).val());
	});
	
	checkForm('roiValidate');
	
	$('input.reserve').change(function(){
		if($(this).is(':checked')) {
			$(this).parents('#roiContact').find('.reserve-show').slideDown();
		} else {
			$(this).parents('#roiContact').find('.reserve-show').slideUp();
		}
	});

});
</script>

<?php get_footer(); ?>

