<?php 
/*
Template Name: Postcard Gallery
*/
get_header(); 
global $theme_meta;?>

<div class="container">

<?php
$headerblock = get_post_meta($post->ID, 'ecpt_headerblock', true);
if ($headerblock != '') : ?>
<div class="subHeader">
	<div class="center sub">
		<?php echo $headerblock; ?>
	</div><!-- /center -->
</div>
<?php endif; ?>
<div class="section">
	<div class="center sub">
		<div class="sectionWrapper">
			<div class="mainColumn">
					
				<?php	if (have_posts()) :	while (have_posts()) : the_post(); ?>
					<?php if($post->post_content != "") : ?>
						<?php the_content(); ?>
					<?php endif; ?>
				<?php endwhile; endif; wp_reset_postdata(); ?>
			
			</div><!-- /.mainColumn -->
			<div class="clear"></div>
		</div><!-- /.sectionWrapper -->
	</div><!-- /center -->
</div><!-- /.section -->

<div class="section">
	<div class="center sub pcGallery">
		<div class="sectionWrapper">
			<div class="mainColumn">
					
				<h2>Dentist Postcards Direct Mail Examples</h2>
				<?php echo do_shortcode('[nggallery id=1 template="lightbox"]'); ?>
				<h2>Back-to-School Direct Mail Examples</h2>
				<?php echo do_shortcode('[nggallery id=5 template="lightbox"]'); ?>
				<h2>Pediatric Dentistry Direct Mail Examples</h2>
				<?php echo do_shortcode('[nggallery id=3 template="lightbox"]'); ?>
				<h2>Orthodontic Direct Mail Examples</h2>
				<?php echo do_shortcode('[nggallery id=4 template="lightbox"]'); ?>
				<h2>Bilingual Direct Mail Examples</h2>
				<?php echo do_shortcode('[nggallery id=6 template="lightbox"]'); ?>
			
			</div><!-- /.mainColumn -->
			<div class="clear"></div>
		</div><!-- /.sectionWrapper -->
	</div><!-- /center -->
</div><!-- /.section -->

<div id="specialPromotionHero"></div>

<?php
	// Include post-content template to match category, if applicable
	if( checkCategories($post->ID, 'gallery')) {
		include 'inc/video_gallery.php';
	} elseif( checkCategories($post->ID, 'contact')) {
		include 'inc/contact.php';
	};
?>

<!--====== CTA SECTION ======-->
<?php include('inc/sections/cta_section.php'); ?>

<!------ FORM ------>
<?php include('inc/sections/sub_form.php'); ?>

<div class="clear"></div>

<?php get_footer(); ?>

