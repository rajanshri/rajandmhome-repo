<?php 

/*
Template Name: Pricing - Videos
*/

get_header(); ?>

<div class="container">

<?php
$headerblock = get_post_meta($post->ID, 'ecpt_headerblock', true);
if ($headerblock != '') : ?>
<div class="subHeader">
	<div class="center sub">
		<?php echo $headerblock; ?>
	</div><!-- /center -->
</div>
<?php endif; ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	<div class="section">
		<div class="center sub">
			<div class="sectionWrapper">
				<div class="mainColumn">
					<table class="pricingTable vPricingTable">
						<thead>
							<tr>
								<th class="first noBG"></th>
								<th class="bnw">Whiteboard</th>
								<th class="color">Full Color</th>
							</tr>
						</thead>
						<tbody>
							<tr class="light">
								<td class="first">2-Minute Package</td>
								<td class="second">$4995</td>
								<td class="third">$6995</td>
							</tr>
							<tr class="dark">
								<td class="first">Additional Minutes*</td>
								<td class="second">$1995</td>
								<td class="third">$2995</td>
							</tr>
							<tr class="light">
							  <td class="first">Description of Package</td>
								<td class="second">Black & White Whiteboard Animation</td>
								<td class="third">Full Color Illustrated Animation</td>
							</tr>
							<tr class="dark">
								<td class="first">Professional Script/Storyboard</td>
								<td class="second"><span class="checked icon-check"></span></td>
								<td class="third"><span class="checked icon-check"></span></td>
							</tr>
							<tr class="light">
								<td class="first">Custom Original Art</td>
								<td class="second"><span class="checked icon-check"></span></td>
								<td class="third"><span class="checked icon-check"></span></td>
							</tr>
							<tr class="dark">
								<td class="first">Professional Voice Over</td>
								<td class="second"><span class="checked icon-check"></span></td>
								<td class="third"><span class="checked icon-check"></span></td>
							</tr>
							<tr class="light">
								<td class="first">Quality Animation</td>
								<td class="second"><span class="checked icon-check"></span></td>
								<td class="third"><span class="checked icon-check"></span></td>
							</tr>
							<tr class="dark">
								<td class="first">Full HD Video</td>
								<td class="second"><span class="checked icon-check"></span></td>
								<td class="third"><span class="checked icon-check"></span></td>
							</tr>
							<tr class="light">
								<td class="first">Color</td>
								<td class="second">Some Color</td>
								<td class="third">Full Color</td>
							</tr>
							<tr class="pricingVids dark">
								<td class="first"></td>
								<td class="second">
									<div class="imgWrapOverlay">
										<a class="videoImg cboxElement" rel="video_examples" href="<?php echo admin_url( 'admin-ajax.php' ) ?>?action=vidGallery&youtubeID=8qHhHjFH6Fs">
											<img src="http://busvids.tidesdev.net/wp-content/themes/businessvideos/images/play.png" />
										</a>
									</div>
									<div class="imgWrap">
										<img src="http://img.youtube.com/vi/8qHhHjFH6Fs/0.jpg" />
									</div>
								</td>
								<td class="third">
									<div class="imgWrapOverlay">
										<a class="videoImg cboxElement" rel="video_examples" href="<?php echo admin_url( 'admin-ajax.php' ) ?>?action=vidGallery&youtubeID=WON2nbRsQF0">
											<img src="http://busvids.tidesdev.net/wp-content/themes/businessvideos/images/play.png" />
										</a>
									</div>
									<div class="imgWrap">
										<img src="http://img.youtube.com/vi/WON2nbRsQF0/0.jpg" />
									</div>
								</td>
							</tr>
							<tr class="pricingBtns light">
								<td class="first"></td>
								<td class="second"><a href="<?php echo HOME_URI ?>/dental-video-gallery" class="button large">See More Samples</a></td>
								<td class="third"><a href="<?php echo HOME_URI ?>/dental-video-gallery" class="button large">See More Samples</a></td>
							</tr>
						</tbody>
						<tfoot class="subTable">
							<tr>
								<td colspan="3">*2 minute minimum required</td>
							</tr>
						</tfoot>
					</table>

					<div class="page" id="post-<?php the_ID(); ?>">
						<?php the_content(); ?>
					</div>
				</div><!-- /.mainColumn -->
				<div class="clear"></div>
			</div><!-- /.sectionWrapper -->
		</div><!-- /center -->
	</div><!-- /.section -->
	<?php endwhile; endif; ?>	

<!--====== CTA SECTION ======-->
<?php include('inc/sections/cta_section.php'); ?>

<!------ FORM ------>
<?php include('inc/sections/sub_form.php'); ?>

<div class="clear"></div>

<?php get_footer(); ?>

