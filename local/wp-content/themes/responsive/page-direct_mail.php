<?php 
/*
Template Name: Product - Direct Mail
*/

get_header(); ?>

<div class="container">

<?php
$headerblock = get_post_meta($post->ID, 'ecpt_headerblock', true);
if ($headerblock != '') : ?>
<div class="subHeader">
	<div class="center sub">
		<?php echo $headerblock; ?>
	</div><!-- /center -->
</div>

<?php endif; ?>

<div class="section">
	<div class="center sub">
		<div class="sectionWrapper">
			<div class="mainColumn">
			
			<?php	if (have_posts()) :	while (have_posts()) : the_post(); ?>
				<?php if($post->post_content != "") : ?>
					<?php the_content(); ?>
				<?php endif; ?>
			<?php endwhile; endif; wp_reset_postdata(); ?>

			<div class="productsContent">
				<div class="product">
					<img src="<?php echo THEME_IMAGES; ?>/products-page/postcards.jpg" alt="view gallery button" />
					<h2>Custom Design</h2>
					<p class="">We create a unique postcard design for your practice that matches the look and feel of your current marketing. Using the tracked results of millions of postcards mailed, design elements and offers are tailored to maximize results.</p>
					<a class="button" href="<?php echo HOME_URI; ?>/dental-postcard-gallery/">View Gallery</a>
					<div class="clear"></div>
				</div>
				<div class="product">
					<img src="<?php echo THEME_IMAGES; ?>/products-page/dashboard.jpg" src="" alt="staff training video image" />
					<h2>Reporting</h2>
					<p>We have an easy-to-use reporting interface, where you can view and track each campaign’s results. You will have 24/7 access to detailed ROI, new patient, staff and call analytics.</p>
					<a class="button" href="<?php echo HOME_URI; ?>/practice-dashboard/">Check Availability</a>
					<div class="clear"></div>
				</div>
				<div class="product">
					<img src="<?php echo THEME_IMAGES; ?>/products-page/map.jpg" src="" alt="dashboard image" />
					<h2>Map Area & Exclusivity</h2>
					<p>We work with our customers to target the best homes around their practice and provide the needed recommendations to ensure success. With our exclusivity promise, we will not mail for any other practice within your core mailing area.</p>
					<a class="button" href="<?php echo HOME_URI; ?>/map-area-exclusivity/">View Reporting</a>
					<div class="clear"></div>
				</div>
				<div class="product">
					<img src="<?php echo THEME_IMAGES; ?>/products-page/training.jpg" src="" alt="dashboard image" />
					<h2>Staff Training</h2>
					<p>How your staff answers the phone has a BIG impact on your success! Our program includes initial staff training and ongoing coaching. This will help your team know the proven tips and techniques to successfully convert calls to new patients.</p>
					<a class="button" href="<?php echo HOME_URI; ?>/staff-training/">Learn More</a>
					<div class="clear"></div>
				</div>
			</div>
		
			</div><!-- /.mainColumn -->
			<div class="clear"></div>
		</div><!-- /.sectionWrapper -->
	</div><!-- /center -->
</div><!-- /.section -->

<div id="specialPromotionHero"></div>

<?php
	// Include post-content template to match category, if applicable
	if( checkCategories($post->ID, 'gallery')) {
		include 'inc/video_gallery.php';
	} elseif( checkCategories($post->ID, 'contact')) {
		include 'inc/contact.php';
	};
?>

<!--====== CTA SECTION ======-->
<?php include('inc/sections/cta_section.php'); ?>

<!------ FORM ------>
<?php include('inc/sections/sub_form.php'); ?>

<div class="clear"></div>

<?php get_footer(); ?>

