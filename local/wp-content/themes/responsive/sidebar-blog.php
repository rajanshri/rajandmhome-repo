<?php 
global $theme_meta;
global $buttons;
?>
<!--======= BEGIN SIDEBAR =======-->
<div id="sidebar">
<div class="sidebarStyle">	
	<!--======= BEGIN SIDEBAR CONTENT =======-->
	<div class="columnBody">
    <div class="staff-caoching-nav">
			<ul>
      	<li>
		       
		      <?php if( count($buttons) > 0 ) : ?>
          <ul class="sideBoxes">
			      <?php foreach( $buttons as $key => $values ) : ?>
			      <li>
	            <div class="bannerBox">
								<a href="<?php echo $values['link'] ?>">
									<div class="bBoxImg"><img src="<?php echo $values['image'] ?>" alt="<?php echo $values['alt'] ?>" /></div>
									<h2 class="bBoxHeading"><?php echo $values['title'] ?></h2>
									<div class="bBoxSub"><?php echo $values['content'] ?></div>
								</a>
					    </div>
			      </li>
			      <?php endforeach; ?>
          </ul>
		      <?php endif; ?>
                     
					</ul><!-- /.sideBoxes -->
				</li>
			</ul>
		</div><!-- /.staff-coacing-nav -->
    
		<?php dynamic_sidebar('Blog'); ?>
		
		<?php dynamic_sidebar('Above Form'); ?>
         
		<div class="contactWidget sideContact" id="sideContact">
			<form action="<?php echo HOME_URI; ?>/wp-admin/admin-ajax.php">
				<h2>Request a Free Consultation</h2>
				<div class="formBody">
				<p class="practice"><label for="practice">Practice Name*</label>:<br /> 
					<input type="text" name="practice" class="text practiceName" value="Enter practice name" /></p>
				<p class="name"><label for="firstName">First and Last Name*</label>:<br />
					<input type="text" name="firstName" class="text firstName" value="Enter first"  />
					<input type="text" name="lastName" class="text lastName" value="and last name"  /></p>
				<p class="phone"><label for="phone">Phone Number*</label>:<br /> 
					(<input type="text" class="phoneArea" name="phoneArea" value="XXX" size="3" />) <input type="text" class="phonePrefix" name="phonePrefix" value="XXX" size="3" /> - <input type="text" class="phoneExchange" name="phoneExchange" value="XXXX" size="4" /></p>
				<p class="sticky"><label for="sticky">Do you want more information?</label><br />
					<input type="checkbox" class="sticky" name="sticky" /></p>
				<p class="email"><label>Email Address*</label>:<br /> 
					<input type="text" name="emailAddress" class="text emailAddress" value="Enter email address" /></p>
				<p class="practice"><label for="comments">Comments/Questions:</label><br />
					<textarea name="comments" cols="34" rows="2">Enter Comments/Questions (optional)</textarea></p>
				<?php if ($theme_meta == 'Postcards') :?>
					<p class="reserve"><label for="reserve">Check area availability: <input type="checkbox" name="reserve" class="reserve" value="checked" /></label></p>
					<div class="reserve-show">
					
						<p class="street"><label for="street">Street Address:</label><br />
							<input type="text" name="street" class="text street " value="Enter street address" /></p>
							
						<p class="street2"><label for="street2">Street Address (cont.):</label><br />
							<input type="text" name="street2" class="text street2" value="Enter street address (cont.)" /></p>
							
						<p class="suite"><label for="suite">Suite:</label><br />
							<input type="text" name="suite" class="text suite" value="Enter suite number" /></p>
							
						<p class="city"><label for="city">City, State, Zip:</label><br />
							<input type="text" name="city" class="text city" value="Enter city" />
							<?php statesDropdown(); ?>
							<input type="text" name="zip" class="text zip" value="Enter zip" />
							</p>
						
					</div>
				<?php endif; ?>
					
				<input type="hidden" name="brand" value="<?php echo $theme_meta ?>" />
				<input type="hidden" name="action" value="sendContactForm" />
				<input type="submit" value="Submit &raquo;" class="submit" /></div>
				
				<div id="sideSendResults" class="response-output"></div>
			</form>
		</div>
		<?php //wp_nav_menu( array( 'theme_location' => 'sidebar-menu' ) ); ?>
		<?php dynamic_sidebar('Below Form'); ?>
    
    
    
	</div><!-- /.columnBody -->
	<!--======= END SIDEBAR CONTENT=======-->
</div><!-- /.sidebarStyle -->
</div>
<!--======= END SIDEBAR =======-->

<script type="text/javascript">
function sideSendSubmit( formData, jqForm, options ) {
	
	// Honeypot spam filter
	if( jQuery('.sideContact input.sticky').is(':checked') ) {
		alert('Are you a robot? I think so... ');
		return false;
	}
	
	jQuery('#sideSendResults').removeClass('error').addClass('loading').animate({ 'opacity': 'show', 'height': 'show' }).html('Loading...');
	
	var valErrors = new Array();
	
	if( jQuery('.sideContact .practiceName').val() == '' || jQuery('.sideContact .practiceName').val() == 'Enter practice name' ) {
		valErrors.push('Practice Name');
		jQuery('.sideContact .practiceName').addClass('error');
	}
	if( 
		jQuery('.sideContact .firstName').val() == '' || 
		jQuery('.sideContact .firstName').val() == 'Enter first' ||
		jQuery('.sideContact .lastName').val() == '' || 
		jQuery('.sideContact .lastName').val() == 'Enter last'
	) {
		valErrors.push('First and Last Name');
		jQuery('.sideContact .firstName').addClass('error');
		jQuery('.sideContact .lastName').addClass('error');
	}
	if( 
		jQuery('.sideContact .phoneArea').val() == '' || 
		jQuery('.sideContact .phoneArea').val() == 'XXX' ||
		jQuery('.sideContact .phonePrefix').val() == '' ||
		jQuery('.sideContact .phonePrefix').val() == 'XXX' ||
		jQuery('.sideContact .phoneExchange').val() == '' ||
		jQuery('.sideContact .phoneExchange').val() == 'XXXX'
	) {
		valErrors.push('Phone Number');
		jQuery('.sideContact .phoneArea, .sideContact .phonePrefix, .sideContact .phoneExchange').addClass('error');
	}
	if( jQuery('.sideContact .phoneArea').val().length != 3 ||
		jQuery('.sideContact .phonePrefix').val().length != 3 ||
		jQuery('.sideContact .phoneExchange').val().length != 4 
	
	) {
		valErrors.push('Phone Number (Check Length)');
		jQuery('.sideContact .phoneArea, .sideContact .phonePrefix, .sideContact .phoneExchange').addClass('error');
	}
	if( jQuery('.sideContact .emailAddress').val() == '' || 
		jQuery('.sideContact .emailAddress').val() == 'Enter email address'
	) {
		valErrors.push('Email Address');
		jQuery('.sideContact .emailAddress').addClass('error');
	}
	var emailAddress = jQuery('.sideContact .emailAddress').val();
	if( !emailReg.test(emailAddress) ) {
		valErrors.push('Email Address (Appears Invalid)');
		jQuery('.sideContact .emailAddress').addClass('error');
	}
	if( jQuery('.sideContact input.reserve').is(':checked') ) {
		if( jQuery('.sideContact input.street').val() == '' ||
			jQuery('.sideContact input.street').val() == 'Enter street address'
		) {
			valErrors.push('Street Address');
			jQuery('.sideContact input.street').addClass('error');
		}
		if( jQuery('.sideContact input.city').val() == '' ||
			jQuery('.sideContact input.city').val() == 'Enter city'
		) {
			valErrors.push('City');
			jQuery('.sideContact input.city').addClass('error');
		}
		if( jQuery('.sideContact input.zip').val() == '' ||
			jQuery('.sideContact input.zip').val() == 'Enter zip'
		) {
			valErrors.push('Zip');
			jQuery('.sideContact input.zip').addClass('error');
		}
	}
	
	if( valErrors.length > 0 ) {
		var valMessage = 'Please fill out the following required fields: ';
		if( valErrors.length > 1 ) {
			for( i = 0; i < valErrors.length; i++ ) {
				if( i < ( valErrors.length - 1 ) ) {
					valMessage += valErrors[i] + ', ';
				} else {
					valMessage += 'and ' + valErrors[i] + '.';
				}
			}
		} else {
			valMessage += valErrors[0];
		}
		jQuery('#sideSendResults').removeClass('loading').addClass('error').html(valMessage);
		console.info('errorMessage',valMessage);
		
		_gaq.push(['_trackPageview', '/virtual/side-form/validation-errors']);
		
		return false;
	} else {
		jQuery('.sideContact input.submit').attr('disabled','disabled').addClass('disabled');
		console.log('Sidebar form submitted successfully');
	}
	
}

function sideSendResponse( data ) {

	console.info('response data',data);
	
	jQuery('.sideContact input.submit').removeAttr('disabled').removeClass('disabled');

	_gaq.push(['_trackPageview', '/virtual/side-form/success']);
	
	jQuery('#sideSendResults').removeClass('loading').addClass(data.status).html(data.message);
	setTimeout(function(){
		jQuery('#sideSendResults').animate({ 'height': 'hide', 'opacity': 'hide' }, 
			function(){
				jQuery(this).removeClass('success').html('Loading...');
			}
		);
	}, 4000);
	
}
jQuery(document).ready(function($){
	// Free Consultation Form Defaults
	var sideDefaults = [
		'Enter practice name',
		'Enter first',
		'and last name',
		'XXX',
		'XXXX',
		'Enter email address',
		'Enter Comments/Questions (optional)',
		'Enter street address',
		'Enter street address (cont.)',
		'Enter suite number',
		'Enter city',
		'Enter zip'
	];
	$('.sideContact').deloClear({
		fieldDef: sideDefaults,
		preColor: '#CECECE'
	});
	
	// Free Consultaion request form from sidebar
	var sideOptions = {
		dataType: 'json',
		beforeSubmit: sideSendSubmit,
		success: sideSendResponse
	};
	
	$('.sideContact form').ajaxForm(sideOptions);
	
	$('input.reserve').change(function(){
		if($(this).is(':checked')) {
			$(this).parents('p').siblings('.reserve-show').slideDown();
		} else {
			$(this).parents('p').siblings('.reserve-show').slideUp();
		}
	});

});
</script>