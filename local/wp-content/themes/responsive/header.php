<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<?php if (basename(get_permalink()) == 'coop') {
	?>
	<!-- Google Analytics Content Experiment code -->
	<script>function utmx_section(){}function utmx(){}(function(){var
	k='62806296-1',d=document,l=d.location,c=d.cookie;
	                        if(l.search.indexOf('utm_expid='+k)>0)return;
	function f(n){if(c){var i=c.indexOf(n+'=');if(i>-1){var j=c.
	indexOf(';',i);return escape(c.substring(i+n.length+1,j<0?c.
	length:j))}}}var x=f('__utmx'),xx=f('__utmxx'),h=l.hash;d.write(
	'<sc'+'ript src="'+'http'+(l.protocol=='https:'?'s://ssl':
	'://www')+'.google-analytics.com/ga_exp.js?'+'utmxkey='+k+
	'&utmx='+(x?x:'')+'&utmxx='+(xx?xx:'')+'&utmxtime='+new Date().
	valueOf()+(h?'&utmxhash='+escape(h.substr(1)):'')+
	'" type="text/javascript" charset="utf-8"><\/sc'+'ript>')})();
	</script><script>utmx('url','A/B');</script>
	<!-- End of Google Analytics Content Experiment code -->
	<?php
};?>

<title><?php wp_title( '&mdash;' ); ?></title>

<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

<?php if (is_front_page()) { ?>
<!-- nivo slider info -->
<!--
	<link rel="stylesheet" href="<?php echo THEME_CSS; ?>/nivo-slider.css" type="text/css" />
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js" type="text/javascript"></script>
	<script src="<?php echo THEME_JS; ?>/jquery.nivo.slider.js" type="text/javascript"></script>
	<script type="text/javascript">
	$(window).load(function() {
	    $('#slider').nivoSlider();
	});
	</script>
-->
<?php } ?>

<!--======= BEGIN SCRIPT ONLOADS =======-->

<?php

wp_head();
global $my_theme;
global $theme_meta;
global $phone;
global $source;

$theme_class = 'theme-'. strtolower($theme_meta);

// cookie variables //
$c_source = $_COOKIE['source'];
$c_sources = $_COOKIE['sources'];
$c_phone = $_COOKIE['phone'];
$c_affiliate = $_COOKIE['aff'];
$c_ram = $_COOKIE['ram'];
?>

</head>

<body <?php body_class($theme_class); ?>>
<?php include 'dev/header_includes.php'; ?>