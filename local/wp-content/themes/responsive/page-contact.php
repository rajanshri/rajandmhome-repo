<?php 
/*
Template Name: Contact Us
*/

get_header(); ?>

<div class="container contactPage">

<?php
$headerblock = get_post_meta($post->ID, 'ecpt_headerblock', true);
if ($headerblock != '') : ?>
<div class="subHeader">
	<div class="center sub">
		<?php echo $headerblock; ?>
	</div><!-- /center -->
</div>

<?php endif; ?>

<?php
	// Include pre-content template to match category, if applicable
	if( checkCategories($post->ID, 'pricing')) {
		include 'inc/pricing.php';
	};
?>

<?php	if (have_posts()) :	while (have_posts()) : the_post(); ?>
	<?php if($post->post_content != "") : ?>
		<div class="section">
			<div class="center sub">
				<div class="sectionWrapper">
					<div class="mainColumn">
					
						<?php the_content(); ?>
			
					</div><!-- /.mainColumn -->
					<div class="clear"></div>
				</div><!-- /.sectionWrapper -->
			</div><!-- /center -->
		</div><!-- /.section -->
	<?php endif; ?>
<?php endwhile; endif; wp_reset_postdata(); ?>
	
<div id="contactInfo" class="section contactSection">
	<div class="center">
		<div class="sectionWrapper">
		 	<div class="contactTop">
				<div>
					<h2>877-319-7772</h2>
					<a href="mailto:info@123postcards.com">info@123postcards.com</a>
				</div>
				<div>
					<h4>Mailing Address</h4>
					DentalMarketing.net<br />
					520 North Main Street<br />
					Suite 501<br />
					Heber City, UT 84032<br />
				</div>
				<div>
					<h4>Location</h4>
					345 W 600 S, Suite 123<br />
					Heber City, UT 84032<br />
					F: 435-657-3737<br />
					<a href="https://www.google.com/maps/place/123+Postcards/@40.499335,-111.41963,18z/data=!4m2!3m1!1s0x0:0x28c1dd4efd941072?hl=en-US" target="_blank">View Map</a>
				</div>
				<p class="clear"></p>
			</div>
			<!--
<div class="contactBottom">
			 <div id="mapFrame">
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3033.880555751977!2d-111.4193!3d40.500101!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x874df5990d8eb79b%3A0x28c1dd4efd941072!2s123+Postcards!5e0!3m2!1sen!2sus!4v1387400078818" width="670" height="500" frameborder="0" style="border:0"></iframe>
					<br />
					<small><a href="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3033.880555751977!2d-111.4193!3d40.500101!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x874df5990d8eb79b%3A0x28c1dd4efd941072!2s123+Postcards!5e0!3m2!1sen!2sus!4v1387400078818" >View Larger Map</a></small>
				</div>	
			</div>
-->
			<div class="clear"></div>
		</div>
	</div><!-- /center -->
</div><!-- /#contactInfo -->
	
<?php
	// Include post-content template to match category, if applicable
	if( checkCategories($post->ID, 'gallery')) {
		include 'inc/video_gallery.php';
	} elseif( checkCategories($post->ID, 'contact')) {
		include 'inc/contact.php';
	};
?>

<!------ FORM ------>
<?php include('inc/sections/sub_form.php'); ?>

<div class="clear"></div>

<?php get_footer(); ?>

