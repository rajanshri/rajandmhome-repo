<?php 
/*
Template Name: Eblast Template
*/
global $my_theme;
global $theme_meta;
get_header(); ?>

<style type="text/css">
#columnWrapper
{
	background-image: none;
}
#mainColumn
{
	width: 1000px;
	padding: 0;
	text-align: center;
}
#mainColumn h1
{
	height: 65px;
	font-size: 50px;
	text-align: center;
	padding-top: 10px;
	color: #525151;
	line-height: 54px;
	margin: 15px 0 15px 0;
	width:1000px;
}
#mainColumn h1 span
{
	font-size: 72px;
}
#mainColumn h2
{
	font-size: 1.8em;
	line-height: 1.2em;
}

#ribbon {
	background: url(<?php echo HOME_URI; ?>/emails/images/ribbon.png);
	background-size: 280px 600px;
	padding: 35px 42px 37px 37px;
	width: 201px;text-align: center;
	height: 531px;margin-left: 5px;
	color: #fff;
	font-family: 'arial narrow', arial, sans-serif;
	font-stretch: condensed;
	font-weight: bold;
	font-style: italic;
	font-size: 1.8em;
	line-height: .9;
	float: left;
	}
	
#ribbon div {
	font-family: "open sans", "arial narrow", arial, sans-serif;
	font-stretch: condensed;
	color: #fff;
	font-weight: bold;
	font-style: italic;
	font-size: .8em;
	padding: 10px 0 0 0;
	line-height: 1.1;
	height: 90px;
	clear:both;
}

#ribbon div a {
	font-family: "open sans", "arial narrow", arial, sans-serif;
	font-stretch: condensed;
	color: #fff;
	font-weight: normal;
	font-style: italic;
	text-decoration: underline;
	line-height: 1.1;
}

#ribbon div a:hover {
	text-decoration: none;
}

#ribbon div img {
	float:left;
	border:0;
}


.quickDrawOffer {
	background: url(<?php echo HOME_URI; ?>/emails/images/quickdraw-landing1.jpg) no-repeat;
	background-position: left center;
	background-size: 395px 78px;
	padding: 0 106px 0 66px;
	width: 223px;
	height: 90px;
	text-align:center;
}

.quickDrawOffer h2 a{
	font-size: .7em;
	font-weight:bold;
}

.rightColumn{
	text-align: center;
	width: 300px;
	float:right;
	margin:10px 10px 0 13px;
	padding:10px 3px 15px;
	background:#DADADA
}

.landingPageForm{
	padding-left: 5px;
	text-align: center;
}
.landingPageForm td{
	text-align: left;
}
.landingPageForm textarea {
	height: 40px;
}
.landingPageForm input, .landingPageForm textarea {
	border: 1px solid #6e6e6e;
	width: 158px;
}
.landingPageForm input.state{
	float: left;
}
.landingPageForm input.zip{
	width: 90px;
}
#landingPageForm input.submit{
	width: 180px;
}
#landingSendResults.response-output{
	width: 224px;
}

.midColumn {
	padding: 10px 0 0 0;
	width: 385px;
	float: right;
}
.callToday{
	clear:right;
	text-align:center;
	padding:15px 0;
}
	
	
</style>


<!--======= BEGIN SITE CONTAINER =======-->

<div id="container">

<!--======= BEGIN MAIN CONTENT AREA =======-->

<div id="content">

<!--======= BEGIN COLUMNS =======-->

<div id="columnWrapper">

<!--======= BEGIN MAIN COLUMN =======-->

<div id="mainColumn" class="mainColumnStyle">
	
<!--======= BEGIN MAIN COLUMN CONTENT =======-->
	
	<div class="columnBody">

	<div class="page" id="post-<?php the_ID(); ?>">
		<h1>Marketing As Easy As 1-2-3! <img style="float: right; margin-top: -15px; padding-right: 40px;width: 120px;" src="<?php echo HOME_URI; ?>/emails/images/guaranteed.png" alt="Double Your Money Guaranteed" /></h1>
        <div id="ribbon">
			How do we double your money?<br />
            <div><a href="<?php echo HOME_URI; ?>/dentist-marketing-consultant-video/"><img src="<?php echo HOME_URI; ?>/emails/images/ribbon1.png" />Complete<br />Design-to-<br />Delivery<br />Solution</a></div>
            <div><a href="<?php echo HOME_URI; ?>/dental-call-tracking/"><img src="<?php echo HOME_URI; ?>/emails/images/ribbon2.png" />100% Call<br />Tracking,<br />Recording,<br />and Scoring</a></div>
            <div><a href="<?php echo HOME_URI; ?>/patient-dental-marketing-reporting/"><img src="<?php echo HOME_URI; ?>/emails/images/ribbon3.png" />24/7 Access<br />to Your<br />Mailing<br />Results</a></div>
            <div><a href="<?php echo HOME_URI; ?>/dental-receptionist-training/"><img src="<?php echo HOME_URI; ?>/emails/images/ribbon4.png" />Initial Staff<br />Training and<br />On-going<br />Coaching</a></div>
        </div>
        
		<div class="rightColumn">
			<!--<h2 style="font-size: 1.7em; text-align: center;">Send my FREE Demographic Analysis &amp; confirm that my exclusive area is available.</h2>-->
	        <img src="<?php echo HOME_URI; ?>/emails/images/yes_image-01.png" alt="Exclusive Townie Offer: Yes, provide me with a FREE Market Area Analysis, FREE Design &amp; Promotion Consultation, FREE Exclusivity Evaluation" style="
	    width: 300px;" />
			
			
			<form id="landingPageForm" action="<?php echo HOME_URI; ?>/wp-admin/admin-ajax.php">
			<table class="landingPageForm">
			<tr class="practice">
				<td>
					<label for="practice">Practice Name*:</label>
				</td>
				<td>
					<input type="text" name="practice" class="text practiceName" value="Enter practice name" />
				</td>
			</tr>
			<tr class="name">
				<td>
					<label for="firstName">First Name*:</label>
				</td>
				<td>
					<input type="text" name="firstName" class="text firstName" value="Enter first"  />
				</td>
			</tr>
			<tr class="name">
				<td>
					<label for="firstName">Last Name*:</label>
				</td>
				<td>
					<input type="text" name="lastName" class="text lastName" value="and last name"  />
				</td>
			</tr>
			<tr class="phone">
				<td>
					<label for="phone">Phone*:</label>
				</td> 
				<td>
					(<input type="text" class="phoneArea" name="phoneArea" value="XXX" size="3" />)
					<input type="text" class="phonePrefix" name="phonePrefix" value="XXX" size="3" /> - 
					<input type="text" class="phoneExchange" name="phoneExchange" value="XXXX" size="4" />
				</td>
			</tr>
			<tr class="sticky">
				<td>
					<label for="sticky">Do you want more information?</label>
					<input type="checkbox" class="sticky" name="sticky" />
				</td>
			</tr>
			<tr class="email">
				<td>
					<label>Email*:</label>
				</td> 
				<td>
					<input type="text" name="emailAddress" class="text emailAddress" value="Enter email address" />
				</td>
			</tr>
			<tr class="practice">
				<td>
					<label for="comments">Comments:</label>
				</td>
				<td>
					<textarea name="comments" rows="2">Enter Comments/Questions (optional)</textarea>
				</td>
			</tr>
			<?php if ($theme_meta == 'Postcards') :?>
			<tr class="reserve">
				<td colspan="2">
					<label for="reserve">Check area availability:</label>
					<input type="checkbox" name="reserve" class="reserve" value="checked" /></label>
				</td>
			</tr>
			<tr class="reserve-show street">
				<td>
					<label for="street">Address:</label>
				</td>
				<td>
					<input type="text" name="street" class="text street " value="Enter street address" />
				</td>
			</tr>
			<tr class="reserve-show street2">
				<td>
					<label for="street2">Address 2:</label>
				</td>
				<td>
					<input type="text" name="street2" class="text street2" value="Enter street address (cont.)" />
				</td>
			</tr>
			<tr class="reserve-show suite">
				<td>
					<label for="suite">Suite:</label>
				</td>
				<td>
					<input type="text" name="suite" class="text suite" value="Enter suite number" />
				</td>
			</tr>
			<tr class="reserve-show city">
				<td>
					<label for="city">City:</label>
				</td>
				<td>
					<input type="text" name="city" class="text city" value="Enter city" />
				</td>
			</tr>
			<tr class="reserve-show">
				<td>
					<label for="city">State, Zip:</label>
				</td>
				<td colspan="2">
				<?php statesDropdown(); ?>
					<input type="text" name="zip" class="text zip" value="Enter zip" />
				</td>
			</tr>
			<?php endif; ?>
		</table>
					
			<input type="hidden" id="interest" name="interest" value="<?php echo $my_theme['interest']; ?>" />
			<input type="hidden" name="action" value="sendContactForm" />
			<input id="source" type="hidden" name="source" value="<?php echo $source; ?>" />
			<input type="submit" value="Submit &raquo;" class="g-button large secondary submit" />
					
			<div id="landingSendResults" class="response-output"></div>
			</form>
		
		</div><!-- /.rightColumn -->
        
		<div class="midColumn">
        	<iframe src="http://www.youtube.com/embed/Hp_UESrTGbg?autoplay=0&amp;rel=0" frameborder="0" width="385" height="340"></iframe>
			<div class="quickDrawOffer">
      	<h2>
	      	<a href="<?php echo HOME_URI . "/" . $post->post_name; ?>-videos/">
	        Now offering Custom QuickDraw Videos to promote your practice!
	        </a>
        </h2>
      </div>
		</div>
        
		<div class="callToday">
			<h2 style="font-size:3.5em">Call <?php echo $phone; ?> Today!</h2>
		</div>
     
	</div>
	

    
	</div>
	
	<!--======= END MAIN COLUMN CONTENT =======-->

</div>

<!--======= END MAIN COLUMN =======-->

<div class="clear"></div>
</div>

<!--======= END COLUMNS =======-->

<div class="clear"></div>
</div>

<!--======= END MAIN CONTENT AREA =======-->

<script type="text/javascript">
function landingSendSubmit( formData, jqForm, options ) {

	console.info('formData',formData);
	console.info('jqForm',jqForm);
	console.info('options',options);
	
	// Honeypot spam filter
	if( jQuery('.landingPageForm input.sticky').is(':checked') ) {
		alert('Are you a robot? I think so... ');
		return false;
	}
	
	jQuery('#landingSendResults').removeClass('error').addClass('loading').animate({ 'opacity': 'show', 'height': 'show' }).html('Loading...');
	
	var valErrors = new Array();
	
	if( jQuery('.landingPageForm .practiceName').val() == '' || jQuery('.landingPageForm .practiceName').val() == 'Enter practice name' ) {
		valErrors.push('Practice Name');
		jQuery('.landingPageForm .practiceName').addClass('error');
	}		
	if( 
		jQuery('.landingPageForm .firstName').val() == '' || 
		jQuery('.landingPageForm .firstName').val() == 'Enter first' ||
		jQuery('.landingPageForm .lastName').val() == '' || 
		jQuery('.landingPageForm .lastName').val() == 'and last name'
	) {
		valErrors.push('First and Last Name');
		jQuery('.landingPageForm .firstName').addClass('error');
		jQuery('.landingPageForm .lastName').addClass('error');
	}
	if( 
		jQuery('.landingPageForm .phoneArea').val() == '' || 
		jQuery('.landingPageForm .phoneArea').val() == 'XXX' ||
		jQuery('.landingPageForm .phonePrefix').val() == '' ||
		jQuery('.landingPageForm .phonePrefix').val() == 'XXX' ||
		jQuery('.landingPageForm .phoneExchange').val() == '' ||
		jQuery('.landingPageForm .phoneExchange').val() == 'XXXX'
	) {
		valErrors.push('Phone Number');
		jQuery('.landingPageForm .phoneArea, .landingPageForm .phonePrefix, .landingPageForm .phoneExchange').addClass('error');
	}
	if( jQuery('.landingPageForm .phoneArea').val().length != 3 ||
		jQuery('.landingPageForm .phonePrefix').val().length != 3 ||
		jQuery('.landingPageForm .phoneExchange').val().length != 4 
	
	) {
		valErrors.push('Phone Number (Check Length)');
		jQuery('.landingPageForm .phoneArea, .landingPageForm .phonePrefix, .landingPageForm .phoneExchange').addClass('error');
	}
	if( jQuery('.landingPageForm .emailAddress').val() == '' || 
		jQuery('.landingPageForm .emailAddress').val() == 'Enter email address'
	) {
		valErrors.push('Email Address');
		jQuery('.landingPageForm .emailAddress').addClass('error');
	}
	var emailAddress = jQuery('.landingPageForm .emailAddress').val();
	if( !emailReg.test(emailAddress) ) {
		valErrors.push('Email Address (Appears Invalid)');
		jQuery('.landingPageForm .emailAddress').addClass('error');
	}
	if( jQuery('.landingPageForm input.reserve').is(':checked') ) {
		if( jQuery('.landingPageForm input.street').val() == '' ||
			jQuery('.landingPageForm input.street').val() == 'Enter street address'
		) {
			valErrors.push('Street Address');
			jQuery('.landingPageForm input.street').addClass('error');
		}
		if( jQuery('.landingPageForm input.city').val() == '' ||
			jQuery('.landingPageForm input.city').val() == 'Enter city'
		) {
			valErrors.push('City');
			jQuery('.landingPageForm input.city').addClass('error');
		}
		if( jQuery('.landingPageForm input.zip').val() == '' ||
			jQuery('.landingPageForm input.zip').val() == 'Enter zip'
		) {
			valErrors.push('Zip');
			jQuery('.landingPageForm input.zip').addClass('error');
		}
	}
	
	if( valErrors.length > 0 ) {
		var valMessage = 'Please fill out the following required fields: ';
		if( valErrors.length > 1 ) {
			for( i = 0; i < valErrors.length; i++ ) {
				if( i < ( valErrors.length - 1 ) ) {
					valMessage += valErrors[i] + ', ';
				} else {
					valMessage += 'and ' + valErrors[i] + '.';
				}
			}
		} else {
			valMessage += valErrors[0];
		}
		jQuery('#landingSendResults').removeClass('loading').addClass('error').html(valMessage);
		
		_gaq.push(['_trackPageview', '/virtual/lead-form/validation-errors/<?php echo $my_theme['interest']; ?>/landingpage']);
		
		return false;
	} else {
	}
	
}

function landingSendResponse( data ) {

	_gaq.push(['_trackPageview', '/virtual/lead-form/success/<?php echo $my_theme['interest']; ?>/landingpage']);
	
	jQuery('#landingSendResults').removeClass('loading').addClass('success').html(data.message);
	setTimeout(function(){
		jQuery('#landingSendResults').animate({ 'height': 'hide', 'opacity': 'hide' }, 
			function(){
				jQuery(this).removeClass('success').html('Loading...');
			}
		);
	}, 4000);
	
}
jQuery(document).ready(function($){
	// Free Consultation Form Defaults
	var defaults = [
		'Enter practice name',
		'Enter first',
		'and last name',
		'XXX',
		'XXXX',
		'Enter email address',
		'Enter Comments/Questions (optional)',
		'Enter street address',
		'Enter street address (cont.)',
		'Enter suite number',
		'Enter city',
		'Enter zip'
	];
	$('.landingPageForm').deloClear({
		fieldDef: defaults,
		preColor: '#CECECE'
	});
	
	// Free Consultaion request form from sidebar
	var sideOptions = {
		dataType: 'json',
		beforeSubmit: landingSendSubmit,
		success: landingSendResponse
	};
	
	$('#landingPageForm').ajaxForm(sideOptions);
	
	$('.firstName').change(function(){
		var practiceName = $('.firstName').val() + ' ' + $('.lastName').val();
		$('#practiceNameHidden').val(practiceName);
	});
	$('.lastName').change(function(){
		var practiceName = $('.firstName').val() + ' ' + $('.lastName').val();
		$('#practiceNameHidden').val(practiceName);
	});
	
	$('input.reserve').change(function(){
	
		if($(this).is(':checked')) {
			$(this).parents('.landingPageForm').find('.reserve-show').show();
		} else {
			$(this).parents('.landingPageForm').find('.reserve-show').hide();
		}
		
	});

});
</script>

<?php get_footer(); ?>