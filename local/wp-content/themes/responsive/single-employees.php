<?php get_header(); ?>

<!--======= BEGIN SITE CONTAINER =======-->

<div id="container">

<!--======= BEGIN MAIN CONTENT AREA =======-->

<div id="content">

<!--======= BEGIN COLUMNS =======-->

<div id="columnWrapper">

<!--======= BEGIN MAIN COLUMN =======-->

<div id="mainColumn" class="mainColumnStyle">
	
<!--======= BEGIN MAIN COLUMN CONTENT =======-->
	
	<div class="columnBody">

	<?php
	
	if (have_posts()) : while (have_posts()) : the_post();
	
	$jobtitle	=	get_post_meta($post->ID, 'ecpt_jobtitle', true);
	$caption 	=	get_post_meta($post->ID, 'ecpt_caption', true);
	$linkedin	=	get_post_meta($post->ID, 'ecpt_linkedin', true);
	$startdate	=	get_post_meta($post->ID, 'ecpt_startdate', true);
	$education	=	get_post_meta($post->ID, 'ecpt_education', true);
	
	$i++;
	
	$list_class = '';
	
	if( $i == $count ) : $list_class .= ' last'; endif;
	
	?>
		<div class="singleEmployee<?php echo $list_class; ?>" id="post-<?php the_ID(); ?>">
			
			<div class="employeeImage">
				<?php if ( has_post_thumbnail() ) { // check if the testimonial has a Post Thumbnail assigned to it.
					?><?php	the_post_thumbnail('large');	?><?php
				} ?>
			</div>
		
			<div class="employeeInfo">
				<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
				<h3><?php echo $jobtitle; ?></h3>
					<?php
						if ($linkedin != ''){
							echo "<a class='linkedIn' href='" . $linkedin . "'><img src='/wp-content/uploads/2013/08/linkedin.png' /></a>";
						}
					?>
				<div class="clear"></div>
				<?php if ($caption != ''){
						echo "<div class='caption'><sub>&#8220;</sub>" . $caption . "<sub>&#8221;</sub></div>";
					}
				?>
				<?php if ($startdate != '' || $education != '') { ?>
					<div class="otherInfo">
						<?php
							if ($startdate != ''){
								echo "<p><strong>Start Date:</strong> " . $startdate . "</p>";
							}
							if ($education != ''){
								echo "<p><strong>Education:</strong> " . $education . "</p>";
							}
						?>
					</div>
				<?php } ?>
			</div>
			<div class="employeeBio">
				<?php the_content(); ?>
			</div>
		</div>
	<?php endwhile; 
	
	wp_reset_query();
	
	wp_pagenavi();
	
	endif; ?>	

	
	</div>
	
	<!--======= END MAIN COLUMN CONTENT =======-->

</div>

<!--======= END MAIN COLUMN =======-->

<!--======= BEGIN SIDEBAR CONTENT AREA =======-->
<?php get_sidebar(); ?>
<!--======= END SIDEBAR CONTENT AREA =======-->

<div class="clear"></div>
</div>

<!--======= END COLUMNS =======-->

<div class="clear"></div>
</div>

<!--======= END MAIN CONTENT AREA =======-->

<?php get_footer(); ?>