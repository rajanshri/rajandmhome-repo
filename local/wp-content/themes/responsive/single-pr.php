<?php get_header(); ?>

<!--======= BEGIN SITE CONTAINER =======-->

<div id="container">

<!--======= BEGIN MAIN CONTENT AREA =======-->

<div id="content">

<!--======= BEGIN COLUMNS =======-->

<div id="columnWrapper">

<!--======= BEGIN MAIN COLUMN =======-->

<div id="mainColumn" class="mainColumnStyle">
	
<!--======= BEGIN MAIN COLUMN CONTENT =======-->
	
	<div class="columnBody">

	<?php if (have_posts()) : 
	
	$headline = get_post_meta($post->ID, 'ecpt_headline', true);
	$headline = str_replace('<p>', '<p>HEADLINE: ', $headline );
	$location = get_post_meta($post->ID, 'ecpt_location', true);
	
	while (have_posts()) : the_post(); ?>
		<div class="pr" id="post-<?php the_ID(); ?>">
			
			<h1><?php the_title(); ?></h1>
			
			<h3 class="prMeta"><?php the_time('F j, Y'); ?> &mdash; <?php echo $location; ?></h3>
			
			<div class="headline"><?php echo $headline; ?></div>
		
			<?php the_content(); ?>
			
			<h3>About 123postcards.com</h3>

			<p>123postcards.com is a Utah entity located in Heber City, Utah. 123 Postcards is a full spectrum dental marketing company providing offline dental marketing services to dentists in the United States of America. 123 Postcards creates and delivers high quality direct mail marketing campaigns, very quickly, while competing to be the low price industry leader.</p>
			
			<p>For one low price dentists receive:</p>
			
			<ul>
				<li>Creative Design</li>
				<li>Complete Printing Services</li>
				<li>Postage (saturated occupant list)</li>
				<li>Mailing Lists</li>
				<li>Drop-Shipping to their local Post Office</li>
				<li>List De-Duplication (to suppress existing clients)</li>
				<li>Demographic Analysis</li>
				<li>Call Tracking</li>
				<li>Custom Reporting</li>
				<li>Full Return on Investment Analysis</li>
			</ul>
			
			<p>Forward-looking Statements. This Press Release does not constitute an offer of any securities for sale. This press release contains forward-looking statements within the meaning of Section 27A of the Securities Act of 1933 and Section 21E of the Securities Exchange Act of 1934. While these statements are made to convey the company?s progress, business opportunities and growth prospects, readers are cautioned that such forward looking statements represent management?s opinion. Actual company results may differ materially from those described. The company?s operations and business prospects are always subject to risk and uncertainties. A more extensive listing of risks and factors that may affect the business prospects of the company and cause actual results to differ materially from those described in the forward-looking statements can be found in the reports and other documents filed by the company with the Securities and Exchange Commission.</p>
			
			<h3>FOR MORE INFORMATION CONTACT:</h3>
			
			<p><strong><em>INVESTOR RELATIONS</em></strong><br />
			Rocky Fischer<br />
			520 N Main, Ste 501<br />
			Heber, UT 84032<br />
			(877) 319-7772 (phone)<br />
			(435) 657-3737 (fax)<br />
			e-mail: info@123postcards.com<br />
			www.123postcards.com</p>
			
		</div>
	<?php endwhile; endif; ?>	
	
	</div>
	
	<!--======= END MAIN COLUMN CONTENT =======-->

</div>

<!--======= END MAIN COLUMN =======-->

<!--======= BEGIN SIDEBAR CONTENT AREA =======-->
<?php get_sidebar(); ?>
<!--======= END SIDEBAR CONTENT AREA =======-->

<div class="clear"></div>
</div>

<!--======= END COLUMNS =======-->

<div class="clear"></div>
</div>

<!--======= END MAIN CONTENT AREA =======-->

<?php get_footer(); ?>