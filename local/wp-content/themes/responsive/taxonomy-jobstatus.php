<?php get_header(); ?>

<!--======= BEGIN SITE CONTAINER =======-->

<div id="container">

<!--======= BEGIN MAIN CONTENT AREA =======-->

<div id="content">

<!--======= BEGIN COLUMNS =======-->

<div id="columnWrapper">

<!--======= BEGIN MAIN COLUMN =======-->

<div id="mainColumn" class="mainColumnStyle">
	
<!--======= BEGIN MAIN COLUMN CONTENT =======-->
	
	<div class="columnBody">
	
	<h1>123 Postcards Job Listings: <span class="taxonomyEmph"><?php echo strip_tags( get_the_term_list($post->ID, 'jobstatus') ); ?></span></h1>

	<?php
		
	if (have_posts()) : ?>
	
	<table class="jobsTable" cellspacing="0">
		<thead>
			<tr>
				<td>Title</td>
				<td>Department</td>
				<td>Status</td>
				<td>Salary</td>
				<td>Location</td>
			</tr>
		</thead>
		<tbody>
	
	<?php while (have_posts()) : the_post();
	
	$headline = get_post_meta($post->ID, 'ecpt_headline', true);
	$sal_low = get_post_meta($post->ID, 'ecpt_sal_low', true);
	$sal_high = get_post_meta($post->ID, 'ecpt_sal_high', true);
	$location = get_post_meta($post->ID, 'ecpt_location', true);
	$loc = get_post_meta($post->ID, 'ecpt_loc', true);
	
	?>
			<tr>
				<td><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></td>
				<td class="brownLink"><?php echo get_the_term_list( $post->ID, 'field'); ?></td>
				<td class="brownLink"><?php echo get_the_term_list( $post->ID, 'jobstatus'); ?></td>
				<td><?php echo $sal_low . ' - ' . $sal_high; ?></td>								
				<td><?php echo $loc; ?></td>
			</tr>
	<?php endwhile; ?>
	
		</tbody>
	</table>
	
	<?php
	wp_pagenavi();
	
	else: 
	?>
	
	<p>Sorry, there are no current positions at this time. Please feel free to submit your resume to info@123postcards.com for future consideration.</p>
	
	<?php endif; ?>
	
	</div>
	
	<!--======= END MAIN COLUMN CONTENT =======-->

</div>

<!--======= END MAIN COLUMN =======-->

<!--======= BEGIN SIDEBAR CONTENT AREA =======-->
<?php get_sidebar(); ?>
<!--======= END SIDEBAR CONTENT AREA =======-->

<div class="clear"></div>
</div>

<!--======= END COLUMNS =======-->

<div class="clear"></div>
</div>

<!--======= END MAIN CONTENT AREA =======-->

<?php get_footer(); ?>