<?php 
global $my_theme;
global $theme_meta;
global $phone;

?>
<!--======= BEGIN MAIN NAVIGATION=======-->

<div id="navigationWrapper">
	<div id="navWrap" class="navigationStyle">
		<?php wp_nav_menu( array( 'theme_location' => $my_theme['menu'] ) ); ?>
	</div>
</div>
<div id='breadcrumbsWrapper'>
<?php if (!is_front_page() && !is_page(23) && !is_page(1605) && !is_page(2247)){?>
	<div id='breadcrumbs'>
		<?php bcn_display(); ?>
	</div>
<?php }; ?>
</div>

<!--======= END MAIN NAVIGATION =======-->

<!--======= BEGIN HEADER=======-->

<div id="headerWrapper">
	<div id="header" class="headerStyle">
		<div class="logoBox">
			<a id="companyLogo" href="<?php echo get_bloginfo('home') . $my_theme['logo_path']; ?>">
				<img src="<?php echo THEME_IMAGES; ?>/<?php echo $my_theme['logo']; ?>" alt="Dental Marketing from 123 Postcards" />
			</a>
			<?php
			//if ($theme_meta == "Postcards") {
			//	echo "<h4 id='tagline'>The New Standard in Performance-Based Dental Marketing</h4>";
			//};?>
		</div>
		<div class="mobileMenuBtn"><span class="icon-menu"></span>Menu</div>
		<h2 id="phone" class="phoneReplace"><div>Call us today!</div><div><span> <?php echo $phone; ?></span></div></h2>
		<div id="topNav" class="topNavStyle">
			<div class="topTabs">
				<?php wp_nav_menu( array( 'theme_location' => 'top-tabs' ) ); ?>
			</div>
			<?php wp_nav_menu( array( 'theme_location' => 'top-menu' ) ); ?>
		</div>
		<div class="mobileMenuWrapper">
			<div class="mobileMenu">
				<div class="mobileMenuTop">
					<span class="mobileMenuHeader">Top Menu</span>
					<?php wp_nav_menu( array( 'theme_location' => 'top-tabs' ) ); ?>
					<?php wp_nav_menu( array( 'theme_location' => 'top-menu' ) ); ?>
				</div>
				<div class="mobileMenuMain">
					<span class="mobileMenuHeader">Main Menu</span>
					<?php wp_nav_menu( array( 'theme_location' => $my_theme['menu'] ) ); ?>
				</div>
				<div class="close icon-close"></div>			
			</div>
		</div>
	</div>
</div>

<!--======= END HEADER =======-->

<!--======= BEGIN FOOTER =======-->

<div id="footerWrapper">
	<div id="footer" class="footerStyle">
	
		<div id="footerSocial">
		
			<a id="footerTw" href="https://twitter.com/123postcards">Follow Us On <span>Twitter</span></a>
			<a id="footerRSS" href="<?php echo HOME_URI; ?>/blog/feed/rss">Subscribe To <span>RSS Feeds</span></a>
			<a id="footerFb" href="https://www.facebook.com/dentalmarketing.net">Find Us On <span>Facebook</span></a>
			<a id="footerLi" href="http://www.linkedin.com/company/123postcards">Connect With Us On <span>LinkedIn</span></a>
		
		</div>
		
		<div id="siteLinks">
			<?php wp_nav_menu( array( 'theme_location' => 'footer-menu' ) ); ?>
		</div>
		
		<div id="footerRight">
		
			<div id="authorityIcons">
			
				<img id="response" src="<?php echo THEME_IMAGES; ?>/bot-logo1.png" />
				<!--<img id="trusted" src="<?php echo THEME_IMAGES; ?>/bot-logo2.png" />-->
				<img id="usps" src="<?php echo THEME_IMAGES; ?>/bot-logo4.png" />
				<div class="clear"></div>
			
			</div>
			
			<div id="footerCopyright">
				
				Copyright &copy; <?php the_time('Y'); ?> <a href="http://www.dentalmarketing.net">DentalMarketing.net</a> &mdash; All Rights Reserved.
				123 Postcards &mdash; <?php echo $phone; ?><br />520 North Main Street Suite 501<br />Heber City, UT 84032
				
			</div>
			
		</div>
	
	</div>
</div>

<!--======= END FOOTER =======-->

<div class="clear"></div>
</div><!-- /.container -->

<div id="ppc_conversion"></div>

<!--======= END SITE CONTAINER =======-->

<!--======= START FOOTER SCRIPTS =======-->

<?php wp_footer(); ?>



<!--======= END FOOTER SCRIPTS =======-->

<!-- Google Code for Remarketing Tag -->
<!--------------------------------------------------
Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. See more information and instructions on how to setup the tag on: http://google.com/ads/remarketingsetup
--------------------------------------------------->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 987670919;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/987670919/?value=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript>


<!--Start of Zopim Live Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute('charset','utf-8');
$.src='//cdn.zopim.com/?V9eF99I710mUcKnnH2Lfb5fbdY6LNWqo';z.t=+new Date;$.
type='text/javascript';e.parentNode.insertBefore($,e)})(document,'script');
</script>
<!--End of Zopim Live Chat Script-->

</body>
</html>