<?php get_header(); ?>

<!--======= BEGIN SITE CONTAINER =======-->

<div id="container">

<!--======= BEGIN MAIN CONTENT AREA =======-->

<div id="content">

<!--======= BEGIN COLUMNS =======-->

<div id="columnWrapper">

<!--======= BEGIN MAIN COLUMN =======-->

<div id="mainColumn" class="mainColumnStyle">
	
<!--======= BEGIN MAIN COLUMN CONTENT =======-->
	
	<div class="columnBody">

	<?php if (have_posts()) : ?>

		<?php while (have_posts()) : the_post(); ?>
		
		<div class="post blog">
		
			<div class="post-date">
				<span class="post-day"><?php the_time('j') ?></span>
				<span class="post-month"><?php the_time('M') ?></span>
				<span class="post-year"><?php the_time('Y') ?></span>
			</div>
			
			<div class="post-title">
				<h2><?php the_title(); ?></h2>
			</div>
			
			<div class="post-info">
				<span class="post-categories">Categories: <?php the_category(', ') ?></span>
				<span class="post-commentcount"><?php comments_popup_link('No Comments', '1 Comment', '% Comments'); ?></span>
				<div class="clear"></div>
			</div>
			
			<div class="entry">
				<?php
					the_post_thumbnail('full');
					the_content();
				?>
			</div>
			
			<div class="tags">
				<?php the_tags(); ?>
			</div>
			
			<?php comments_template(); ?>
			
		</div>

		<?php endwhile; ?>

	<?php else : ?>

		<h2>Not Found</h2>
		<p>Sorry, but you are looking for something that isn't here.</p>

	<?php endif; ?>
	
	</div>
	
	<!--======= END MAIN COLUMN CONTENT =======-->

</div>

<!--======= END MAIN COLUMN =======-->

<!--======= BEGIN SIDEBAR CONTENT AREA =======-->
<?php get_sidebar(); ?>
<!--======= END SIDEBAR CONTENT AREA =======-->

<div class="clear"></div>
</div>

<!--======= END COLUMNS =======-->

<div class="clear"></div>
</div>

<!--======= END MAIN CONTENT AREA =======-->

<?php get_footer(); ?>