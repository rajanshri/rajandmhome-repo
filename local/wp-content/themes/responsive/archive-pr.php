<?php get_header(); ?>

<!--======= BEGIN SITE CONTAINER =======-->

<div id="container">

<!--======= BEGIN MAIN CONTENT AREA =======-->

<div id="content">

<!--======= BEGIN COLUMNS =======-->

<div id="columnWrapper">

<!--======= BEGIN MAIN COLUMN =======-->

<div id="mainColumn" class="mainColumnStyle">
	
<!--======= BEGIN MAIN COLUMN CONTENT =======-->
	
	<div class="columnBody">
	
	<h1>123 Postcards Press Releases</h1>

	<?php
	if( have_posts() ) :
	
	$i = 0;
	$count = 0;
	
		while( have_posts() ) : the_post();
	
		$count++;
	
		endwhile;
		
	endif;
	
	
	
	if (have_posts()) : while (have_posts()) : the_post();
	
	$headline = get_post_meta($post->ID, 'ecpt_headline', true);
	$headline = str_replace('<p>', '<p>HEADLINE: ', $headline );
	$location = get_post_meta($post->ID, 'ecpt_location', true);
	
	$i++;
	
	$list_class = '';
	
	if( $i == $count ) : $list_class .= ' last'; endif;
	
	?>
		<div class="archivePr<?php echo $list_class; ?>" id="post-<?php the_ID(); ?>">
		
			<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
			
			<h3 class="prMeta"><?php the_time('F j, Y'); ?> &mdash; <?php echo $location; ?></h3>
			
			<div class="headline"><?php echo $headline; ?></div>
		
			<?php the_excerpt('<p class="serif">Read the rest of this page &raquo;</p>'); ?>
			
			<a href="<?php the_permalink(); ?>">Read More &raquo;</a>
				
		</div>
	<?php endwhile; 
	
	wp_pagenavi();
	
	endif; ?>	
	
	</div>
	
	<!--======= END MAIN COLUMN CONTENT =======-->

</div>

<!--======= END MAIN COLUMN =======-->

<div class="clear"></div>
</div>

<!--======= END COLUMNS =======-->

<div class="clear"></div>
</div>

<!--======= END MAIN CONTENT AREA =======-->

<?php get_footer(); ?>