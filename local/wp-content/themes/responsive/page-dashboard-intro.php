<?php
/*
Template Name: Dashboard Intro
*/
global $theme_meta;
global $my_theme;
get_header();
$youtubeID = get_post_meta($post->ID, 'youtube_id', true);
?>

<!--======= BEGIN SITE CONTAINER =======-->

<div id="container">

<!--======= BEGIN MAIN CONTENT AREA =======-->

<div id="content">

<div id="specialPromotionHero"></div>

<!--======= BEGIN COLUMNS =======-->

<div id="columnWrapper">
	<div class="dashboardVideo">
			<img class="introducingTheDash" src="<?php echo THEME_IMAGES; ?>/introducing.png" />
		<iframe src="//www.youtube.com/embed/<?php echo $youtubeID; ?>" height="400" width="710" allowfullscreen="" frameborder="0"></iframe>
	</div>
	<div class="dashboardTop">
		<div class="introducingDashboard">
			<p>A new solution to easily track the success of all your campaigns.</p>
		</div>
		<div class="dashboardForm">
			<form id="funnelContact" class="funnelContact" action="<?php echo HOME_URI; ?>/wp-admin/admin-ajax.php">
				<h2 class="formTitle">Request a Free Consultation</h2>
				<div class="formBody">
					<div class="subHomeBlockContent">
						<p class="practice"><label for="practice">Practice Name*</label>:<br /> 
							<input type="text" name="practice" class="text practiceName" value="Enter practice name" /></p>
						<p class="name"><label for="firstName">First and Last Name*</label>:<br />
							<input type="text" name="firstName" class="text firstName" value="Enter first"  />
							<input type="text" name="lastName" class="text lastName" value="and last name"  /></p>
						<p class="phone"><label for="phone">Phone Number*</label>:<br /> 
							(<input type="text" class="phoneArea" name="phoneArea" value="XXX" size="3" />) <input type="text" class="phonePrefix" name="phonePrefix" value="XXX" size="3" /> - <input type="text" class="phoneExchange" name="phoneExchange" value="XXXX" size="4" /></p>
						<p class="sticky"><label for="sticky">Do you want more information?</label><br />
							<input type="checkbox" class="sticky" name="sticky" /></p>
						<p class="email"><label>Email Address*</label>:<br /> 
							<input type="text" name="emailAddress" class="text emailAddress" value="Enter email address" /></p>
						<p class="practice"><label for="comments">Comments/Questions:</label><br />
							<textarea name="comments" cols="34" rows="2">Enter Comments/Questions (optional)</textarea></p>
							<?php if ($theme_meta == 'Postcards') :?>
								<p class="reserve"><label for="reserve">Check area availability: <input type="checkbox" name="reserve" class="reserve" value="checked" /></label></p>
								<div class="reserve-show" style="display:none;">
								
									<p class="street"><label for="street">Street Address:</label><br />
										<input type="text" name="street" class="text street " value="Enter street address" /></p>
										
									<p class="street2"><label for="street2">Street Address (cont.):</label><br />
										<input type="text" name="street2" class="text street2" value="Enter street address (cont.)" /></p>
										
									<p class="suite"><label for="suite">Suite:</label><br />
										<input type="text" name="suite" class="text suite" value="Enter suite number" /></p>
										
									<p class="city"><label for="city">City, State, Zip:</label><br />
										<input type="text" name="city" class="text city" value="Enter city" />
									</p>
									<p class="stateZip">
										<?php statesDropdown(); ?>
										<input type="text" name="zip" class="text zip" value="Enter zip" />
									</p>
									
								</div>
							<?php endif; ?>
							<input type="hidden" name="brand" value="<?php echo $theme_meta ?>" />
							<input type="hidden" value="sendContactForm" name="action" />
					</div>
					<input type="submit" value="Submit" class="g-button submit large secondary" /></div>
				<div id="funnelSendResults" class="response-output"></div>
			</form>
		</div>
		<div class="clientLogin">
			Already a client? <a href="http://dashboard.dentalmarketing.net">Log into your dashboard here!</a>
		</div>
	</div>

<!--======= BEGIN MAIN COLUMN =======-->

<div id="mainColumn" class="mainColumnStyle">
	
<!--======= BEGIN MAIN COLUMN CONTENT =======-->
	
	<div class="columnBody">

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

    <?php
	/* if post id is 7 which is for design gallery and if form submit is successfully sent  from front page */
	 if(get_the_id()==7 && $_GET['fsuccess']==1){ ?>       
        <div class="notification-design-gallery">
         Thanks for your submission!<br> We'll contact you within 24 hours. To get a jump start on your direct mail campaign, get some ideas for your postcard design by browsing our design gallery below.
        </div>
    <?php } ?>
		<div class="page" id="post-<?php the_ID(); ?>">
		<?php the_content(); ?>
		</div>
	    <?php endwhile; endif; ?>	
	
	</div>
	
	<!--======= END MAIN COLUMN CONTENT =======-->

</div>

<!--======= END MAIN COLUMN =======-->

<div class="clear"></div>
</div>

<!--======= END COLUMNS =======-->

<div class="clear"></div>
</div>

<!--======= END MAIN CONTENT AREA =======-->

<script type='text/javascript' src='<?php echo THEME_JS; ?>/form-dash.js'></script>

<?php get_footer(); ?>



