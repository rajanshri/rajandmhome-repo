<?php get_header(); 
global $theme_meta;?>

<div class="container">



<?php if (have_posts()) : 

$name	=	get_post_meta($post->ID, 'ecpt_tname', true);
$loc 	=	get_post_meta($post->ID, 'ecpt_tloc', true);
$exc	=	get_post_meta($post->ID, 'ecpt_texc', true);
$auth	=	get_post_meta($post->ID, 'ecpt_tauth', true);
$pos	=	get_post_meta($post->ID, 'ecpt_tpos', true);
$pdesc = get_post_meta($post->ID, 'ecpt_practicedescription', true);
$pcFront = get_post_meta($post->ID, 'ecpt_postcard_front', true);
$pcBack = get_post_meta($post->ID, 'ecpt_postcard_back', true);
$is_cs = get_post_meta($post->ID, 'ecpt_iscs', true);

while (have_posts()) : the_post(); ?>
				
<div class="subHeader testimonialHeader">
	<div class="center sub">
		<h1><?php the_title(); ?></h1>
		<h3><?php echo $loc; ?></h3>
	</div><!-- /center -->
</div>

<div class="section testimonialSingle">
	<div class="center sub">
		<div class="sectionWrapper">
			<div class="mainColumn">
				<div class="caseStudy" id="post-<?php the_ID(); ?>">
					<?php if ( has_post_thumbnail() ) { // check if the testimonial has a featured image assigned to it.
						the_post_thumbnail();
					} ?>
					<div class="practiceDescription">
						<?php if ($pdesc) { 
							echo $pdesc; 
						}	?>
					</div>
					<div class="testimonialFull">
						<div class="testimonialText">
							<div class="icon-openq quotes"></div>
							<div class="betweenQuotes"><?php the_content(); ?></div>
							<div class="icon-closeq quotes"></div>
						</div>
						<div class="testimonialInfo">
							<h3 class="testimonialMeta">
								- <?php echo $auth; 
										if ($pos) {
											echo ', ' . $pos;
										};
									?>
							</h3>
						</div>
					</div>
					<?php if ($pcFront) { ?>
						<div class="postcardThumb">
							<img src="<?php echo $pcFront; ?>" /><br />
							<div>Front</div>
						</div>
					<?php } ?>
					<?php if ($pcBack) { ?>
						<div class="postcardThumb">
							<img src="<?php echo $pcBack; ?>" /><br />
							<div>Back</div>
						</div>
					<?php } ?>
					</div>
			<div class="clear"></div>

			</div><!-- /.mainColumn -->
			<div class="clear"></div>
		</div><!-- /.sectionWrapper -->
	</div><!-- /center -->
</div><!-- /.section -->
<?php 
endwhile;
endif; 
//wp_pagenavi();
wp_reset_query(); ?>	

<div id="specialPromotionHero"></div>

<?php
	// Include post-content template to match category, if applicable
	if( checkCategories($post->ID, 'gallery')) {
		include 'inc/video_gallery.php';
	} elseif( checkCategories($post->ID, 'contact')) {
		include 'inc/contact.php';
	};
?>

<!--====== CTA SECTION ======-->
<?php include('inc/sections/cta_section.php'); ?>

<!------ FORM ------>
<?php include('inc/sections/sub_form.php'); ?>

<div class="clear"></div>

<?php get_footer(); ?>
