<?php

// Truncate Text
// Original PHP code by Chirp Internet: www.chirp.com.au
// Please acknowledge use of this code by including this header.

function myTruncate( $string, $limit, $break=".", $pad="..." ) {
  // return with no change if string is shorter than $limit
  if(strlen($string) <= $limit) return $string;

  // is $break present between $limit and the end of the string?
  if(false !== ($breakpoint = strpos($string, $break, $limit))) {
    if($breakpoint < strlen($string) - 1) {
      $string = substr($string, 0, $breakpoint) . $pad;
    }
  }

  return $string;
}

function asMoney( $num ) {
	$money = '$' . number_format( $num );
	return $money;
}

function statesDropdown(){
	$states = array('AL'=>"Alabama",
		'AK'=>"Alaska",
		'AZ'=>"Arizona",
		'AR'=>"Arkansas",
		'CA'=>"California",
		'CO'=>"Colorado",
		'CT'=>"Connecticut",
		'DE'=>"Delaware",
		'DC'=>"District Of Columbia",
		'FL'=>"Florida",
		'GA'=>"Georgia",
		'HI'=>"Hawaii",
		'ID'=>"Idaho",
		'IL'=>"Illinois",
		'IN'=>"Indiana",
		'IA'=>"Iowa",
		'KS'=>"Kansas",
		'KY'=>"Kentucky",
		'LA'=>"Louisiana",
		'ME'=>"Maine",
		'MD'=>"Maryland",
		'MA'=>"Massachusetts",
		'MI'=>"Michigan",
		'MN'=>"Minnesota",
		'MS'=>"Mississippi",
		'MO'=>"Missouri",
		'MT'=>"Montana",
		'NE'=>"Nebraska",
		'NV'=>"Nevada",
		'NH'=>"New Hampshire",
		'NJ'=>"New Jersey",
		'NM'=>"New Mexico",
		'NY'=>"New York",
		'NC'=>"North Carolina",
		'ND'=>"North Dakota",
		'OH'=>"Ohio",
		'OK'=>"Oklahoma",
		'OR'=>"Oregon",
		'PA'=>"Pennsylvania",
		'RI'=>"Rhode Island",
		'SC'=>"South Carolina",
		'SD'=>"South Dakota",
		'TN'=>"Tennessee",
		'TX'=>"Texas",
		'UT'=>"Utah",
		'VT'=>"Vermont",
		'VA'=>"Virginia",
		'WA'=>"Washington",
		'WV'=>"West Virginia",
		'WI'=>"Wisconsin",
		'WY'=>"Wyoming"
	);
	echo '<select name="state" class="statesDropdown">';
	echo '<option selected="selected">--</option>';
	foreach($states as $key=>$value) {
		echo '<option value="'.$key.'">'.$key.'</option>';
	}
	echo '</select>';
}

function write_log($filename,$type,$message) {

	switch($type) {
		case 'new_action':
			$prefix	=	"\n\n\n!!!! [". date('r') ."] Action: ";
			break;
		case 'new_section':
			$prefix	=	"\n\n>>>> ";
			break;
		case 'sub_section':
			$prefix =	"\n   >>>> ";
			break;
		case 'end_action':
			$prefix =	"\n\n==== ";
			break;
		default: 
			$prefix =	"\n\n**** [no log action type specified] ";  
			break;
	}
	
	$handle	=	fopen($filename,'a');
	
	fwrite($handle,$prefix.$message);
	
	fclose($handle);
	
}

?>