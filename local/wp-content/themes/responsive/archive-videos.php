<?php 
/*
Template Name: Video Gallery 
*/
get_header(); ?>

<!--======= BEGIN SITE CONTAINER =======-->

<div id="container">

<!--======= BEGIN MAIN CONTENT AREA =======-->

<div id="content">

<!--======= BEGIN COLUMNS =======-->

<div id="columnWrapper">

<!--======= BEGIN MAIN COLUMN =======-->

<div id="mainColumn" class="mainColumnStyle">
	
<!--======= BEGIN MAIN COLUMN CONTENT =======-->

	<div class="columnBody">
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<?php echo the_content(); ?>
		<?php endwhile; endif; ?>
		
		<?php 
		$taxonomies = array( 
	    'videocategory'
		);
		
		$terms = get_terms( $taxonomies );
		
		foreach($terms as $term):
			$slug = $term->slug;
			$catTitle = $term->name;
			$catDesc = $term->description;
		?>
		
			<div class='videoCategory'>
				<h2 class='categoryTitle'><?php echo $catTitle; ?></h2>
				<div class="categoryDescription"><?php echo $catDesc; ?></div>
				<?php
				
				if (have_posts()) : 
						
				$args = array(
					'post_type' => 'videos',
					'posts_per_page' => '-1',
					'videocategory' => $slug
				);
				
				query_posts($args);
				while (have_posts()) : the_post(); 
				
				$id		=	get_post_meta($post->ID, 'ecpt_video_id', true);
				$length	=	get_post_meta($post->ID, 'ecpt_video_duration', true);
				$description =	get_post_meta($post->ID, 'ecpt_video_description', true);
				$title	=	get_post_meta($post->ID, 'ecpt_video_title', true);
				$feature	 =	get_post_meta($post->ID, 'ecpt_feature', true);
				// $image = get_the_post_thumbnail( $post->ID );
				$image = "<img src='http://img.youtube.com/vi/$id/0.jpg' alt='' />";
				$colorbox_url	=	admin_url( 'admin-ajax.php' ) . '?action=vidGallery&youtubeID=' . $id;
				?>
				<div class='archiveVideosWrapper' id='<?php echo $post->ID ?>'>
					<a href='<?php echo $colorbox_url ?>' class= 'videoImg'>
						<h3 class='iconText1'><?php echo $title ?><span><?php echo $length ?></span></h3>
						<?php	echo $image; ?>
					</a>
				</div>
			
				<?php	endwhile;	endif; ?>
			</div>
	
		<?php endforeach;	?>
		<?php 
		$content = '[cta]';
		echo do_shortcode( $content ) ?>
		
	</div>
	<!--======= END MAIN COLUMN CONTENT =======-->

</div>
<!--======= END MAIN COLUMN =======-->

<div class="clear"></div>
</div>

<!--======= END COLUMNS =======-->

<div class="clear"></div>
</div>
<!--======= END MAIN CONTENT AREA =======-->

<?php get_footer(); ?>

<! Start Lightbox scripts >

<script>
	jQuery(document).ready(function($) {
		jQuery('a.videoImg').colorbox({
			height: 673,
			width: 875,
			top: 25,
			fixed: false,
			arrowKey: false,
		});
	});
</script>