<?php get_header(); ?>

<!--======= BEGIN SITE CONTAINER =======-->

<div id="container">

<!--======= BEGIN MAIN CONTENT AREA =======-->

<!--======= BEGIN COLUMNS =======-->

<div id="columnWrapper">

<!--======= BEGIN MAIN COLUMN =======-->

<div id="mainColumn" class="mainColumnStyle">
	
<!--======= BEGIN MAIN COLUMN CONTENT =======-->
	
	<div class="columnBody">

	<?php if (have_posts()) : ?>
	
		<h2 id="category">Showing results for posts from:<?php $my_month = single_month_title('', false); if( $my_month != '' ) : echo '<br />'; single_month_title( ' ' ); else :  ?><br /><?php the_time( 'Y ' ); endif; ?></h2>

		<?php while (have_posts()) : the_post(); ?>

		<div class="post blog">
		
			<div class="post-date">
				<span class="post-day"><?php the_time('j') ?></span>
				<span class="post-month"><?php the_time('M') ?></span>
				<span class="post-year"><?php the_time('Y') ?></span>
			</div>
			
			<div class="post-title">
				<h2><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
			</div>
			
			<div class="post-info">
				<span class="post-categories">Categories: <?php the_category(', ') ?></span>
				<span class="post-commentcount"><?php comments_popup_link('No Comments', '1 Comment', '% Comments'); ?></span>
				<div class="clear"></div>
			</div>
			
			<div class="entry">
				<?php the_excerpt(); ?>
			</div>
			
			<p><a href="<?php the_permalink(); ?>" title="View Article">View this article &raquo;</a></p>
			
		</div>


		<?php endwhile; ?>

		<div class="postNavigation"> 
			<span class="previous-entries"><?php next_posts_link('Older Entries') ?></span>
			<span class="next-entries"><?php previous_posts_link('Newer Entries') ?></span> 
		</div>

	<?php else : ?>

		<h2>Not Found</h2>
		<p>Sorry, but you are looking for something that isn't here.</p>

	<?php endif; ?>
	
	<h2 class="browse">Browse Our Blog Archive</h1>
		
		<h2>Archives by Month:</h2>
		<ul class="post-archives">
			<?php wp_get_archives('type=monthly'); ?>
		</ul>
		
		<div class="hr" style="border-color:#DADADA;margin-right:25px;"></div>
		
		<h2>Archives by Subject:</h2>
		<ul class="post-archives">
			 <?php wp_list_categories( array ( 'title_li' => '' ) ); ?>
		</ul>
	
	</div>
	
	<!--======= END MAIN COLUMN CONTENT =======-->
	<img src="/images/featured-media.png" alt="United Gold Featured Media" />

</div>

<!--======= END MAIN COLUMN =======-->

<!--======= BEGIN SIDEBAR CONTENT AREA =======-->
<?php get_sidebar(); ?>
<!--======= END SIDEBAR CONTENT AREA =======-->

<div class="clear"></div>
</div>

<!--======= END COLUMNS =======-->

<!--======= END MAIN CONTENT AREA =======-->

<?php get_footer(); ?>