<?php 
/*
Template Name: Eblast Template - Coop Columns
*/
global $my_theme;
global $theme_meta;
get_header(); ?>

<style type="text/css">
#columnWrapper
{
	background-image: none;
}
#mainColumn
{
	width: 1000px;
	padding: 25px 0;
}
#mainColumn h1
{
	font-size: 38px;
	color: #525151;
	line-height: 54px;
	margin-bottom: 26px;
}
#mainColumn h1 span
{
	font-size: 72px;
}
#mainColumn h2
{
	font-size: 1.8em;
	line-height: 1.2em;
}
#mainColumn h2
{
	font-size: 1.8em;
	line-height: 1.2em;
}
p
{
	margin-bottom: 1em;
}
.mainColumnStyle ul li{
	padding: 4px 0;
}
.landingPageForm input, .landingPageForm textarea {
	border: 1px solid #555;
	width: 180px;
}
.landingPageForm textarea {
	height: 40px;
}
.landingPageForm .firstName{
	width: 76px;
}
.landingPageForm .lastName {
	width: 90px;
}
form input.submit {
	width: 180px;
}
.column {
	float: left;
}
.topBox .first{
	width: 540px;
}
.topBox .second{
	width: 400px;
	padding: 0 10px;
}
.topBox{
	padding: 10px 20px;
	float:left;
}
.bottomBox{
	float: left;
	padding: 0 10px;
}
.bottomBox .column{
	float: left;
	padding: 10px;
	width: 470px;
}
.bottomBox img{
padding: 0 35px;
}
.testimonialWrapper{
	float:left;
	width: 1000px;
}
.homeTestimonial {
	background: #f6f6f6;
	float: left;
	width: auto;
	padding:24px 50px;
}

</style>


<!--======= BEGIN SITE CONTAINER =======-->

<div id="container">

<!--======= BEGIN MAIN CONTENT AREA =======-->

<div id="content">

<!--======= BEGIN COLUMNS =======-->

<div id="columnWrapper">

<!--======= BEGIN MAIN COLUMN =======-->

<div id="mainColumn" class="mainColumnStyle">
	
<!--======= BEGIN MAIN COLUMN CONTENT =======-->
	
	<div class="columnBody">

	<div class="page" id="post-<?php the_ID(); ?>">
		<div class="topBox">
			<div class="column first">
	      <iframe src="http://www.youtube.com/embed/XeSm5AI7jfQ?autoplay=0&rel=0" frameborder="0" width="540" height="303"></iframe>
	      <p>
					<h2>Cooperative Marketing by DentalMarketing.net</h2>
					<p>As a partner of DentalMarketing.net's Cooperative Marketing Program, you can expand your marketing efforts, offer a more comprehensive solution and make money by introducing your customers to our one of a kind direct mail, call tracking and video program. It's easy to get started and your customers will love the results.</p>
					<ul>	      
						<li>Double your customer's money*</li>
						<li>Expand your product offering</li>
						<li>Track customer marketing performance</li>
						<li>Receive competitive compensation</li>
						<li>And More</li>
					</ul>
	      </p>
			</div>
			<div class="column second" style="text-align: center;">
			
				<h1 style="text-align: center;">Grow your business with our Cooperative Marketing Program!</h1>
				
				<h2>Find out more:</h2>
				
				<form id="landingPageForm" action="<?php echo HOME_URI; ?>/wp-admin/admin-ajax.php">
					<table class="landingPageForm">
					<tbody>
						<tr>
							<td>
								<label for="practice">Company Name*</label>:
							</td>
							<td class="practice">
								<input type="text" name="practice" class="text practiceName" value="Enter company name" />
							</td>
						</tr>
						<tr>
							<td>
								<label for="firstName">First and Last Name*</label>:
							</td>
							<td class="name">
								<input type="text" name="firstName" class="text firstName" value="Enter first"  /> <input type="text" name="lastName" class="text lastName" value="and last name"  />
							</td>
						</tr>
						<tr>
							<td>
								<label for="phone">Phone Number*</label>:
							</td>
							<td class="phone">
								(<input type="text" class="phoneArea" name="phoneArea" value="XXX" size="3" />) <input type="text" class="phonePrefix" name="phonePrefix" value="XXX" size="3" /> - <input type="text" class="phoneExchange" name="phoneExchange" value="XXXX" size="4" />
							</td>
						</tr>
						<tr><td class="sticky"><label for="sticky">Do you want more information?</label><input type="checkbox" class="sticky" name="sticky" /></td></tr>
						<tr>
							<td>
								<label>Email Address*</label>:
							</td>
							<td class="email">
								<input type="text" name="emailAddress" class="text emailAddress" value="Enter email address" />
							</td>
						</tr>
						<tr>
							<td>
								<label for="comments">Comments/Questions:</label>
							</td>
							<td class="practice">
								<textarea name="comments" cols="34" rows="2">Enter Comments/Questions (optional)</textarea>
							</td>
						</tr>
					</tbody>
					</table>
	
					<input type="hidden" id="interest" name="interest" value="CoOp" />
					<input type="hidden" name="action" value="sendContactForm" />
					<input id="source" type="hidden" name="source" value="<?php echo $source; ?>" />
					<input type="submit" value="Submit &raquo;" class="g-button large secondary submit" />
					
					<div id="landingSendResults" class="response-output"></div>
				</form>
				
			</div>
		</div>
		<!-- Testimonials -->
		<div class="testimonialWrapper">
			<div class="homeTestimonial">
				<a href="/dental-marketing-reviews/" class="homeTestImg"><img width="159" height="159" src="<?php echo HOME_URI; ?>/wp-content/uploads/2013/12/Screen-Shot-2013-12-06-at-9.00.20-AM-213x110.png" class="attachment-home-testimonial wp-post-image" alt="Physician's Resource" /></a>
				<div class="testimonialContent">
					<p>"Dental Marketing has been a great partner and we would recommend them to anyone! As we've worked together, they've made everything so easy and they bring tremendous value to our doctor's practices. Their expertise and commitment to success is hard to find, and the benefit they've provided us as well, has been invaluable."</p>
					<div class="testimonialMeta">
						<span class="testimonialName">- Terri Allison - Physician's Resource</span>
					</div>
				</div>
			</div>
		</div>
		<!-- end Testimonials -->
		<div class="bottomBox">
			<div class="column first">
				<img src="<?php echo THEME_IMAGES; ?>/logo-pc.png" /><br />
				<iframe src="http://www.youtube.com/embed/8qHhHjFH6Fs?autoplay=0&rel=0" frameborder="0" width="470" height="264"></iframe>
			</div>
			<div class="column second">
				<img src="<?php echo THEME_IMAGES; ?>/logo-vd.png" /><br />
				<iframe src="http://www.youtube.com/embed/ohDB-7ZR-ws?autoplay=0&rel=0" frameborder="0" width="470" height="264"></iframe>
			</div>
		</div>
	</div>
    
	</div>
	
	<!--======= END MAIN COLUMN CONTENT =======-->

</div>

<!--======= END MAIN COLUMN =======-->

<div class="clear"></div>
</div>

<!--======= END COLUMNS =======-->

<div class="clear"></div>
</div>

<!--======= END MAIN CONTENT AREA =======-->

<script type="text/javascript">
function landingSendSubmit( formData, jqForm, options ) {

	console.info('formData',formData);
	console.info('jqForm',jqForm);
	console.info('options',options);
	
	// Honeypot spam filter
	if( jQuery('.landingPageForm input.sticky').is(':checked') ) {
		alert('Are you a robot? I think so... ');
		return false;
	}
	
	jQuery('#landingSendResults').removeClass('error').addClass('loading').animate({ 'opacity': 'show', 'height': 'show' }).html('Loading...');
	
	var valErrors = new Array();
	
	if( jQuery('.landingPageForm .practiceName').val() == '' || jQuery('.landingPageForm .practiceName').val() == 'Enter practice name' ) {
		valErrors.push('Practice Name');
		jQuery('.landingPageForm .practiceName').addClass('error');
	}	
	if( 
		jQuery('.landingPageForm .firstName').val() == '' || 
		jQuery('.landingPageForm .firstName').val() == 'Enter first' ||
		jQuery('.landingPageForm .lastName').val() == '' || 
		jQuery('.landingPageForm .lastName').val() == 'and last name'
	) {
		valErrors.push('First and Last Name');
		jQuery('.landingPageForm .firstName').addClass('error');
		jQuery('.landingPageForm .lastName').addClass('error');
	}
	if( 
		jQuery('.landingPageForm .phoneArea').val() == '' || 
		jQuery('.landingPageForm .phoneArea').val() == 'XXX' ||
		jQuery('.landingPageForm .phonePrefix').val() == '' ||
		jQuery('.landingPageForm .phonePrefix').val() == 'XXX' ||
		jQuery('.landingPageForm .phoneExchange').val() == '' ||
		jQuery('.landingPageForm .phoneExchange').val() == 'XXXX'
	) {
		valErrors.push('Phone Number');
		jQuery('.landingPageForm .phoneArea, .landingPageForm .phonePrefix, .landingPageForm .phoneExchange').addClass('error');
	}
	if( jQuery('.landingPageForm .phoneArea').val().length != 3 ||
		jQuery('.landingPageForm .phonePrefix').val().length != 3 ||
		jQuery('.landingPageForm .phoneExchange').val().length != 4 
	
	) {
		valErrors.push('Phone Number (Check Length)');
		jQuery('.landingPageForm .phoneArea, .landingPageForm .phonePrefix, .landingPageForm .phoneExchange').addClass('error');
	}
	if( jQuery('.landingPageForm .emailAddress').val() == '' || 
		jQuery('.landingPageForm .emailAddress').val() == 'Enter email address'
	) {
		valErrors.push('Email Address');
		jQuery('.landingPageForm .emailAddress').addClass('error');
	}
	var emailAddress = jQuery('.landingPageForm .emailAddress').val();
	if( !emailReg.test(emailAddress) ) {
		valErrors.push('Email Address (Appears Invalid)');
		jQuery('.landingPageForm .emailAddress').addClass('error');
	}
	
	if( valErrors.length > 0 ) {
		var valMessage = 'Please fill out the following required fields: ';
		if( valErrors.length > 1 ) {
			for( i = 0; i < valErrors.length; i++ ) {
				if( i < ( valErrors.length - 1 ) ) {
					valMessage += valErrors[i] + ', ';
				} else {
					valMessage += 'and ' + valErrors[i] + '.';
				}
			}
		} else {
			valMessage += valErrors[0];
		}
		jQuery('#landingSendResults').removeClass('loading').addClass('error').html(valMessage);
		
		_gaq.push(['_trackPageview', '/virtual/lead-form/validation-errors/coop/landingpage']);
		
		return false;
	} else {
	}
	
}

function landingSendResponse( data ) {

	console.info('data',data);

	_gaq.push(['_trackPageview', '/virtual/lead-form/success/coop/landingpage']);
	
	jQuery('#landingSendResults').removeClass('loading').addClass('success').html(data.message);
	setTimeout(function(){
		jQuery('#landingSendResults').animate({ 'height': 'hide', 'opacity': 'hide' }, 
			function(){
				jQuery(this).removeClass('success').html('Loading...');
			}
		);
	}, 4000);
}
jQuery(document).ready(function($){
	// Free Consultation Form Defaults
	var defaults = [
		'Enter company name',
		'Enter first',
		'and last name',
		'XXX',
		'XXXX',
		'Enter email address',
		'Enter Comments/Questions (optional)',
		'Enter street address',
		'Enter street address (cont.)',
		'Enter suite number',
		'Enter city',
		'Enter zip'
	];
	$('.landingPageForm').deloClear({
		fieldDef: defaults,
		preColor: '#CECECE'
	});
	
	// Free Consultaion request form from sidebar
	var sideOptions = {
		dataType: 'json',
		beforeSubmit: landingSendSubmit,
		success: landingSendResponse
	};
	
	$('#landingPageForm').ajaxForm(sideOptions);
	
	$('.firstName').change(function(){
		var practiceName = $('.firstName').val() + ' ' + $('.lastName').val();
		$('#practiceNameHidden').val(practiceName);
	});
	$('.lastName').change(function(){
		var practiceName = $('.firstName').val() + ' ' + $('.lastName').val();
		$('#practiceNameHidden').val(practiceName);
	});

});
</script>

<?php get_footer(); ?>