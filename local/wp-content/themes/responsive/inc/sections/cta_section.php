<div class='section lightGrayBG ctaSection'>
<div class='center'>
	<div class='sectionWrapper'>
		<?php 
		$ctaText = get_post_meta($post->ID, 'ecpt_cta_text', true);
		if ($ctaText) {
			echo $ctaText;
		} else {
			echo '<h1>Get started! ' . $phone . '</h1>';
		} ?>
	</div><!-- /.sectionwrapper-->
</div><!-- /.center -->
</div><!-- /.section -->