<div class="section subFormSection blueBG">
	<div class="center">
		<div class="sectionWrapper">
			<!-- sub form -->
			<div class="contactForm subForm">
 				<?php include(THEME_DIR . '/inc/form_content.php'); ?>
			</div>
			<div class="clear"></div>
			<!-- end sub form -->
		</div>
	</div><!-- /center -->
</div>