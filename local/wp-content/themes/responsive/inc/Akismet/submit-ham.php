<?php

require_once( get_stylesheet_directory() . '/inc/Akismet/Akismet.class.php' );

$akismet = new Akismet( $blog, 'beb589950138' );
$akismet->setCommentAuthor($comment_author);
$akismet->setCommentAuthorEmail($comment_author_email);
$akismet->setCommentContent($comment_content);
$akismet->setPermalink($referrer);
$akismet->setUserIP($user_ip);
$akismet->setReferrer($referrer);
$akismet->setCommentType($comment_type);
$akismet->setCommentUserAgent($user_agent);

$akismet_result = $akismet->submitSpam();

?>