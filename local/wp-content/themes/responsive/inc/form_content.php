<form action="<?php echo HOME_URI; ?>/wp-admin/admin-ajax.php">
	<h3>Request a Free Consultation</h3>
	<div class="formBody">
		<p class="practice"><label for="practice">Practice Name:*</label><br /> 
			<input type="text" name="practice" class="text practiceName" value="Enter practice name" /></p>
		<p class="name"><label for="firstName">First and Last Name:*</label><br />
			<input type="text" name="firstName" class="text firstName" value="Enter first"  />
			<input type="text" name="lastName" class="text lastName" value="and last name"  /></p>
		<p class="phone"><label for="phone">Phone Number:*</label><br /> 
			<input type="text" class="phoneArea" name="phoneArea" value="XXX" size="3" /><input type="text" class="phonePrefix" name="phonePrefix" value="XXX" size="3" /><input type="text" class="phoneExchange" name="phoneExchange" value="XXXX" size="4" /></p>
		<p class="email"><label>Email Address:*</label><br /> 
			<input type="text" name="emailAddress" class="text emailAddress" value="Enter email address" />
		</p>
		<p class="practice"><label for="comments">Comments/Questions:</label><br />
			<textarea name="comments" cols="34" rows="2">Enter Comments/Questions (optional)</textarea>
		</p>
		<input type="hidden" id="interest" name="interest" value="<?php echo $my_theme['interest']; ?>" />
		<input type="hidden" name="action" value="sendContactForm" />
		<input id="source" type="hidden" name="source" value="<?php echo $source; ?>" />
		<div id="sideSendResults" class="submitWrapper">
			<span class="icon icon-send"></span>
			<input type="submit" value="Submit &raquo;" class="submit" />
			<div class="response-output"></div>
		</div>
	</div>
</form>
<script type='text/javascript' src='<?php echo THEME_JS; ?>/form.js'></script>