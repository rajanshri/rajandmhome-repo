<?php

switch( $event['phone'] ) {
	case '+18323848337':
		$event['long_name']	=	'Practice Marketing and Internet Marketing hosted by Carestream at Benco Dental';
		$event['short_name']=	'Houston - Carestream';
		$event['sms']		=	'Thank you! Free Tracking bonus reserved. Add\'l info email to follow within 24hrs.';
		$event['message']	=	'';
		break;
	case '+14808450667':
		$event['long_name']	=	'AADOM Annual Dental Managers Conference';
		$event['short_name']=	'AADOM Conference';
		$event['sms']		=	'Thank you for your submission! Add\'l info email to follow within 24hrs.';
		$event['message']	=	'';
		break;
	case '+18013865230':
		$event['long_name']	=	'The Team Training Institute at the Salt Palace';
		$event['short_name']=	'Team Training Institute';
		$event['sms']		=	'Thank you for your submission! Add\'l info email to follow within 24hrs.';
		$event['message']	=	'';
		break;
	case '+14355039036':
		$event['long_name']	=	'Greater New York Dental Show';
		$event['short_name']=	'NYC Dental Show';
		$event['sms']		=	'Thanks for contacting 123 Postcards! We look forward to driving new patients to your practice with the best dental direct mail program on the planet!';
		$event['smssecond']	=	'Check out this introduction video from 123 Postcards for program details: http://tinyurl.com/123postcards';
		$event['message']	=	'';
		break;
	case '+14355724443':
		$event['long_name']	=	'DentalTown Webinar';
		$event['short_name']=	'DentalTown Webinar';
		$event['sms']		=	'Your FREE 90 day tracking is now reserved in your name! You will receive an email confirmation soon. For questions, feel free to contact us at 435-572-4443.';
		$event['message']	=	'<h3>Offer Reserved: FREE 90 day tracking</h3>';
		$event['type']		=	'call-tracking-only';
		break;
	case '+14352140114':
		$event['long_name']	=	'Bob Spiel Presentation';
		$event['short_name']=	'Bob Spiel Presentation';
		$event['sms']		=	'Your FREE 90 day tracking is now reserved in your name! You will receive an email confirmation soon. For questions, feel free to contact us at 435-214-0114.';
		$event['message']	=	'<h3>Offer Reserved: FREE 90 day tracking - Bob Spiel</h3>';
		$event['type']		=	'call-tracking-only';
		break;
	default:
		$event['long_name']	=	'Event Name';
		$event['short_name']=	'Event Name';
		$event['sms']		=	'Thank you for contacting 123 Postcards! Add\'l info email to follow within 24hrs.';
		$event['smssecond']	=	'Check out this introduction video from 123 Postcards for program details: http://tinyurl.com/123postcards';
		$event['message']	=	'';
		break;
}

?>