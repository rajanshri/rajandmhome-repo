<?php

add_action( 'wp_ajax_nopriv_sendContactForm', 'sendContactForm' );
add_action( 'wp_ajax_sendContactForm', 'sendContactForm');

function sendContactForm(){

	$ex_time_start	=	microtime(true);

	$logfile	=	THEME_DIR .'/logs/submissions_log.txt';
	write_log($logfile,'new_action',"New lead submitted via contact form");

	// Kill form if bot fills fields
	$area_length = strlen($_REQUEST['phoneArea']);
	$pref_length = strlen($_REQUEST['phonePrefix']);
	$exch_length = strlen($_REQUEST['phoneExchange']);
	$honeypot	 = $_REQUEST['sticky'];
	
	if(
		$area_length != 3 ||
		$_REQUEST['phoneArea'] == 'XXX' ||
		$pref_length != 3 ||
		$_REQUEST['phonePrefix'] == 'XXX' || 
		$exch_length != 4 ||
		$_REQUEST['phoneExchange'] == 'XXXX' ||
		$honeypot	== 'on'
	) {
		die();
	}
	
	// Kill form if accessed directly and not in development environment
	if( !is_production() ) {
		$refering	= parse_url( $_SERVER['HTTP_REFERER'] );
		if( $refering['host'] != $_SERVER['HTTP_HOST'] ) {
			die();
		}
	}
	
	// Akismet spam filtering
	require_once(THEME_DIR . '/inc/Akismet/Akismet.class.php');
	
	$akismet = new Akismet( get_site_url(), 'beb589950138' );
	$akismet->setCommentAuthor($name);
	$akismet->setCommentAuthorEmail($email);
	$akismet->setCommentContent($questions);
	$akismet->setPermalink($_SERVER['HTTP_REFERER']);
	
	$practice	=	$_REQUEST['practice'];
	$firstName	=	$_REQUEST['firstName'];
	$lastName	=	$_REQUEST['lastName'];
	$name		=	$firstName . ' ' . $lastName;
	$phone		=	'(' . $_REQUEST['phoneArea'].') '.$_REQUEST['phonePrefix'].'-'.$_REQUEST['phoneExchange'];
	$email		=	$_REQUEST['emailAddress'];
	if( isset($_REQUEST['comments']) ) {
		$questions	=	$_REQUEST['comments'];
	} else {
		$questions	=	'';
	}
	if( $questions == 'Enter Comments/Questions (optional)' ) : $questions = ''; endif;
	$street		=	$_REQUEST['street'];
	if( $street == 'Enter street address' || !isset($_REQUEST['street']) ) : $street = ''; endif;
	$street2	=	$_REQUEST['street2'];
	if( $street2 == 'Enter street address (cont.)' || !isset($_REQUEST['street2']) ) : $street2 = ''; endif;
	$suite		=	$_REQUEST['suite'];
	if( $suite == 'Enter suite number' || !isset($_REQUEST['suite']) ) : $suite = ''; endif;
	$city		=	$_REQUEST['city'];
	if( $city == 'Enter city' || !isset($_REQUEST['city']) ) : $city = ''; endif;
	$state		=	$_REQUEST['state'];
	if( $state == '--' || !isset($_REQUEST['state']) ) : $state = ''; endif;
	$zip		=	$_REQUEST['zip'];
	if( $zip == 'Enter zip'|| !isset($_REQUEST['zip'])  ) : $zip = ''; endif;
	$spam		=	false;
	if( $akismet->isCommentSpam() ) {
		$spam	=	true;
	}
	$test	=	false;
	if( !is_production() ) {
		$test	=	true;
	}
	
	if( isset($_REQUEST['interest']) && $_REQUEST['interest'] != 'DentalMarketing' ) {
		$interest	=	ucfirst($_REQUEST['interest']);
	} else {
		$interest	=	'General';
	}
	
	if( setSourcePhone() ) {
		$source	=	stripslashes( setSourcePhone() );
	} else {
		$source	=	'Website - Consult Request';
	}
	
	if( isset($_COOKIE['srcs']) ) {
		$sources	=	stripslashes( $_COOKIE['srcs'] );
	} else {
		$sources	=	'';
	}
	
	if( isset($_COOKIE['ram']) ) {
		$ram	=	stripslashes( $_COOKIE['ram'] );
	} else {
		$ram	=	'';
	}
	
	if( isset($_COOKIE['aff']) ) {
		$aff	=	stripslashes( $_COOKIE['aff'] );
	} else {
		$aff	=	'';
	}
	
	$queryParams = array(
		'k'	=>	'qlasplAthOanoasPiufIEmoaBrlafRoaVoephletlexlubriuBroafl6dienoesP',
		'a'	=>	'46',
		'source'	=>	$source,
		'sources'	=>	$sources,
		'email'	=>	$email,
		'phone'	=>	$phone,
		'fname'	=>	$firstName,
		'lname'	=>	$lastName,
		'pname'	=>	$practice,
		'questions'	=>	$questions,
		'street'	=>	$street,
		'street2'	=>	$street2,
		'suite'	=>	$suite,
		'city'	=>	$city,
		'state'	=>	$state,
		'zip'	=>	$zip,
		'spam'	=>	$spam,
		'search'	=>	true,
		'test'	=>	$test,
		'aff' => $aff,
		'ram' => $ram,
		'interest'	=>	$interest
	);
	
	$url	=	FORM_API . http_build_query($queryParams);
	write_log($logfile,'new_section',"Submission Url: $url");
	
	$curl	=	curl_init($url);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	 
	$json_response	=	curl_exec($curl);
	write_log($logfile,'new_section',"Submission response: $json_response");
	
	$response		=	json_decode($json_response);
	
	curl_close($curl);
	
	if( $response->status == 'success' ) {
		$status	=	'success';
		$message = "Your request has been submitted successfully! We will contact you shortly.";
	} else {
		$status	=	'error';
		$message	=	"There was an error processing your request. Please contact us directly at 1-877-319-7772.";
	}
	
	$output	= array(
		'status'	=> $status,
		'message'	=> $message
	);
	$output = json_encode($output);
	
	echo $output;
	
	$ex_time_end	=	microtime(true);
	$execution_time = round($ex_time_end - $ex_time_start,2) . 's';
	write_log($logfile,'end_action',"Total execution time = $execution_time");
	
	die(); 
	
}

?>