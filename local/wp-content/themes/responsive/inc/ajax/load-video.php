<?php
add_action( 'wp_ajax_nopriv_cbLoadVid', 'cbLoadVid' );
add_action( 'wp_ajax_cbLoadVid', 'cbLoadVid');

function cbLoadVid() {

?>

<div>
	<embed src="//www.youtube.com/v/Hp_UESrTGbg?hl=en_US&amp;version=3&amp;rel=0" type="application/x-shockwave-flash" width="815" height="350" allowscriptaccess="always" allowfullscreen="true"></embed>
</div>
<div class="lightboxTray" style="margin-top:-5px;">
			<div class="trayTop">
				<h2 class="trayHeadline">Like this video?</h2>
				<h3 class="trayCTA">Let's get started for your practice</h3>
				<div class="clear"></div>
			</div>
			<div class="trayForm trayContact">
			
				<!-- BEGIN FORM -->
				<form action="<?php echo HOME_URI; ?>/wp-content/admin-ajax.php" method="post">
					<div class="trayFormInput">
						<label for="practice">Practice Name: <input type="text" name="practice" class="practiceName" placeholder="Enter practice name"/></label>
					</div>
					<div class="trayFormInput">
						<label for="firstName">Your Name: <input type="text" name="firstName" class="firstName" placeholder="Enter first" /> <input type="text" name="lastName" class="lastName" placeholder="and last name"/></label>
					</div>
					<div class="trayFormInput">
						<label for="phoneArea">Phone: (<input type="text" name="phoneArea" class="phoneArea" size="3" placeholder="XXX"/>) <input type="text" name="phonePrefix" class="phonePrefix" size="3" placeholder="XXX" /> - <input type="text" name="phoneExchange" size="4" class="phoneExchange" placeholder="XXX" /></label>
					</div>
					<div class="trayFormInput">
						<label for="emailAddress">Email: <input type="text" name="emailAddress" class="emailAddress" placeholder="Enter email address" /></label>
					</div>
					<div class="trayFormInput">
						<label for="reserve">Check area availability: <input type="checkbox" name="reserve" class="reserve checkbox" /></label>
					</div>
						<div class="reserve-show">
							<div class="trayFormInput">
								<label for="street">Street Address:</label>
								<input type="text" name="street" class="street" value="Enter street address" />
								<input type="text" name="street2" class="street2" value="Enter street address (cont.)" />
								<input type="text" name="suite" class="suite" value="Enter suite number" />
							</div>
							<div class="trayFormInput">
								<label for="city">City:</label>
								<input type="text" name="city" class="text city" value="Enter city" />
								<label for="state">State:</label>
								<?php statesDropdown(); ?>
								<label for="zip">Zip:</label>
								<input type="text" name="zip" class="text zip" value="Enter zip" />
							</div>
						</div>
					<div class="trayFormInput traySubmit">
						<input type="hidden" name="action" value="sendContactForm" />
						<input type="submit" value="Submit" class="submit" />
						<div class="trayArrow"></div>
					</div>
					<div class="clear"></div>
					<div id="traySendResults" class="response-output"></div>
				</form>
			<!-- END FORM -->
			</div>
		</div>

<!-- BEGIN FROM JS -->
	<script type="text/javascript">
function traySendSubmit( formData, jqForm, options ) {
	
	jQuery('#traySendResults').removeClass('error').addClass('loading').animate({ 'opacity': 'show', 'height': 'show' }).html('Loading...');
	
	var valErrors = new Array();
	
	if( jQuery('.trayContact .practiceName').val() == '' || jQuery('.trayContact .practiceName').val() == 'Enter practice name' ) {
		valErrors.push('Practice Name');
		jQuery('.trayContact .practiceName').addClass('error');
	}
	if( 
		jQuery('.trayContact .firstName').val() == '' || 
		jQuery('.trayContact .firstName').val() == 'Enter first' ||
		jQuery('.trayContact .lastName').val() == '' || 
		jQuery('.trayContact .lastName').val() == 'Enter last'
	) {
		valErrors.push('First and Last Name');
		jQuery('.trayContact .firstName').addClass('error');
		jQuery('.trayContact .lastName').addClass('error');
	}
	if( 
		jQuery('.trayContact .phoneArea').val() == '' || 
		jQuery('.trayContact .phoneArea').val() == 'XXX' ||
		jQuery('.trayContact .phonePrefix').val() == '' ||
		jQuery('.trayContact .phonePrefix').val() == 'XXX' ||
		jQuery('.trayContact .phoneExchange').val() == '' ||
		jQuery('.trayContact .phoneExchange').val() == 'XXXX'
	) {
		valErrors.push('Phone Number');
		jQuery('.trayContact .phoneArea, .trayContact .phonePrefix, .trayContact .phoneExchange').addClass('error');
	}
	if( jQuery('.trayContact .phoneArea').val().length != 3 ||
		jQuery('.trayContact .phonePrefix').val().length != 3 ||
		jQuery('.trayContact .phoneExchange').val().length != 4 
	
	) {
		valErrors.push('Phone Number (Check Length)');
		jQuery('.trayContact .phoneArea, .trayContact .phonePrefix, .trayContact .phoneExchange').addClass('error');
	}
	if( jQuery('.trayContact .emailAddress').val() == '' || 
		jQuery('.trayContact .emailAddress').val() == 'Enter email address'
	) {
		valErrors.push('Email Address');
		jQuery('.trayContact .emailAddress').addClass('error');
	}
	var emailAddress = jQuery('.trayContact .emailAddress').val();
	if( !emailReg.test(emailAddress) ) {
		valErrors.push('Email Address (Appears Invalid)');
		jQuery('.trayContact .emailAddress').addClass('error');
	}
	if( jQuery('.trayContact input.reserve').is(':checked') ) {
		if( jQuery('.trayContact input.street').val() == '' ||
			jQuery('.trayContact input.street').val() == 'Enter street address'
		) {
			valErrors.push('Street Address');
			jQuery('.trayContact input.street').addClass('error');
		}
		if( jQuery('.trayContact input.city').val() == '' ||
			jQuery('.trayContact input.city').val() == 'Enter city'
		) {
			valErrors.push('City');
			jQuery('.trayContact input.city').addClass('error');
		}
		if( jQuery('.trayContact input.zip').val() == '' ||
			jQuery('.trayContact input.zip').val() == 'Enter zip'
		) {
			valErrors.push('Zip');
			jQuery('.trayContact input.zip').addClass('error');
		}
	}
	
	if( valErrors.length > 0 ) {
		var valMessage = 'Please fill out the following required fields: ';
		if( valErrors.length > 1 ) {
			for( i = 0; i < valErrors.length; i++ ) {
				if( i < ( valErrors.length - 1 ) ) {
					valMessage += valErrors[i] + ', ';
				} else {
					valMessage += 'and ' + valErrors[i] + '.';
				}
			}
		} else {
			valMessage += valErrors[0];
		}
		jQuery('#traySendResults').removeClass('loading').addClass('error').html(valMessage);
		console.info('errorMessage',valMessage);
		
		_gaq.push(['_trackPageview', '/virtual/gallery-tray-form/validation-errors']);
		
		return false;
	} else {
		jQuery('.trayContact input.submit').attr('disabled','disabled').addClass('disabled');
		console.log('Gallery tray form submitted successfully');
	}
	
}

function traySendResponse( data ) {

	console.info('response data',data);

	jQuery('.sideContact input.submit').removeAttr('disabled').removeClass('disabled');

	_gaq.push(['_trackPageview', '/virtual/gallery-tray-form/success']);
	
	jQuery('#traySendResults').removeClass('loading').addClass('success').html(data.message);
	setTimeout(function(){
		jQuery('#traySendResults').animate({ 'height': 'hide', 'opacity': 'hide' }, 
			function(){
				jQuery(this).removeClass('success').html('Loading...');
			}
		);
	}, 8000000);
	
}
jQuery(document).ready(function($){
	// Free Consultation Form Defaults
	var trayDefaults = [
		'Enter practice name',
		'Enter first',
		'and last name',
		'XXX',
		'XXXX',
		'Enter email address',
		'Enter Comments/Questions (optional)',
		'Enter street address',
		'Enter street address (cont.)',
		'Enter suite number',
		'Enter city',
		'Enter zip'
	];
	$('.trayContact').deloClear({
		fieldDef: trayDefaults,
		preColor: '#CECECE'
	});
	
	// Free Consultaion request form from design page lightbox
	var trayOptions = {
		dataType: 'json',
		beforeSubmit: traySendSubmit,
		success: traySendResponse
	};
	
	$('.trayForm form').ajaxForm(trayOptions);

});
</script>
<script type="text/javascript">
			
		jQuery(document).ready(function($){
			$('input.reserve').change(function(){
			
				if($(this).is(':checked')) {
					$(this).parents('div').siblings('.reserve-show').show();
					setTimeout("jQuery.fn.colorbox.resize();",50);
				} else {
					$(this).parents('div').siblings('.reserve-show').hide();
					setTimeout("jQuery.fn.colorbox.resize();",50);
				}
				
			});
			$('.trayForm input.submit').click(function(){
				setTimeout("jQuery.fn.colorbox.resize();",600);	
			});
		});
		
		
		
	</script>
	
	<?php
	
	die();
}

?>