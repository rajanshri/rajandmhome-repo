<?php

add_action( 'wp_ajax_nopriv_sendVideoForm', 'sendVideoForm' );
add_action( 'wp_ajax_sendVideoForm', 'sendVideoForm');

function sendVideoForm(){

	date_default_timezone_set('America/Denver');

	// Kill form if bot fills fields
	$area_length = strlen($_REQUEST['phoneArea']);
	$pref_length = strlen($_REQUEST['phonePrefix']);
	$exch_length = strlen($_REQUEST['phoneExchange']);
	$honeypot	 = $_REQUEST['sticky'];
	
	if(
		$area_length != 3 ||
		$_REQUEST['phoneArea'] == 'XXX' ||
		$pref_length != 3 ||
		$_REQUEST['phonePrefix'] == 'XXX' || 
		$exch_length != 4 ||
		$_REQUEST['phoneExchange'] == 'XXXX' ||
		$honeypot	== 'on'
	) {
		die();
	}
	
	// Kill form if accessed directly
	$refering	= parse_url( $_SERVER['HTTP_REFERER'] );
	if( $refering['host'] != $_SERVER['HTTP_HOST'] ) {
		die();
	}
	
	$message = "Your request has been submitted successfully! We will contact you shortly.";
	
	// Akismet spam filtering
	require_once(THEME_DIR . '/inc/Akismet/Akismet.class.php');
	
	$akismet = new Akismet( get_site_url(), 'beb589950138' );
	$akismet->setCommentAuthor($name);
	$akismet->setCommentAuthorEmail($email);
	$akismet->setCommentContent($questions);
	$akismet->setPermalink($_SERVER['HTTP_REFERER']);
	
	$practice	=	stripslashes( $_REQUEST['practice'] );
	$firstName	=	stripslashes( $_REQUEST['firstName'] );
	$lastName	=	stripslashes( $_REQUEST['lastName'] );
	$name		=	$firstName . ' ' . $lastName;
	$phone		=	'(' . $_REQUEST['phoneArea'].') '.$_REQUEST['phonePrefix'].'-'.$_REQUEST['phoneExchange'];
	$email		=	stripslashes( $_REQUEST['emailAddress'] );
	
	if( isset($_REQUEST['source']) ) {
		$source	=	stripslashes( $_REQUEST['source'] );
	} else {
		$source	=	'123Postcards - Website - Consult Request';
	}
	
	// Mandrill for email
	require_once(THEME_DIR . '/inc/Mandrill_lib/config.php');
	require_once(THEME_DIR . '/inc/Mandrill_lib/Mandrill.php');
	
	$messagebody=	"<h1>New QuickDraw Lead from $name</h1>"
				.	"<table style=\"margin-bottom: 25px;\"><tbody>"
				.	"<tr><td>Practice:</td><td>$practice</td></tr>"
				.	"<tr><td>Name:</td><td>$name</td></tr>"
				.	"<tr><td>Phone:</td><td>$phone</td></tr>"
				.	"<tr><td>Email:</td><td>$email</td></tr>"
				.	"</tbody></table>"
				
				.	"<p style=\"font-size: 10px;color: #A0A0A0\">This email was generated automatically by www.dentalmarketing.net on " . date('n/j/Y g:i') . "</p>";
				
	$messagebody	=	str_replace("'", '"', $messagebody);
				
	
				
	$mandrill_json = '{"type":"messages","call":"send","message":{"html": "'. addslashes($messagebody) .'", "text": "example text", "subject": "New QuickDraw Video Lead", "from_email": "no-reply@123postcards.com", "from_name": "123 Postcards Sales", "to":[{"email": "kc@tidesarerising.com", "name": "KC Holmes"}],"bcc_address":"mark@123postcards.com","track_opens":true,"track_clicks":true,"auto_text":true,"url_strip_qs":true,"tags":["sales","new lead","internal","quick draw"]}}';
	
	$mand_resp	=	Mandrill::call( (array) json_decode($mandrill_json) );
	
	// Twilio for SMS
	require_once(THEME_DIR . '/inc/Twilio_lib/config.php');
	require_once(THEME_DIR . '/inc/Twilio_lib/Twilio.php');
	
	$client = new Services_Twilio(TW_ACCOUNT_SID,TW_AUTH_TOKEN);
	$sms = "New QuickDraw Videos Lead: $name - $phone";
	$numbers	=	array(
		'+19252860014',
		'+14355036337'
	);
	foreach( $numbers as $to ) {
		$client->account->sms_messages->create(
			'+14352144195', // From a valid Twilio number
			$to, // To a valid phone number
			$sms // Message content
		);
	}
	
	$output	=	array(
		'status'	=> 'success',
		'message'	=>	'Thank you for contacting us! We will get in touch with you as soon as possible to talk more about wowing your patients with our QuickDraw videos!'
	);
	
	$response	=	json_encode($output);
	
	echo $response;
	
	die(); 
	
}

?>