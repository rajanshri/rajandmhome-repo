<?php
add_action( 'wp_ajax_nopriv_cbLoadContact', 'cbLoadContact' );
add_action( 'wp_ajax_cbLoadContact', 'cbLoadContact');

function cbLoadContact() {
?>
<style>
.contactLightbox {
	float: left;
	width: 370px;
	margin-right: 25px;
	margin-top: 10px;
	margin-left:-7px;
	position: relative;
}
.contactLightbox h2 {
	margin-bottom: 0;
	padding: 12px 4px;
	font-size: 16px;
	font-weight: bold;
	text-align: center;
	border-radius: 3px 3px 0 0;
	-webkit-border-radius: 3px 3px 0 0;
	-moz-border-radius: 3px 3px 0 0;
	color: #FFFFFF;
	border-color: rgb(247, 147, 38);
	background-color: #f79326;
	filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#f79326', endColorstr='#dd710c');
	background: -webkit-gradient(linear, 0% 0%, 0% 100%, from(rgb(247, 147, 38)), to(rgb(221, 113, 12)));
	background: -moz-linear-gradient(top,  rgb(247, 147, 38),  rgb(221, 113, 12));
	text-align: center;
	text-shadow: 0px 1px 1px rgba(0,0,0,1);
}
.contactLightbox label {
	font-weight: bold;
}
.contactLightbox p {
	margin-bottom: 5px;
	text-align: left;
	color:#000;
}
.contactLightbox .formBody {
	min-height: 322px;
	padding: 5px 15px 10px;
	border: 1px solid #DADADA;
	position: relative;
	text-align: center;
}
.contactLightbox input.submit {
	margin-bottom: 0;
	padding: 12px 4px;
	font-size: 20px;
	font-weight: bold;
	border-radius: 3px;
	-webkit-border-radius: 3px;
	-moz-border-radius: 3px;
	color: #FFFFFF;
	border-color: rgb(134, 67, 11);
	background-color: #763805;
	filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#763805', endColorstr='#542702');
	background: -webkit-gradient(linear, 0% 0%, 0% 100%, from(rgb(118, 56, 5)), to(rgb(84, 39, 2)));
	background: -moz-linear-gradient(top,  rgb(118, 56, 5),  rgb(84, 39, 2));
	text-decoration: none;
	cursor: pointer;
	display: block;
	width: 100%;
	text-align: center;
	text-shadow: 0px 1px 1px rgba(0,0,0,1);
	line-height: 1;
	-moz-box-shadow: 0 1px 3px rgba(0, 0, 0, .8);
	-webkit-box-shadow: 0 1px 3px rgba(0, 0, 0, .8);
	box-shadow: 0 1px 3px rgba(0, 0, 0, .8);
	position: relative;
	border-style: solid;
}
.contactLightbox input.submit.disabled {
	border-color: rgba(108,108,108,1);
	background-color: #b6b6b6;
	filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#b6b6b6', endColorstr='#a9a9a9');
	background: -webkit-gradient(linear, 0% 0%, 0% 100%, from(rgb(173,173,173)), to(rgb(135,135,135)));
	background: -moz-linear-gradient(top,  rgb(173,173,173),  rgb(135,135,135));
}
.contactLightbox input.submit:hover {
	top: 1px;
}
.contactLightbox input.submit:active {
	top: 2px;
}
.contactLightbox .response-output {
	position: absolute;
	top: 400px;
	width: 207px;
	left: 10px;
	margin: 0;
}
.contactLightbox.fixed {
	position: fixed;
}

.contactLightbox select {
	padding: 3px;
	font-size: 12px;
	border: 1px solid #CECECE;
	width: 310px;
	outline: none;
}
.contactLightbox input, .contactLightbox textarea{
	padding: 3px;
	font-size: 12px;
	width: 310px;
	border: 1px solid #CECECE;
	width: 310px;
}
.contactLightbox .firstName {
	width: 237px;
	margin-right: 5px;
}
.contactLightbox .lastName {
	width: 237px;
}
.contactLightbox .phone {
	margin-left: 0;
	margin-right: 0;
}
.contactLightbox .phoneArea,
.contactLightbox .phonePrefix,
.contactLightbox .phoneExchange,
.landingPageForm .phoneArea,
.landingPageForm .phonePrefix,
.landingPageForm .phoneExchange {
	margin: 0;
	width: 24px;
}
.contactLightbox .phoneExchange,
.landingPageForm .phoneExchange {
	width: 32px;
}
.contactLightbox .validate input {
	width: 25px;
	margin-right: 5px;
	padding: 6px 8px 6px 8px;
	display: inline-block;
}
.contactLightbox .check-status {
	padding: 3px 7px;
	width: 135px;
	display: inline-block;
}
.contactLightbox .check-status-error {
	border: 2px solid red;
	background: #FFCFCF 4px center url('images/form-validation-error-small.png') no-repeat;
	padding-left: 30px;
}
.contactLightbox .check-status-success {
	border: 2px solid green;
	background: #b3f6b3 4px center url('images/form-validation-good-small.png') no-repeat;
	padding-left: 30px;
}
.contactLightbox input.focused, .contactLightbox textarea.focused {
	border-color: #fc7101;
}
.reserve {
	color: #fc7101;
	font-weight: bold;
}
.contactLightbox .reserve input {
	width: auto;
	display: inline;
}
.contactLightbox .city {
	margin-bottom: 8px;
}
.contactLightbox .statesDropdown {
	width: 60px;
}
.contactLightbox .zip {
	width: 80px;
}
.contactLightbox .reserve-show,
#roiContact .reserve-show,
.landingPageForm .reserve-show {
	display: none;
}
</style>

<div class="lightboxTray" style="margin-top:-5px;">
  <div class="trayForm trayContact">
		<div class="contactLightbox">	
	  <!-- BEGIN FORM -->
	  <form action="<?php echo HOME_URI; ?>/wp-admin/admin-ajax.php">
				<h2>Request a Free Consultation</h2>
				<div class="formBody">
				<p class="practice"><label for="practice">Practice Name*</label>:<br /> 
					<input type="text" name="practice" class="text practiceName" placeholder="Enter practice name" /></p>
				<p class="name"><label for="firstName">First Name*</label>:<br />
					<input type="text" name="firstName" class="text firstName" placeholder="Enter first name"  />
                    </p>
				<p class="name">
				  <label for="lastName">Last Name*</label>:<br />
					<input type="text" name="lastName" class="text lastName" placeholder="Enter last name"  />
				</p>
				<p class="phone"><label for="phone">Phone Number*</label>:<br /> 
				  (<input type="text" class="phoneArea" name="phoneArea" placeholder="XXX" size="3" />) <input type="text" class="phonePrefix" name="phonePrefix" placeholder="XXX" size="3" /> - <input type="text" class="phoneExchange" name="phoneExchange" placeholder="XXXX" size="4" /></p>
				<p class="sticky"><label for="sticky">Do you want more information?</label><br />
					<input type="checkbox" class="sticky" name="sticky" /></p>
				<p class="email"><label>Email Address*</label>:<br /> 
					<input type="text" name="emailAddress" class="text emailAddress" placeholder="Enter email address" /></p>
				<p class="practice"><label for="comments">Comments/Questions:</label><br />
					<textarea name="comments" cols="40" rows="5" placeholder="Enter Comments/Questions (optional)"></textarea></p>
					
				<p class="reserve"><label for="reserve">Check area availability: <input type="checkbox" name="reserve" class="reserve" placeholder="checked" /></label></p>
				
				<div class="reserve-show">
				
					<p class="street"><label for="street">Street Address:</label><br />
						<input type="text" name="street" class="text street " placeholder="Enter street address" /></p>
						
					<p class="street2"><label for="street2">Street Address (cont.):</label><br />
						<input type="text" name="street2" class="text street2" placeholder="Enter street address (cont.)" /></p>
						
					<p class="suite"><label for="suite">Suite:</label><br />
						<input type="text" name="suite" class="text suite" placeholder="Enter suite number" /></p>
						
					<p class="city"><label for="city">City, State, Zip:</label><br />
						<input type="text" name="city" class="text city" placeholder="Enter city" />
						<?php statesDropdown(); ?>
						<input type="text" name="zip" class="text zip" placeholder="Enter zip" />
				  </p>
					
				</div>
				
				<input type="hidden" placeholder="sendContactForm" name="action" />
				<input type="submit" placeholder="Submit »" class="submit"></div>
				
				<div id="sideSendResults" class="response-output"></div>
			</form>
	<!-- END FORM -->
         </div>
         </div>
		</div>

<!-- BEGIN FROM JS -->
<script type="text/javascript">
//homepage slider functions

function sideSendSubmit( formData, jqForm, options ) {

	console.log(formData);
	console.log(jqForm);
	console.log(options);
	
	// Honeypot spam filter
	if( jQuery('.sideContact input.sticky').is(':checked') ) {
		alert('Are you a robot? I think so... ');
		return false;
	}
	
	jQuery('#sideSendResults').removeClass('error').addClass('loading').animate({ 'opacity': 'show', 'height': 'show' }).html('Loading...');
	
	var valErrors = new Array();
	
	if( jQuery('.sideContact .practiceName').val() == '' || jQuery('.sideContact .practiceName').val() == 'Enter practice name' ) {
		valErrors.push('Practice Name');
		jQuery('.sideContact .practiceName').addClass('error');
	}
	if( 
		jQuery('.sideContact .firstName').val() == '' || 
		jQuery('.sideContact .firstName').val() == 'Enter first' ||
		jQuery('.sideContact .lastName').val() == '' || 
		jQuery('.sideContact .lastName').val() == 'and last name'
	) {
		valErrors.push('First and Last Name');
		jQuery('.sideContact .firstName').addClass('error');
		jQuery('.sideContact .lastName').addClass('error');
	}
	if( 
		jQuery('.sideContact .phoneArea').val() == '' || 
		jQuery('.sideContact .phoneArea').val() == 'XXX' ||
		jQuery('.sideContact .phonePrefix').val() == '' ||
		jQuery('.sideContact .phonePrefix').val() == 'XXX' ||
		jQuery('.sideContact .phoneExchange').val() == '' ||
		jQuery('.sideContact .phoneExchange').val() == 'XXXX'
	) {
		valErrors.push('Phone Number');
		jQuery('.sideContact .phoneArea, .sideContact .phonePrefix, .sideContact .phoneExchange').addClass('error');
	}
	if( jQuery('.sideContact .phoneArea').val().length != 3 ||
		jQuery('.sideContact .phonePrefix').val().length != 3 ||
		jQuery('.sideContact .phoneExchange').val().length != 4 
	
	) {
		valErrors.push('Phone Number (Check Length)');
		jQuery('.sideContact .phoneArea, .sideContact .phonePrefix, .sideContact .phoneExchange').addClass('error');
	}
	if( jQuery('.sideContact .emailAddress').val() == '' || 
		jQuery('.sideContact .emailAddress').val() == 'Enter email address'
	) {
		valErrors.push('Email Address');
		jQuery('.sideContact .emailAddress').addClass('error');
	}
	var emailAddress = jQuery('.sideContact .emailAddress').val();
	if( !emailReg.test(emailAddress) ) {
		valErrors.push('Email Address (Appears Invalid)');
		jQuery('.sideContact .emailAddress').addClass('error');
	}
	if( jQuery('.sideContact input.reserve').is(':checked') ) {
		if( jQuery('.sideContact input.street').val() == '' ||
			jQuery('.sideContact input.street').val() == 'Enter street address'
		) {
			valErrors.push('Street Address');
			jQuery('.sideContact input.street').addClass('error');
		}
		if( jQuery('.sideContact input.city').val() == '' ||
			jQuery('.sideContact input.city').val() == 'Enter city'
		) {
			valErrors.push('City');
			jQuery('.sideContact input.city').addClass('error');
		}
		if( jQuery('.sideContact input.zip').val() == '' ||
			jQuery('.sideContact input.zip').val() == 'Enter zip'
		) {
			valErrors.push('Zip');
			jQuery('.sideContact input.zip').addClass('error');
		}
	}
	
	if( valErrors.length > 0 ) {
		var valMessage = 'Please fill out the following required fields: ';
		if( valErrors.length > 1 ) {
			for( i = 0; i < valErrors.length; i++ ) {
				if( i < ( valErrors.length - 1 ) ) {
					valMessage += valErrors[i] + ', ';
				} else {
					valMessage += 'and ' + valErrors[i] + '.';
				}
			}
		} else {
			valMessage += valErrors[0];
		}
		jQuery('#sideSendResults').removeClass('loading').addClass('error').html(valMessage);
		console.info('errorMessage',valMessage);
		
		_gaq.push(['_trackPageview', '/virtual/side-form/validation-errors']);
		
		return false;
	} else {
		jQuery('.sideContact input.submit').attr('disabled','disabled').addClass('disabled');
		console.log('Sidebar form submitted successfully');
	}
	
}

function sideSendResponse( data ) {

	console.info('data',data);

	jQuery('.sideContact input.submit').removeAttr('disabled').removeClass('disabled');

	_gaq.push(['_trackPageview', '/virtual/side-form/success']);
	
	jQuery('#sideSendResults').removeClass('loading').addClass(data.status).html(data.message);
	setTimeout(function(){
		jQuery('#sideSendResults').animate({ 'height': 'hide', 'opacity': 'hide' }, 
			function(){
				jQuery(this).removeClass('success').html('Loading...');
			}
		);
	}, 4000);
	
}
jQuery(window).load(function(){

	//execute slides
	if( jQuery('#featured #slider').is('*') ) {
	
		slideone();
	
		setTimeout("slidetwo();", 4000);
		
		setTimeout("slidethree();", 8500);
		
		setTimeout("slidefour();", 13000);
		
		setTimeout("slidefive();", 18000);
		
		setTimeout("slidefivetwo();", 21500);
		
	}
	
});

jQuery(document).ready(function($){

	// Slider CTA button movement
	$('#slider a').hover(
		function(){
			var bottom = parseInt( $(this).css('bottom') );
			
			var lower = bottom - 1;
			
			$(this).css({ 'bottom': lower });
			
		},
		function(){
			var lower = parseInt( $(this).css('bottom') );
			
			var bottom = lower + 1;
			
			$(this).css({ 'bottom': bottom });
		}
	);
	$('#slider a').mousedown(function(){
		var lower = parseInt( $(this).css('bottom') );
		
		var active = lower - 1;
		
		$(this).css({ 'bottom': active });
	});
	$('#slider a').mouseup(function(){
		var active = parseInt( $(this).css('bottom') );
		
		var lower = active + 1;
		
		$(this).css({ 'bottom': lower });
	});

	// Free Consultation Form Defaults
	var sideDefaults = [
		'Enter practice name',
		'Enter first',
		'and last name',
		'XXX',
		'XXXX',
		'Enter email address',
		'Enter Comments/Questions (optional)',
		'Enter street address',
		'Enter street address (cont.)',
		'Enter suite number',
		'Enter city',
		'Enter zip'
	];
	$('.sideContact').deloClear({
		fieldDef: sideDefaults,
		preColor: '#CECECE'
	});
	
	// Free Consultaion request form from sidebar
	var sideOptions = {
		dataType: 'json',
		beforeSubmit: sideSendSubmit,
		success: sideSendResponse
	};
	
	$('.sideContact form').ajaxForm(sideOptions);
	
	$('input.reserve').change(function(){
	
		if($(this).is(':checked')) {
			$('.reserve-show').slideDown();
		} else {
			$('.reserve-show').slideUp();
		}
		
	});
	
	// Homepage GA events
	$('#calcLink').click(function(){
		if( $(this).hasClass('animation') ) {
			_gaq.push(['_trackEvent', 'Clicks', 'Homepage', 'Intro Video - During animation']);
		} else {
			_gaq.push(['_trackEvent', 'Clicks', 'Homepage', 'Intro Video - After animation']);
		}
	});
	$('#galleryLink').click(function(){
		if( $(this).hasClass('animation') ) {
			_gaq.push(['_trackEvent', 'Clicks', 'Homepage', 'Postcard Gallery - During animation']);
		} else {
			_gaq.push(['_trackEvent', 'Clicks', 'Homepage', 'Postcard Gallery - After animation']);
		}
	});
	$('#contactLink').click(function(){
		_gaq.push(['_trackEvent', 'Clicks', 'Homepage', 'ROI Calculator']);
	});
	$('#pricingLink').click(function(){
		_gaq.push(['_trackEvent', 'Clicks', 'Homepage', 'Postcard Pricing']);
	});

});
</script>

	
	<?php
	
	die();
}

?>