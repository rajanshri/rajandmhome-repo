<?php

wp_localize_script( 'vidGallery', 'MyAjax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );

add_action( 'wp_ajax_nopriv_vidGallery', 'vidGallery' );
add_action( 'wp_ajax_vidGallery', 'vidGallery' );


function vidGallery() {
	$id = $_REQUEST['youtubeID'];

?>
	<div class="videoViewer">
		<! -- YOUTUBE VIDEO IS HERE! -->
		<iframe width="840" height="472" src="//www.youtube.com/embed/<?php echo $id; ?>?color=white&vq=hd720&rel=0&showinfo=0&autohide=1&autoplay=1 autoplay" frameborder="0" allowfullscreen></iframe>
		
		<! -- START FORM TITLES -->
		<div class="lightboxTray">
			<div class="trayTop">
				<h2 class="vidFormHeadline">Like this video? Contact us below!</h2>
				<div class="clear"></div>
			</div>
			<div class="trayForm trayContact">
			
				<!-- BEGIN LIGHTBOX FORM -->
				<form action="<?php echo HOME_URI; ?>/wp-admin/admin-ajax.php">
					<div class="trayFormInput">
						<label for="practice">Practice Name: <input type="text" name="practice" class="practiceName" value="Enter practice name" /></label>
					</div>
					<div class="trayFormInput">
						<label for="firstName">Your Name: <input type="text" name="firstName" class="firstName" value="Enter first" /> <input type="text" name="lastName" class="lastName" value="and last name" /></label>
					</div>
					<div class="trayFormInput">
						<label for="phoneArea">Phone: (<input type="text" name="phoneArea" class="phoneArea" size="3" value="XXX" />) <input type="text" name="phonePrefix" class="phonePrefix" size="3" value="XXX" /> - <input type="text" name="phoneExchange" size="4" class="phoneExchange" value="XXXX" /></label>
					</div>
					<div class="trayFormInput">
						<label for="emailAddress">Email: <input type="text" name="emailAddress" class="emailAddress" value="Enter email address" /></label>
					</div>
					<div class="trayFormInput traySubmit">
						<input type="hidden" id="interest" name="interest" value="videos" />
						<input type="hidden" name="action" value="sendContactForm" />
						<input type="submit" value="Submit" class="submit" />
						<div class="trayArrow"></div>
					</div>
					<div class="clear"></div>
					<div id="traySendResults" class="response-output"></div>
				</form>
			<!-- END FORM -->
			</div>
		</div>

	</div>
	
	<script type="text/javascript">
		
		jQuery(document).ready(function($){
			$('.trayForm input.submit').click(function(){
				setTimeout("jQuery.fn.colorbox.resize();",240);	
			});
		});
		
	</script>
	
<!-- BEGIN FROM JS -->

<script type='text/javascript' src='<?php echo THEME_JS; ?>/form-gallery.js'></script>

<?php	die(); } ?>