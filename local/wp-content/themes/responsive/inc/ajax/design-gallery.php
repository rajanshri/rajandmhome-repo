<?php

wp_localize_script( 'cbGalImage', 'MyAjax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );

add_action( 'wp_ajax_nopriv_cbGalImage', 'cbGalImage' );
add_action( 'wp_ajax_cbGalImage', 'cbGalImage');

function cbGalImage() {
	$front	= $_REQUEST['front'];
	$back	= $_REQUEST['back'];
	
	$front_img = nggdb::find_image(str_replace('ngg-','',$front));
	$back_img = nggdb::find_image(str_replace('ngg-','',$back));
	
	?>
	<div class="viewerWrap">    
		<div class="cardflip">
			<ul id="card_holder" style="display: none;">
				<li><img id="card_front" src="<?php echo $front_img->imageURL; ?>" width="<?php echo $front_img->meta_data['width']; ?>" height="<?php echo $front_img->meta_data['height']; ?>" /></li>
				<li><img id="card_back" src="<?php echo $back_img->imageURL; ?>" width="<?php echo $back_img->meta_data['width']; ?>" height="<?php echo $back_img->meta_data['height']; ?>" /></li>
			</ul>
			<p class="infoNote" id="flipNote">Click on the image to see the other side.</p>
			<p class="infoNote" id="favNote"><label for="favorite">Save as favorite <input type="checkbox" id="<?php echo $front_img->pid; ?>" class="favoriteCard" name="favorite" value="<?php echo $front_img->pid .'|'. $front_img->alttext .'|'. $front_img->imageURL .'|'. $back_img->imageURL; ?>" /></label></p>
		</div>
		
		<div class="lightboxTray">
			<div class="trayTop">
				<h2 class="trayHeadline">Like this design?</h2>
				<h3 class="trayCTA">Let's get one started for your practice</h3>
				<div class="clear"></div>
			</div>
			<div class="trayForm trayContact">
			
				<!-- BEGIN FORM -->
				<form action="<?php echo HOME_URI; ?>/wp-admin/admin-ajax.php">
					<div class="trayFormInput">
						<label for="practice">Practice Name: <input type="text" name="practice" class="practiceName" value="Enter practice name" /></label>
					</div>
					<div class="trayFormInput">
						<label for="firstName">Your Name: <input type="text" name="firstName" class="firstName" value="Enter first" /> <input type="text" name="lastName" class="lastName" value="and last name" /></label>
					</div>
					<div class="trayFormInput">
						<label for="phoneArea">Phone: (<input type="text" name="phoneArea" class="phoneArea" size="3" value="XXX" />) <input type="text" name="phonePrefix" class="phonePrefix" size="3" value="XXX" /> - <input type="text" name="phoneExchange" size="4" class="phoneExchange" value="XXXX" /></label>
					</div>
					<div class="trayFormInput">
						<label for="emailAddress">Email: <input type="text" name="emailAddress" class="emailAddress" value="Enter email address" /></label>
					</div>
					<div class="trayFormInput">
						<label for="reserve">Check area availability: <input type="checkbox" name="reserve" class="reserve checkbox" /></label>
					</div>
						<div class="reserve-show">
							<div class="trayFormInput">
								<label for="street">Street Address:</label>
								<input type="text" name="street" class="street" value="Enter street address" />
								<input type="text" name="street2" class="street2" value="Enter street address (cont.)" />
								<input type="text" name="suite" class="suite" value="Enter suite number" />
							</div>
							<div class="trayFormInput">
								<label for="city">City:</label>
								<input type="text" name="city" class="text city" value="Enter city" />
								<label for="state">State:</label>
								<?php statesDropdown(); ?>
								<label for="zip">Zip:</label>
								<input type="text" name="zip" class="text zip" value="Enter zip" />
							</div>
						</div>
					<div class="trayFormInput traySubmit">
						<input type="hidden" id="interest" id="interest" name="interest" value="postcards" />
						<input type="hidden" name="action" value="sendContactForm" />
						<input type="submit" value="Submit" class="submit" />
						<div class="trayArrow"></div>
					</div>
					<div class="clear"></div>
					<div id="traySendResults" class="response-output"></div>
				</form>
			<!-- END FORM -->
			</div>
		</div>
		
	</div>
	
	<script type="text/javascript">
		var this_card_id	=	<?php echo $front_img->pid; ?>;
		
		jQuery(document).ready(function($){
			$('input.reserve').change(function(){
			
				if($(this).is(':checked')) {
					$(this).parents('div').siblings('.reserve-show').show();
					setTimeout("jQuery.fn.colorbox.resize();",50);
				} else {
					$(this).parents('div').siblings('.reserve-show').hide();
					setTimeout("jQuery.fn.colorbox.resize();",50);
				}
				
			});
			$('.trayForm input.submit').click(function(){
				setTimeout("jQuery.fn.colorbox.resize();",240);	
			});
		});
		
		
		
	</script>
	
	
<!-- BEGIN FROM JS -->

<script type='text/javascript' src='<?php echo THEME_JS; ?>/form-gallery.js'></script>
	
<?php	die(); } ?>