<?php

$firstName	=	stripslashes( $_REQUEST['firstName'] );
$lastName	=	stripslashes( $_REQUEST['lastName'] );
$name		=	$firstName . ' ' . $lastName;
$phone		=	'(' . $_REQUEST['phoneArea'].') '.$_REQUEST['phonePrefix'].'-'.$_REQUEST['phoneExchange'];
$email		=	$_REQUEST['emailAddress'];

$refFirstNames	=	$_REQUEST['refFirstName'];
$refLastNames	=	$_REQUEST['refLastName'];
$refPhoneAreas	=	$_REQUEST['refPhoneArea'];
$refPhonePrefs	=	$_REQUEST['refPhonePrefix'];
$refPhoneExchs	=	$_REQUEST['refPhoneExchange'];
$refEmails		=	$_REQUEST['refEmail'];

$count = count($refFirstNames);
$referrals = array();

for( $i = 0; $i < $count; $i++ ) {
	$referrals[$i] = array(
			'fname' => $refFirstNames[$i],
			'lname' => $refLastNames[$i],
			'phone' => '(' . $refPhoneAreas[$i] . ') ' . $refPhonePrefs[$i] . '-' . $refPhoneExchs[$i],
			'email' => $refEmails[$i]
	);
}

// Create the contact in SF via webservice
require_once '../Salesforce/createContact_Postcards_Referrals.php';


// Construct email message via Swift Mailer

require_once '../Swift_lib/swift_required.php';

// Create the Transport
$transport = Swift_SmtpTransport::newInstance('smtp.webfaction.com', 25)
	->setUsername('p123_forms')
	->setPassword('ZuwE?a+ethE$6d#e')
;

$subject = "DentalMarketing.net Referral Submission";

$messagebody	=	"<h1>Referral Submission from " . $name . '</h1>';

$messagebody	.=	"<table style=\"margin-bottom: 25px\" cellpadding=\"8\"><thead style=\"font-weight: bold;\">";
$messagebody	.=	"<tr><td>Status</td>";
$messagebody	.=	"<td>Name</td>";
$messagebody	.=	"<td>Phone</td>";
$messagebody	.=	"<td>Email</td><td></td></tr></thead>";
$messagebody	.=	"<tbody>";

foreach($referrals as $referral) {
	if( $referral['email'] != '' ) {
		$messagebody	.= '<tr><td><a href="' . $referral['url'] . '">' . $referral['sfmessage'] . '</a></td>';
		$messagebody	.= '<td>' . $referral['fname'] . ' ' . $referral['lname'] . '</td>';
		$messagebody	.= '<td>' . $referral['phone'] . '</td>';
		$messagebody	.= '<td>' . $referral['email'] . '</td>';
		$messagebody	.= '<td><a href="' . $referral['url'] . '">View in Salesforce</a></td>';
	}
}

$messagebody	.=	"</tbody>";
$messagebody	.=	"</table>";

$messagebody	.=	"<p>This message was sent on " . date( 'r') . " via the Referral Submission on DentalMarketing.net.</p>";

// Create the Mailer using your created Transport
$mailer = Swift_Mailer::newInstance($transport);

// Create a message
$message = Swift_Message::newInstance($subject)
  ->setFrom(array($email => $name))
  ->setTo(array('aaron@123postcards.com' => 'Aaron Boone'))
  ->setCc(array('mark@123postcards.com' => 'Mark Delorey'))
  ->setBody($messagebody, 'text/html')
  ;

// Send the message
$result = $mailer->send($message);

// Construct Twilio SMS

// Send a text message to all of the admins
require_once( '../Twilio_lib/Twilio.php' );

// Set our AccountSid and AuthToken from twilio.com/user/account
$AccountSid = 'ACd3622997cc01433f912cc71753549a4b';
$AuthToken = 'de0eefe0e642eb0c328596cf31634794';

// Instantiate a new Twilio Rest Client
$client = new Services_Twilio($AccountSid, $AuthToken);

// Your Twilio Number or Outgoing Caller ID
$from = '8019215865';

// make an associative array of server admins. Feel free to change/add your 
// own phone number and name here.
$people = array(
    'Mark' => '925-286-0014'
);

// Iterate over all admins in the $people array. $to is the phone number, 
// $name is the user's name
foreach ( $people as $name => $to ) {
    // Send a new outgoing SMS 
    $body = 'Referrals submitted from ' . $phone;
    $client->account->sms_messages->create($from, $to, $body);
}

$message = "Thank you so much! We'll keep you posted on the progress of your referrals.";

$output	= array(
	'message' => $message
);
$output = json_encode($output);

header('HTTP/1.1 200 OK');
echo $output;

?>