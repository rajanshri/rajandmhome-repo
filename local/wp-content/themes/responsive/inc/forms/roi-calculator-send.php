<?php

// Kill form if bot fills fields
$area_length = strlen($_REQUEST['phoneArea']);
$pref_length = strlen($_REQUEST['phonePrefix']);
$exch_length = strlen($_REQUEST['phoneExchange']);
$honeypot	 = $_REQUEST['sticky'];

if(
	$area_length != 3 ||
	$pref_length != 3 ||
	$exch_length != 4 ||
	$honeypot	== 'on'
) {
	die();
}

// Kill form if accessed directly
$refering	= parse_url( $_SERVER['HTTP_REFERER'] );
if( $refering['host'] != $_SERVER['HTTP_HOST'] ) {
	die();
}

require_once('../../../../../wp-blog-header.php');

$cardQty	=	intval( $_REQUEST['cardQty'] );
$patVal		=	intval( $_REQUEST['patVal'] );
$callRate	=	floatval( $_REQUEST['callRate'] );
$convRate	=	floatval( $_REQUEST['convRate'] );

$practice	=	stripslashes( $_REQUEST['practiceName'] );
$firstName	=	stripslashes( $_REQUEST['firstName'] );
$lastName	=	stripslashes( $_REQUEST['lastName'] );
$name		=	$firstName . ' ' . $lastName;
$position	=	$_REQUEST['position'];
$phone		=	'(' . $_REQUEST['phoneArea'].') '.$_REQUEST['phonePrefix'].'-'.$_REQUEST['phoneExchange'];
$email		=	$_REQUEST['emailAddress'];
$questions	=	stripslashes( $_REQUEST['comments'] );

$guarROI	=	200;

// Akismet spam filtering
require_once('../Akismet/Akismet.class.php');

$akismet = new Akismet( get_site_url(), 'beb589950138' );
$akismet->setCommentAuthor($name);
$akismet->setCommentAuthorEmail($email);
$akismet->setCommentContent($questions);
$akismet->setPermalink($_SERVER['HTTP_REFERER']);

// Build query strings for Spam/Ham submission
$queryParams = array(
	'blog' => get_site_url(),
	'user_ip' => $_SERVER['REMOTE_ADDR'],
	'user_agent' => $_SERVER['HTTP_USER_AGENT'],
	'referrer' => $_SERVER[''],
	'comment_type' => 'contact',
	'comment_author' => $name,
	'comment_author_email' => $email,
	'comment_content' => $questions
);
if( !$akismet->isCommentSpam() ) {
	$queryParams['comp_id'] = $sf_compID;
}

$queryString = '';
$i = 0;

foreach( $queryParams as $key => $value ) {
	$i++;
	if( $i == 1 ) {
		$pref = '?';
	} else {
		$pref = '&';
	}
	$queryString .= $pref . $key . '=' . $value;
}

if( $akismet->isCommentSpam() ) {
	
	$subject	= 'Possible Spam: DentalMarketing.net ROI Calculator Lead';
	$sms		= 'Possible Spam on ROI Calculator form';
	$to	= array(
		'mark@123postcards.com' => 'Mark Delorey'
	);
	
} else {
	
	$subject = 'DentalMarketing.net ROI Calculator Lead';
	$sms	 = 'DentalMarketing.net ROI Calculator Lead from ' . $name . ' ' . $phone;
	$to = array(
		'aaron@123postcards.com' => 'Aaron Boone'
	);
}

// Create the contact in SF via webservice if not flagged as spam
if( !$akismet->isCommentSpam() ) {
	require_once '../Salesforce/createContact_Postcards.php';
}

// Price per card based on card volume
switch( $cardQty ) {
	case 5000:
		$unitPrice	=	'.399';
		break;
	case 10000:
		$unitPrice	=	'.369';
		break;
	case 15000:
		$unitPrice	=	'.359';
		break;
	case 20000:
		$unitPrice	=	'.349';
		break;
	default: // Above 25000
		$unitPrice	=	'.339';
		break;
}

$campaignCost	=	$cardQty * $unitPrice;

// Forecasted metrics based on input
$newCalls	=	$cardQty * $callRate;
$newPat		=	$newCalls * $convRate;
$newRev		=	$newPat * $patVal;
$foreROI	=	round( $newRev / $campaignCost * 100 );

// Guaranteed results based on 200% ROI guarantee
$reqRev		=	( $guarROI / 100 ) * $campaignCost;
$reqPat		=	ceil( $reqRev / $patVal );
$reqCalls	=	ceil( $reqPat / $callRate ) / 100;

// Construct email message via Swift Mailer

require_once '../Swift_lib/swift_required.php';

// Create the Transport
$transport = Swift_SmtpTransport::newInstance('smtp.webfaction.com', 25)
	->setUsername('p123_forms')
	->setPassword('ZuwE?a+ethE$6d#e')
;

$messagebody	=	"<h1>Contact Request from " . $name . '</h1>';

if( $akismet->isCommentSpam() ) {

	$messagebody .=	 '<h2>The spam filter flagged this message</h2>';
	$messagebody .=	 '<p>If this is not spam, please <a href="' . THEME_URI . '/submit-ham/' . $queryString . '">let us know it was good</a>.</p>';
	$messagebody	.=	'<a href="' . $sfresponse->result[0] . '"><h2>' . $sfmessage . '</h2></a>';

} else {

	$messagebody	.=	'<a href="' . $sfresponse->result[0] . '"><h2>' . $sfmessage . '</h2></a>';

}

$messagebody	.=	"<table style=\"margin-bottom: 25px\">";
$messagebody	.=	"<tr><td>Practice Name:</td><td>" . $practice . "</td></tr>";
$messagebody	.=	"<tr><td>Contact Name:</td><td>" . $name . "</td></tr>";
$messagebody	.=	"<tr><td>Contact Position:</td><td>" . $position . "</td></tr>";
$messagebody	.=	"<tr><td>Phone:</td><td>" . $phone . "</td></tr>";
$messagebody	.=	"<tr><td>Email Address:</td><td>" . $email . "</td></tr>";
$messagebody	.=	"</table>";

$messagebody	.=	"<p>Comments Questions:<br />" . $questions . "</p>";

$messagebody	.=	"<h2>Campaign Request Information</h2>";

$messagebody	.=	"<table style=\"margin-bottom: 25px\">";
$messagebody	.=	"<tr><td>Campaign Cost:</td><td>" . asMoney( $campaignCost ) . "</td></tr>";
$messagebody	.=	"<tr><td>Card Quantity:</td><td>" . $cardQty . "</td></tr>";
$messagebody	.=	"<tr><td>Patient Value:</td><td>" . $patVal . "</td></tr>";
$messagebody	.=	"<tr><td>Call Rate:</td><td>" . $callRate . "</td></tr>";
$messagebody	.=	"<tr><td>Conversion Rate:</td><td>" . $convRate . "</td></tr><tr></tr>";
$messagebody	.=	"</table>";

$messagebody	.=	"<table style=\"margin-bottom: 25px\">";
$messagebody	.=	"<tr><td>Forecasted ROI:</td><td>" . $foreROI . "%</td></tr>";
$messagebody	.=	"<tr><td>Forecasted Calls:</td><td>" . $newCalls . "</td></tr>";
$messagebody	.=	"<tr><td>Forecasted Patients:</td><td>" . $newPat . "</td></tr>";
$messagebody	.=	"<tr><td>Forecasted Revenue:</td><td>" . asMoney( $newRev ) . "</td></tr><tr></tr>";
$messagebody	.=	"</table>";

$messagebody	.=	"<table style=\"margin-bottom: 25px\">";
$messagebody	.=	"<tr><td>Guaranteed ROI:</td><td>" . $guarROI . "%</td></tr>";
$messagebody	.=	"<tr><td>Guaranteed Calls:</td><td>" . $reqCalls . "</td></tr>";
$messagebody	.=	"<tr><td>Guaranteed Patients:</td><td>" . $reqPat . "</td></tr>";
$messagebody	.=	"<tr><td>Guaranteed Revenue:</td><td>" . asMoney( $reqRev ) . "</td></tr>";
$messagebody	.=	"</table>";

$messagebody	.=	'<p>Spam message? <a href="' . THEME_URI . '/submit-spam/' . $queryString . '">Submit it here</a></p>';

$messagebody	.=	"<p>This message was sent on " . date( 'r') . " via the ROI Calculator on DentalMarketing.net.</p>";

// Create the Mailer using your created Transport
$mailer = Swift_Mailer::newInstance($transport);

// Create a message
$message = Swift_Message::newInstance($subject)
  ->setFrom(array($email => $name))
  ->setTo($to)
  ->setBody($messagebody, 'text/html')
  ;

if( !$akismet->isCommentSpam() ) {
	$message->setCc( array( 'mark@123postcards.com' => 'Mark Delorey' ) );
}

// Send the message
$result = $mailer->send($message);

// Construct Twilio SMS

// Send a text message to all of the admins
require_once( '../Twilio_lib/Twilio.php' );

// Set our AccountSid and AuthToken from twilio.com/user/account
$AccountSid = 'ACd3622997cc01433f912cc71753549a4b';
$AuthToken = 'de0eefe0e642eb0c328596cf31634794';

// Instantiate a new Twilio Rest Client
$client = new Services_Twilio($AccountSid, $AuthToken);

// Your Twilio Number or Outgoing Caller ID
$from = '8019215865';

// make an associative array of server admins. Feel free to change/add your 
// own phone number and name here.
$people = array(
    'Mark' => '9252860014',
    'Aaron' => '8019536771'
);

// Iterate over all admins in the $people array. $to is the phone number, 
// $name is the user's name
foreach ( $people as $name => $to ) {
    // Send a new outgoing SMS 
    $body = $sms;
    $client->account->sms_messages->create($from, $to, $body);
}

$message = 'Thanks for using our ROI Calculator! We\'ll contact you shortly to help you get started.';

$output	= array(
	'message' => $message
);
$output = json_encode($output);

header('HTTP/1.1 200 OK');
echo $output;

?>