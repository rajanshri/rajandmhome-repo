<?php

// Kill form if bot fills fields
$area_length = strlen($_REQUEST['phoneArea']);
$pref_length = strlen($_REQUEST['phonePrefix']);
$exch_length = strlen($_REQUEST['phoneExchange']);
$honeypot	 = $_REQUEST['sticky'];

if(
	$area_length != 3 ||
	$_REQUEST['phoneArea'] == 'XXX' ||
	$pref_length != 3 ||
	$_REQUEST['phonePrefix'] == 'XXX' || 
	$exch_length != 4 ||
	$_REQUEST['phoneExchange'] == 'XXXX' ||
	$honeypot	== 'on'
) {
	die();
}

// Kill form if accessed directly
$refering	= parse_url( $_SERVER['HTTP_REFERER'] );
if( $refering['host'] != $_SERVER['HTTP_HOST'] ) {
	die();
}

require_once('../../../../../wp-blog-header.php');

$message = "Your request has been submitted successfully! We will contact you shortly.";

// Akismet spam filtering
require_once('../Akismet/Akismet.class.php');

$akismet = new Akismet( get_site_url(), 'beb589950138' );
$akismet->setCommentAuthor($name);
$akismet->setCommentAuthorEmail($email);
$akismet->setCommentContent($questions);
$akismet->setPermalink($_SERVER['HTTP_REFERER']);

$practice	=	stripslashes( $_REQUEST['practice'] );
$firstName	=	stripslashes( $_REQUEST['firstName'] );
$lastName	=	stripslashes( $_REQUEST['lastName'] );
$name		=	$firstName . ' ' . $lastName;
$phone		=	'(' . $_REQUEST['phoneArea'].') '.$_REQUEST['phonePrefix'].'-'.$_REQUEST['phoneExchange'];
$email		=	$_REQUEST['emailAddress'];
$questions	=	stripslashes( $_REQUEST['comments'] );
$spam		=	false;
if( $akismet->isCommentSpam() ) {
	$spam	=	true;
}

$queryParams = array(
	'k'	=>	'sWiaphiuwr0ecia0la8OubRLaspiawIexLaSplESpiust5EvOaSpiesIETrlumIa',
	'a'	=>	'44',
	'source'	=>	'Website - Consult Request',
	'email'	=>	$email,
	'phone'	=>	$phone,
	'fname'	=>	$firstName,
	'lname'	=>	$lastName,
	'pname'	=>	$practice,
	'questions'	=>	$questions,
	'street'	=>	'',
	'street2'	=>	'',
	'suite'	=>	'',
	'city'	=>	'',
	'state'	=>	'',
	'zip'	=>	'',
	'spam'	=>	$spam
);

$url	=	'https://sfrestapi.dentalmarketing.net/?' . http_build_query($queryParams);

$curl	=	curl_init($url);
 
$response	=	curl_exec($curl);

curl_close($curl);

$output	= array(
	'message' => $message
);
$output = json_encode($output);

header('HTTP/1.1 200 OK');
echo $output;

?>