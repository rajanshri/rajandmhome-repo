<?php

require_once('wp-blog-header.php');
 
// Create the contact in SF via webservice
require_once 'wp-content/themes/invisible-hand/inc/Salesforce/createContact_Postcards.php';

// Construct email message via Swift Mailer
require_once 'wp-content/themes/invisible-hand/inc/Swift_lib/swift_required.php';

// Create the Transport
$transport = Swift_SmtpTransport::newInstance('smtp.webfaction.com', 25)
	->setUsername('p123_forms')
	->setPassword('ZuwE?a+ethE$6d#e')
;

// Create the Mailer using your created Transport
$mailer = Swift_Mailer::newInstance($transport);

// Internal lead email

$in_subject = $event['short_name'] . ' Event Offer SMS Lead';

$messagebody	=	'<h1>SMS Lead: ' . $fullName . '</h1>';

$messagebody	.=	'<a href="' . $sfresponse->result[0] . '"><h2>' . $sfmessage . '</h2></a>';

if( $event['message'] != '' ) {
	$messagebody .= $event['message'];
}

$messagebody	.=	'<table style="margin-bottom: 25px">';
$messagebody	.=	'<tr><td>Contact Name:</td><td>' . $fullName . '</td></tr>';
$messagebody	.=	'<tr><td>Phone:</td><td>' . $phone . '</td></tr>';
$messagebody	.=	'<tr><td>Email Address:</td><td>' . $email . '</td></tr>';
$messagebody	.=	'</table>';

$messagebody	.=	'<p>This message was sent on ' . date( 'r') . ' via the SMS lead from the ' . $event['name'] . ' event.</p>';

$to = array(
	'aaron@123postcards.com' => 'Aaron Boone'
);

// Create a message
$internal = Swift_Message::newInstance($in_subject)
	->setFrom(array($email => $fullName))
	->setTo($to)
	->setBcc( array( 'mark@123postcards.com' => 'Mark Delorey' ) )
	->setBody($messagebody, 'text/html')
	;
	
// Send the message
$in_result = $mailer->send($internal);

// External email confirmation

$ex_subject		=	'123 Postcards Request Confirmation';

$messagebody	=	'<h1>Request Confirmation for ' . $fullName . '</h1>';

$messagebody	.=	'<p>Thank you for contacting 123 Postcards!</p>';

if( $event['message'] != '' ) {
	$messagebody .= $event['message'];
}

if( $event['type'] == 'call-tracking-only' ) {
	$messagebody	.=	'<p>We look forward to helping you get the most out of your marketing dollars. We will be contacting you soon to set up your FREE call tracking.</p>';
	$messagebody	.=	'<p>You may also be interested in watching our <a href="http://tinyurl.com/123postcards">4-min introduction video</a> to see why practices across the country love our marketing programs.</p>';
} else {
	$messagebody	.=	'<p>We look forward to driving new patients to your practice with the best dental direct mail program on the planet!</p>';	$messagebody	.=	'<p>Please take a few minutes to watch our <a href="http://tinyurl.com/123postcards">introduction video</a> and see why practices across the country love our program and consistently make 2x to 5x their money.</p>';
}

if( $event['type'] != 'call-tracking-only' ) {
	$messagebody	.=	'<p><strong>*AREA EXCLUSIVITY</strong>: Please respond to this email with your practice name and address to verify availability and reserve exclusivity.</p>';
}

$messagebody	.=	'<p style="font-size: 11px; color: #666;">This message was sent on ' . date( 'r') . ' via SMS submission from the ' . $event['long_name'] . ' event. You are receiving this email because you texted your name and email to claim the event offer.</p>';

$to = array(
	$email => $fullName
);

// Create a message
$external = Swift_Message::newInstance($ex_subject)
	->setFrom( array( 'aaron@123postcards.com' => '123 Postcards' ) )
	->setTo($to)
	->setBody($messagebody, 'text/html')
	;
	
// Send the message
$ex_result = $mailer->send($external);

?>