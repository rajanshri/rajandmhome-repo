<?php

$cardQty	=	intval( $_REQUEST['cardQty'] );
$patVal		=	intval( $_REQUEST['patVal'] );
$callRate	=	floatval( $_REQUEST['callRate'] );
$convRate	=	floatval( $_REQUEST['convRate'] );
$guarROI	=	200;

// Price per card based on card volume
switch( $cardQty ) {
	case 5000:
		$unitPrice	=	'.399';
		break;
	case 10000:
		$unitPrice	=	'.369';
		break;
	case 15000:
		$unitPrice	=	'.359';
		break;
	case 20000:
		$unitPrice	=	'.349';
		break;
	default: // Above 25000
		$unitPrice	=	'.339';
		break;
}

$campaignCost	=	$cardQty * $unitPrice;

// Forecasted metrics based on input
$newCalls	=	$cardQty * $callRate;
$newPat		=	$newCalls * $convRate;
$newRev		=	$newPat * $patVal;
$foreROI	=	round( $newRev / $campaignCost * 100 );

// Guaranteed results based on 200% ROI guarantee
$reqRev		=	( $guarROI / 100 ) * $campaignCost;
$reqPat		=	ceil( $reqRev / $patVal );
$reqCalls	=	ceil( $reqPat / $callRate ) / 100;

$output	= '{';
$output	.=	'"campaignCost":"' . $campaignCost . '",';
$output	.=	'"forecasted":';
	$output	.=	'{';
		$output	.=	'"calls": "' . $newCalls . '",';
		$output	.=	'"patients": "' . $newPat . '",';
		$output	.=	'"rev": "' . $newRev . '",';
		$output	.=	'"roi": "' . $foreROI . '%"';
	$output	.=	'},';
$output	.=	'"guaranteed":';
	$output	.=	'{';
		$output	.=	'"calls": "' . $reqCalls . '",';
		$output	.=	'"patients": "' . $reqPat . '",';
		$output	.=	'"rev": "' . $reqRev . '",';
		$output	.=	'"roi": "' . $guarROI . '%"';
	$output	.=	'}';
$output .= '}';

echo $output;

?>