<?php

// Register the featured link
add_action( 'init', 'flink' );

function flink(){
	
	$labels = array(
		'name' => _x('Featured Links', 'post type general name'),
		'singular_name' => _x('Featured Link', 'post type singular name'),
		'add_new' => _x('Add New', 'featured'),
		'add_new_item' => __('Add New Featured Link'),
		'edit_item' => __('Edit Featured Link'),
		'new_item' => __('New Featured Link'),
		'view_item' => __('View Featured Link'),
		'search_items' => __('Search Featured Links'),
		'not_found' => __('Nothing found'),
		'not_found_in_trash' => __('Nothing found in Trash'),
		'parent_item_colon' => ''
	);
	
	$args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'exclude_from_search' => true,
		'show_ui' => true,
		'query_var' => true,
		'rewrite' => 'true',
		'capability_type' => 'post',
		'hierarchical' => false,
		'menu_position' => null,
		'supports' => array( 'title', 'editor' ),
		'show_in_nav_menus' => false
	);
	
	register_post_type( 'flink', $args );
	
	register_taxonomy('Placement', array('flink'), array('hierarchical' => false, 'label' => 'Placements', 'singular_label' => 'Placement', 'rewrite' => true));

}

// Add the meta boxes for the featured link
add_action('admin_init', 'flink_init');
 
function flink_init(){
  add_meta_box('credits_meta', 'Featured Link Info', 'flink_meta', 'flink', 'normal', 'high');
}
 
function flink_meta() {
  global $post;
  $custom = get_post_custom($post->ID);
  $anchor_text = $custom['fl_anchor_text'][0];
  $url = $custom['fl_url'][0];
  ?>
  <p><label>Anchor text</label><br />
  <input type="text"  name="fl_anchor_text" value="<?php echo $anchor_text; ?>" /></p>
  <p><label>Destination URL</label><br />
  <input type="text"  name="fl_url" value="<?php echo $url; ?>" /></p>
  <?php
}

// Save the meta information for the featured link
add_action('save_post', 'save_flink_details');

function save_flink_details(){
  global $post;
 
  update_post_meta($post->ID, 'fl_anchor_text', $_POST['fl_anchor_text']);
  update_post_meta($post->ID, 'fl_url', $_POST['fl_url']);
  update_post_meta($post->ID, 'fl_desc', $_POST['fl_desc']);
}

// Show meta information on featured link list
add_action('manage_posts_custom_column',  'flink_custom_columns');
add_filter('manage_edit-flink_columns', 'flink_edit_columns');
 
function flink_edit_columns($columns){
  $columns = array(
    'cb' => '<input type="checkbox" />',
    'title' => 'Link Title',
    'anchor_text' => 'Anchor Text',
    'url' => 'Dest. URL',
    'description' => 'Description'
  );
 
  return $columns;
}
function flink_custom_columns($column){
  global $post;
 
  switch ($column) {
    case 'description':
      $custom = get_post_custom();
      echo $custom['fl_desc'][0];
      break;
    case 'url':
      $custom = get_post_custom();
      echo $custom['fl_url'][0];
      break;
    case 'anchor_text':
      $custom = get_post_custom();
      echo $custom['fl_anchor_text'][0];
      break;
  }
}


?>