<?php
/*

//[cta] shortcode
function ihm_cta( $atts, $content = null ) {
	global $ihmSettings;
	global $my_theme;
	global $theme_meta;
	
	//Define shortcode options/attributes
	
	if ($theme_meta == 'DentalMarketing') :
		extract( shortcode_atts( array(
			'size'	=> 'large',
			'page1'	=> 'postcards',
			'text1'	=> 'Postcards: Guaranteed ROI',
			'page2'	=> 'videos',
			'text2'	=> 'Videos: Showcase Your Practice'
		), $atts) );
		//HTML output template
		return '<div class="outer-center dmcta"><div class="inner-center"><a href="'.$page1.'" class="button large orange '.$size.'" title="'.$text1.'">'.$text1.'</a><a href="'.$page2.'" class="button large green '.$size.'" title="'.$text2.'">'.$text2.'</a></div></div>';
	else :
		extract( shortcode_atts( array(
			'size'	=> 'large',
			'page'	=> $my_theme['cta_page'],
			'text'	=> $my_theme['cta_text']
		), $atts) );
		//HTML output template
		return '<div class="outer-center"><div class="inner-center"><a href="' . HOME_URI . $page.'" class="button large '.$size.'" title="'.$text.'">'.$text.'</a></div></div>';
	endif;
		
}//End function ihm_cta()
*/



//[cta] shortcode

function ihm_cta( $atts, $content = null ) { 
	global $ihmSettings;

	$ctaContent = "" . 
/*
	"<div class='section blueBG ctaSection'>" . 
	"<div class='center'>" . 
		"<div class='sectionWrapper'>" . 
		"<a href='/postcards' class='first'>" . 
			"<span class='icon icon-coin'></span>" . 
			"<span class='btnText'>Get Pricing Info</span>" . 
		"</a>" . 
		"<a href='/videos' class='second'>" . 
			"<span class='icon icon-gallery'></span>" . 
			"<span class='btnText'>View Postcard Gallery</span>" . 
		"</a>" . 
		"</div><!-- /.sectionwrapper-->" . 
	"</div><!-- /.center -->" . 
	"</div><!-- /.section -->" . 
*/
	"";

	return $ctaContent;
} //End function ihm_cta()

//Shortcodes. they make things SOOO much easier!
add_shortcode( 'cta', 'ihm_cta' );

?>
