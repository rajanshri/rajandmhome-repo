<?php 

$themename	=	'Invisible Hand';

$shortname	=	'ihm';

$categories = get_categories('hide_empty=0&orderby=name');
$wp_cats = array();
foreach ($categories as $category_list ) {
       $wp_cats[$category_list->cat_ID] = $category_list->cat_name;
}
array_unshift($wp_cats, 'Choose a category');

$options = array (

	// Options title
	array( 'name' => $themename.' Options',
		'type' => 'title'),
	
	// General section
	array( 'name' => 'General',
		'type' => 'section'),
	array( 'type' => 'open'),
	
	array( 'name' => 'Colour Scheme',
		'desc' => 'Select the colour scheme for the theme',
		'id' => $shortname.'_color_scheme',
		'type' => 'select',
		'options' => array('blue', 'red', 'green'),
		'std' => 'blue'),
	
	array( 'name' => 'Logo Image',
		'desc' => 'Upload a photo for your logo',
		'id' => $shortname.'_logo',
		'type' => 'upload',
		'std' => ''),
		
	array( 'name' => 'Logo Alt Text',
		'desc' => 'Displayed in place of the logo if not available',
		'id' => $shortname.'_logo_alt',
		'type' => 'text',
		'std' => 'Invisible Hand'),
	
	array( 'name' => 'Custom CSS',
		'desc' => 'Want to add any custom CSS code? Put in here, and the rest is taken care of. This overrides any other stylesheets. eg: a.button{color:green}',
		'id' => $shortname.'_custom_css',
		'type' => 'textarea',
		'std' => ''),
	
	// Footer section
	array( 'type' => 'close'),
	array( 'name' => 'Footer',
		'type' => 'section'),
	array( 'type' => 'open'),
	
	array( 'name' => 'Footer copyright text',
		'desc' => 'Enter text used in the right side of the footer. It can be HTML',
		'id' => $shortname.'_footer_text',
		'type' => 'text',
		'std' => ''),
		
	// Do not remove
	array( 'type' => 'close')

); // end $options

// Display theme option admin panel

function ihm_admin() {

	global $themename, $shortname, $options;
	$i=0;
	
	if ( $_REQUEST['saved'] ) echo '<div id="message" class="updated fade"><p><strong>'.$themename.' settings saved.</strong></p></div>';
	if ( $_REQUEST['reset'] ) echo '<div id="message" class="updated fade"><p><strong>'.$themename.' settings reset.</strong></p></div>';
	
	?>
	<div class="wrap ihm_wrap">
	
	<script type="text/javascript">
	
		jQuery(function($){
		
			// upload / insert image function
			var txtBox_id = '';
			$('.ihm_upload_image_button').click(function() {
				txtBox_id = '#'+$(this).prop('id').replace('_button', '');
				image_id = '#image_'+$(this).prop('id').replace('upload_image_button_', '');
				formfield = $(txtBox_id);
				tb_show('', 'media-upload.php?type=image&amp;TB_iframe=true');
				return false;
			});
			// send the selected image to the image url field
			window.send_to_editor = function(html) {
				imgurl = $('img',html).prop('src');
				$(txtBox_id).val(imgurl);
				$(image_id).prop('src', imgurl)
				tb_remove();
			}
			
		});
		
		jQuery(document).ready(function($){
			$('.fade').delay('4000').animate({ 'height': '0', 'opacity': '0' });
		});
		
	</script>
	
	
	<h2><?php echo $themename; ?> Settings</h2>
	
	<div class="ihm_opts">
	<form method="post" enctype="multipart/form-data">
	
	<?php
	
	foreach ($options as $value) {
		switch ( $value['type'] ) {
			
			// Open section
			case "open":
				break;
			
			// Close section
			case "close": ?>
				</div>
				</div>
				<br />
				<?php 
				break;
			
			// Title option
			case "title": ?>
				<p>To easily use the <?php echo $themename;?> theme, you can use the menu below.</p>
				
				<?php 
				break;
			
			// Text option
			case 'text': ?>
				<div class="ihm_input ihm_text">
					<label for="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></label>
				 	<input name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>" type="<?php echo $value['type']; ?>" value="<?php if ( get_settings( $value['id'] ) != "") { echo stripslashes(get_settings( $value['id'])  ); } else { echo $value['std']; } ?>" />
				 	<small><?php echo $value['desc']; ?></small><div class="clearfix"></div>
				 </div>
				<?php
				break;
			
			// Textarea option
			case 'textarea': ?>
				<div class="ihm_input ihm_textarea">
					<label for="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></label>
				 	<textarea name="<?php echo $value['id']; ?>" type="<?php echo $value['type']; ?>" cols="" rows=""><?php if ( get_settings( $value['id'] ) != "") { echo stripslashes(get_settings( $value['id']) ); } else { echo $value['std']; } ?></textarea>
				 	<small><?php echo $value['desc']; ?></small><div class="clearfix"></div>
				 </div>
				<?php
				break;
			
			// Select option
			case 'select': ?>
				<div class="ihm_input ihm_select">
					<label for="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></label>
					<select name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>">
					<?php foreach ($value['options'] as $option) { ?>
						<option <?php if (get_settings( $value['id'] ) == $option) { echo 'selected="selected"'; } ?>><?php echo $option; ?></option><?php } ?>
					</select>
					<small><?php echo $value['desc']; ?></small><div class="clearfix"></div>
				</div>
				<?php
				break;
			
			// Checkbox option
			case "checkbox": ?>
				<div class="ihm_input ihm_checkbox">
					<label for="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></label>
					<?php if(get_option($value['id'])){ $checked = "checked=\"checked\""; }else{ $checked = "";} ?>
					<input type="checkbox" name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>" value="true" <?php echo $checked; ?> />
					<small><?php echo $value['desc']; ?></small><div class="clearfix"></div>
				 </div>
				<?php break;
			
			// Upload option
			case "upload": ?>
				 <div class="ihm_input ihm_text"> 
					<label for="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></label>
					<input type="text" name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>" class="ihm_text" />
					<input id="<?php echo $value['id']; ?>_button" class="ihm_upload_image_button button-primary" value="Choose Image" type="button" />
					<small><?php echo $value['desc']; ?></small><div class="clearfix"></div>
				</div>
				 <?php
				 break;
			
			// Section
			case "section": ?>
				<div class="ihm_section">
					<div class="ihm_title"><h3><?php echo $value['name']; ?></h3><span class="submit"><input name="save<?php echo $i; ?>" type="submit" value="Save changes" />
					</span><div class="clearfix"></div></div>
				<div class="ihm_options">
				<?php 
				$i++;
				break;
		
		}
	}
	?>
	
	<input type="hidden" name="action" value="save" />
	</form>
	<form method="post">
	<p class="submit">
	<input name="reset" type="submit" value="Reset" />
	<input type="hidden" name="action" value="reset" />
	</p>
	</form>
	 </div> 
	
	<?php
} //end function mytheme_admin()

//Display theme option admin panel

function ihm_add_admin() {

	global $themename, $shortname, $options;

	if ( $_GET['page'] == basename(__FILE__) ) {
	
		if ( 'save' == $_REQUEST['action'] ) {
		
			foreach ($options as $value) {
				update_option( $value['id'], $_REQUEST[ $value['id'] ] ); 
			}

			
			foreach ($options as $value) {  
			
				if( isset( $_REQUEST[ $value['id'] ] ) ) {
					update_option( $value['id'], $_REQUEST[ $value['id'] ]  );
				}
			}			
			header("Location: admin.php?page=functions-options.php&saved=true");
			die;
		}
		else if( 'reset' == $_REQUEST['action'] ) {
			foreach ($options as $value) {
				delete_option( $value['id'] ); 
			}
			header("Location: admin.php?page=functions-options.php&reset=true");
			die;
		}
	}

	add_menu_page($themename, $themename, 'administrator', basename(__FILE__), 'ihm_admin', THEME_IMAGES . '/ihm_icon.png');

}

function ihm_add_init() {
	wp_enqueue_style(
		'functions', 
		THEME_CSS . '/functions.css', 
		false, 
		'1.0', 
		'all'
	);
}

function ihm_add_styles() {
	wp_enqueue_style('thickbox');
}

function ihm_add_scripts() {
	wp_enqueue_script('media-upload');
	wp_enqueue_script('thickbox');
	wp_register_script(
		'ihm_adminJS',
		THEME_JS . '/admin.js',
		array('jquery'),
		1.0
	);
}

add_action('admin_init', 'ihm_add_init');
add_action('admin_menu', 'ihm_add_admin');
add_action('admin_print_styles', 'ihm_add_styles');
add_action('admin_print_scripts', 'ihm_add_scripts');

?>