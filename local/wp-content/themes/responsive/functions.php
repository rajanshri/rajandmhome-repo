<?php

/***************
 Security Measures 
 ***************/

	//Remove WP login errors
	function wrong_login() {
	  return 'Wrong username or password.';
	}
	add_filter('login_errors', 'wrong_login');
	
	// Remove WP version meta
	function remove_version() {
	  return '';
	}
	add_filter('the_generator', 'remove_version');
	
	// Remove WLW and RSD
	add_action('init', 'remheadlink');
	function remheadlink() {
		remove_action('wp_head', 'rsd_link');
		remove_action('wp_head', 'wlwmanifest_link');
	}
	
	// Remove relational links except on blog
	add_action('wp', 'remRelational');
	function remRelational(){
		if( is_page() || is_front_page() ) {
			remove_action('wp_head', 'parent_post_rel_link', 10, 0);
			remove_action('wp_head', 'start_post_rel_link', 10, 0 );
			remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
		}
	}
	
/***************
 End Security Measures 
 ***************/

date_default_timezone_set('America/Denver');

// Define constants
define( 'HOME_URI', get_bloginfo('url') );
define( 'THEME_URI', get_stylesheet_directory_uri() );
define( 'THEME_IMAGES', THEME_URI . '/images' );
define( 'THEME_CSS', THEME_URI . '/css' );
define( 'THEME_JS', THEME_URI . '/js' );
define(	'THEME_DIR', get_stylesheet_directory() );

// Development

// Use to detect development environment particularly with lead notifications
function is_production() {

	$return	=	false;
	if( HOME_URI == 'http://www.dentalmarketing.net' ) {
		$return	=	true;
	}
	return $return;
}

// Config File
require('inc/config.php');

// Shortcodes
require('inc/shortcodes.php');

// Utilities
require('inc/utilities.php');

// AJAX
require('inc/ajax/design-gallery.php');
//require('inc/ajax/load-video.php');
//require('inc/ajax/load-contact-us.php');
require('inc/ajax/contact-form.php');
require('inc/ajax/roi-calculator.php');
//require('inc/ajax/videos-lead-form.php');
require('inc/ajax/video-gallery.php');

// Enable thumbnails
add_theme_support( 'post-thumbnails' );

add_image_size( 'home-testimonial', 282, 159 );
add_image_size( 'testimonial-list', 213, 120 );
add_image_size( 'jobs-list', 100, 120);

// Register menus for main nav, top nav, sidebar nav, and footer nav
add_action( 'init', 'register_my_menu' );

function register_my_menu() {
	register_nav_menu( 'postcards-menu', __( 'Postcards Menu' ) );
	register_nav_menu( 'videos-menu', __( 'Videos Menu' ) );
	register_nav_menu( 'dm-menu', __( 'DentalMarketing Menu' ) );
	register_nav_menu( 'top-menu', __( 'Top Menu' ) );
	register_nav_menu( 'top-tabs', __( 'Top Tabs' ) );
	register_nav_menu( 'footer-menu', __( 'Footer Menu' ) );
	register_nav_menu( 'sidebar-menu', __( 'Sidebar Menu' ) );
}

// Register "Above Form" sidebar
register_sidebar(array(
	'name' => 'Above Form',
	'before_widget' => '<div id="%1$s" class="widget %2$s">',
	'after_widget' => '</div>',
	'before_title' => '<h2>',
	'after_title' => '</h2>',
));

// Register "Below Form" sidebar
register_sidebar(array(
	'name' => 'Below Form',
	'before_widget' => '<div id="%1$s" class="%2$s">',
	'after_widget' => '</div>',
	'before_title' => '<h2>',
	'after_title' => '</h2>',
));

// Register "Postcards Home" sidebar
register_sidebar(array(
	'name' => 'Postcards Home',
	'before_widget' => '<div class="subHomeBlock">',
	'after_widget' => '</div>',
	'before_title' => '<h2>',
	'after_title' => '</h2>',
));

// Register "Videos Home" sidebar
register_sidebar(array(
	'name' => 'Videos Home',
	'before_widget' => '<div class="subHomeBlock">',
	'after_widget' => '</div>',
	'before_title' => '<h2>',
	'after_title' => '</h2>',
));

// Register "Videos Home" sidebar
register_sidebar(array(
	'name' => 'Blog',
	'before_widget' => '<div class="blogSidebar">',
	'after_widget' => '</div>',
	'before_title' => '<h2>',
	'after_title' => '</h2>',
));

// Change the URL to the ajax-loader image from CF7
add_filter('wpcf7_ajax_loader', 'my_wpcf7_ajax_loader');
function my_wpcf7_ajax_loader () {
	return  THEME_IMAGES . '/ajax-loader.gif';
}

// Remove CSS for CF7
add_action( 'wp_print_styles', 'deregister_cf7_styles', 100 );
function deregister_cf7_styles() {
	wp_deregister_style( 'contact-form-7' );
}

// Replace "read more" on excerpts

function excerpt_read_more_link($output) {
 global $post;
 return str_replace('[&hellip;]', '<a class="toggleFull">[&hellip;]</a>', $output);
}
add_filter('the_excerpt', 'excerpt_read_more_link');


/*
function excerpt_read_more_link($output) {
 global $post;
 return $output . '<div class="showMore">Show More...</div>';
}
add_filter('the_excerpt', 'excerpt_read_more_link');
*/

global $my_theme;

add_action('wp','get_my_theme');

function get_my_theme() {
	
	global $my_theme;
	global $post;
	global $theme_meta;
	global $buttons;
	
	$theme_meta	=	get_post_meta($post->ID, 'ecpt_theme', true);
	
	switch($theme_meta) {
		case 'Videos':
			$my_theme	=	array(
				'color'	=>	'green',
				'logo'	=>	'logo-vd.png',
				'logo_path'	=>	'/videos/',
				'menu'	=>	'videos-menu',
				'testimonials' => '/video-testimonials',
				'id' => 1605,
				'cta_page'	=> '/dentist-marketing-videos',
				'cta_text'	=> 'Why do you need videos?',
				'interest'	=>	'videos'
			);
			break;
		case 'Postcards';
			$my_theme	=	array(
				'color'	=>	'blue',
				//'logo'	=>	'logo-pc.png',
				'logo'	=>	'logo-dm.png',
				//'logo_path'	=>	'/postcards/',
				'logo_path' => '/',
				//'menu'	=>	'postcards-menu',
				'menu'	=>	'dm-menu',
				'testimonials' => '/postcard-testimonials',
				'id' => 23,
				'cta_page'	=> '/best-dental-marketing-roi-calculator',
				'cta_text'	=> 'Try Our Marketing ROI Calculator',
				'interest'	=>	'postcards'
			);
			break;
		default:
			$my_theme	=	array(
				'color'	=>	'blue',
				'logo'	=>	'logo-dm.png',
				'logo_path'	=>	'/',
				'menu'	=>	'dm-menu',
				'testimonials' => '/dental-marketing-testimonials',
				'id' => '',
				'interest'	=>	'general'
			);
			break;
	}
	
	$postID = $my_theme['id'];
	
	$buttons	=	array();
	
	if($postID != '') :
	  for( $i = 1; $i < 4; $i++ ) :
	    $buttons[$i]	=	array(
				"image" => get_post_meta($postID, 'ecpt_button'. $i .'image', true),
				"alt" => get_post_meta($postID, 'ecpt_button'. $i .'alt', true),
				"title" => strtoupper(get_post_meta($postID, 'ecpt_button'. $i .'title', true)),
				"content" => get_post_meta($postID, 'ecpt_button'. $i .'content', true),
				"link" => get_post_meta($postID, 'ecpt_button'. $i .'link', true)
			);
	  endfor;
	endif;
	
};

// Enqueue template css
add_action('wp_print_styles', 'enqueueMyCSS');

function enqueueMyCSS(){
	global $post;
	global $my_theme;
	if ( !is_admin() && !is_login_page() ) {
		wp_enqueue_style('layout', THEME_CSS . '/layout.css');
		wp_enqueue_style('openSans', 'http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,700italic,400,300,700');
		wp_enqueue_style('architectsDaughter', 'http://fonts.googleapis.com/css?family=Architects+Daughter');
		if( is_front_page() ){ 
			// include nivo styles and themes
			wp_enqueue_style('nivoStyle', THEME_URI . '/nivo/nivo-slider.css');
			wp_enqueue_style('nivoDefault', THEME_URI . '/nivo/nivo-themes/default_new/default.css');
		}
		if( is_page_template('frontpage_funnel.php') ) {
			wp_enqueue_style('styleHomeFunnel',	THEME_CSS .'/style-home-funnel.css');
		}	
		if( is_page_template('frontpage_funnel_b.php') ) {
			wp_enqueue_style('styleHomeFunnel',	THEME_CSS .'/style-home-funnel_b.css');
		}	
		wp_enqueue_style('styleColors',	THEME_CSS . '/' . $my_theme['color'] . '.css');
		if( is_page_template('dashboard-intro.php') ) {
			wp_enqueue_style('styleHomeFunnel',	THEME_CSS .'/dashboard-intro.css');
		}	
	}
}

function is_login_page() {
    return in_array($GLOBALS['pagenow'], array('wp-login.php', 'wp-register.php'));
}

// Enqueue template js
add_action('wp_print_scripts', 'enqueueMyJS');

function enqueueMyJS(){
	global $theme_meta;

	if ( !is_admin() ) {
		wp_deregister_script('jquery');
		
		// Register and enqueue main js
		wp_register_script('jquery',
			//THEME_JS . '/jquery.js',
			'http://ajax.googleapis.com/ajax/libs/jquery/1.8/jquery.min.js',
			array()
		);
		wp_enqueue_script('jquery');
		
		// Register and enqueue main js
		wp_register_script('jqplugins',
			THEME_JS . '/jquery.plugins.js',
			array('jquery'),
			1.2
		);
		wp_enqueue_script('jqplugins');
		
		if ( is_front_page() ) { 
			// Register Nivo Slider js
			wp_register_script('jqnivo',
				THEME_URI . '/nivo/jquery.nivo.slider.js',
				array('jquery')
			);
			wp_enqueue_script('jqnivo');
		}
		
		// Register and enqueue main js
		wp_register_script('base',
			THEME_JS . '/base.js',
			array('jquery', 'jqplugins'),
			1.2
		);
		wp_enqueue_script('base');
		
		if( is_page_template('page-roi-calculator.php') ) {
			wp_enqueue_script('jquery-ui-core');
			wp_enqueue_script('jquery-ui-widget');
			wp_register_script('raphael',
				THEME_JS . '/raphael.min.js',
				array('jquery-ui-core','jquery-ui-widget'),
				1.0
			);
			wp_enqueue_script('raphael');
			wp_register_script('raphael-wijmo',
				THEME_JS . '/jquery.wijmo.raphael.min.js',
				array('raphael','jquery-ui-core','jquery-ui-widget'),
				1.0
			);
			wp_enqueue_script('raphael-wijmo');
			wp_register_script('wijgauge',
				THEME_JS . '/jquery.wijmo.wijgauge.min.js',
				array('raphael-wijmo','raphael','jquery-ui-core','jquery-ui-widget'),
				1.0
			);
			wp_enqueue_script('wijgauge');
			wp_register_script('wijradialgauge',
				THEME_JS . '/jquery.wijmo.wijradialgauge.min.js',
				array('wijgauge','raphael-wijmo','raphael','jquery-ui-core','jquery-ui-widget'),
				1.0
			);
			wp_enqueue_script('wijradialgauge');
		}
	}
}

// Any wp_head scripts
add_action('wp_head', 'headerScripts');

function headerScripts() {
	global $my_theme;
	if ( !is_admin() ) {
		$scripts	=	"<script type=\"text/javascript\">\n\n"
	
					.	"// Google Analytics\n"
					.	"var _gaq = _gaq || [];"
					.	"_gaq.push(['_setAccount', 'UA-7412644-2']);"
					.	"_gaq.push(['_trackPageview']);"
					.	"(function() {"
					.	"var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;"
					.	"ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';"
					.	"var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);"
					.	"})();\n"
					.	"// Section\n"
					.	"var section = '". $my_theme['interest'] ."';\n\n"
					
					.	"</script>";
					
		echo $scripts;
	}
}



// COOKIES //

global $timeout_days;
$timeout_days	=	array(
	'45'	=>	time()+60*60*24*45
);


add_action('wp', 'setMiscCookies');

function setMiscCookies() {
	global $timeout_days;
	$new_sources	=	array();
	
	// aff cookie
	if( isset($_GET['aff']) ) {
		setcookie('aff', $_GET['aff'], $timeout_days['45'], '/');
		$new_sources[]	=	'Affiliate';
	}
	
	// ram cookie
	if( isset($_GET['ram']) ) {
		setcookie('ram', $_GET['ram'], $timeout_days['45'], '/');
		$new_sources[]	=	'RAM Referral';
	}
	
	if( isset($_GET['aff']) || isset($_GET['ram']) ) {
		addSources($new_sources);
	}
	
}

function setSourceCookie($source) {
	global $timeout_days;
	$new_sources	=	array();
	// src and srcs cookie
	if ( $source != '' ) {
		setcookie('source', $source, $timeout_days['45'], '/');
		$new_sources[]	=	$source;
	}
	
	addSources($new_sources);
	
}

function setPhoneCookie($phone) {
	global $timeout_days;
	
	// phone cookie
	if ( $phone != '' ) {
		setcookie('phone', $phone, $timeout_days['45'], '/');
	}
	
}

function addSources($new) {
	global $timeout_days;
	if( !isset($_COOKIE['sources']) ) {
	
		setcookie('sources', implode($new,','), $timeout_days['45'], '/');
		
	} else {
	
		$new_string	=	'';
		
		$src_array	=	explode(',',stripslashes($_COOKIE['sources']));
		foreach($new as $src) {
			if( !in_array($src, $src_array) ) {
				$src_array[]	=	$src;
			}
		}
				
		setcookie('sources', implode(',',$src_array), $timeout_days['45'], '/');
		
	}
	
}

// check page categories for a match
function checkCategories($post, $category) {
	$pageCategory = wp_get_object_terms($post, 'pagecategory');
	
	$return = false;
	$numberCats = 0;	
	foreach ($pageCategory as $cat) {
		if ($cat->slug == $category) {
			$numberCats ++;
		}
	}	
	if( $numberCats > 0){ $return = true; }
	
	return $return;
}

add_action('wp', 'setSourcePhone');

function setSourcePhone() {

global $phone;
global $source;
global $post;
global $my_theme;
global $theme_meta;

$return	=	false;

$blastphone = get_post_meta($post->ID, 'ecpt_uniquephone', true);
$blastsource = get_post_meta($post->ID, 'ecpt_leadsource', true);
$defaultphone = '877-319-7772';

	if (isset($_GET['src'])) {
		// use URL from url query and use switch case to determine source and phone
		$src = $_GET['src'];
		switch ($src) {
			case 'aaa':
			  $phone = "877-689-5123";
			  $return = "Orthotown eNews Banner Feb '14";
			  break;
			case '':
		  	$phone = $defaultphone;
			  $return = "Website - Consult Request";
			  break;
		  default:
		  	$phone = $defaultphone;
			  $return = "Unknown Source";
		 }
				
		// set both 'source' and 'phone' cookies
		setSourceCookie($return);
		setPhoneCookie($phone);
	}else{
		if ($blastsource){
			// use source from '$blastsource'
			$return = $blastsource;
			
			// set cookie 'src' to 'blastsource' value
			setSourceCookie($return);
		}elseif (isset($_COOKIE['source'])){
			// use source from cookie
			$return = $_COOKIE['source'];
		}
		
		if ($blastphone){
			// use phone from '$blastphone'
			$phone = $blastphone;
			
			// set cookie 'phone' to 'blastphone' value
			setPhoneCookie($phone);
		}elseif (isset($_COOKIE['phone'])){
			// use phone from cookie
			$phone = $_COOKIE['phone'];
		}else{
			$phone = $defaultphone;
		}
	}

return $return;	

}