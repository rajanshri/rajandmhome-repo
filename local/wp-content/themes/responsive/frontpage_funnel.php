<?php 
/*
	Template Name: Front Page - Funnel
*/
	
get_header(); ?>
<div id="container">


<!--====== FEATURED SECTION ======-->

<div class="section darkBG featuredSection">
<!-- <div class="center"> -->
	<div class="sectionWrapper">
	
	<!-- Begin Nivo Slider -->
		<div class="sliderWrapperWrapper">
			<div class="slider-wrapper theme-default">
		    <div class="ribbon"></div>
				<div id="slider" class="nivoSlider">
					<img src="<?php echo THEME_IMAGES ?>/banners/fossum_b.png" style="visibility: hidden;" class="featuredTestBG" title="#fossum_content" />
					<img src="<?php echo THEME_IMAGES ?>/banners/riverrock.png" style="visibility: hidden;" class="featuredTestBG" title="#riverrock_content" />
					<img src="<?php echo THEME_IMAGES ?>/banners/sedalia.png" style="visibility: hidden;" class="featuredTestBG" title="#sedalia_content" />
				</div>
				<div id="fossum_content" class="nivo-html-caption">
					<div class="featuredContent fossum">
						<div class="featuredTestInfo">Over 400 new patients.<br />$481,000 in revenue.</div>
						<div class="featuredTestimonial">"This is our biggest referral source."</div>
						<div class="featuredTestBy">- Dr. Richard Fossum</div>
						<a href="<?php echo HOME_URI; ?>/dental-marketing-reviews/fossum-family-dental/" class="button large"><span class="icon icon-stats"></span>See how they did it.</a>
						<div class="featuredLogo">
							<img src="<?php echo THEME_IMAGES ?>/banners/assets/fossum_logo.png" alt="Dr. Richard Fossum | Logo" />
						</div>
					</div>
				</div>
				<div id="riverrock_content" class="nivo-html-caption">
					<div class="featuredContent riverrock">
<!-- 						<div class="featuredTestInfo"><span>Over 100 Calls &<br />50 New Patients</span><br />in Less Than 2 Months!</div> -->
						<div class="featuredTestInfo">Over 100 Calls & 50 New Patients<br />in Less Than 2 Months!</div>
						<div class="featuredTestimonial">"This method has made the greatest impact in the growth of my practice."</div>
						<div class="featuredTestBy">- Dr. Brent Martin</div>
						<a href="<?php echo HOME_URI; ?>/dental-marketing-reviews/river-rock-dental/" class="button large"><span class="icon icon-stats"></span>See how they did it.</a>
						<div class="featuredLogo">
							<img src="<?php echo THEME_IMAGES ?>/banners/assets/riverrock_logo.png" alt="RiverRock Dental | Logo" />
						</div>
					</div>
				</div>
				<div id="sedalia_content" class="nivo-html-caption">
					<div class="featuredContent sedalia">
						<div class="featuredTestInfo">305 New Patients in 1 Year<br />with DentalMarketing.net!</div>
						<div class="featuredTestimonial">"The best marketing tool we've used!"</div>
						<div class="featuredTestBy">- Drs. Aaron & Katie Carrol</div>
						<a href="<?php echo HOME_URI; ?>/dental-marketing-reviews/sedalia-dental/" class="button large"><span class="icon icon-stats"></span>See how they did it.</a>
						<div class="featuredLogo">
							<img src="<?php echo THEME_IMAGES ?>/banners/assets/sedalia_logo.png" alt="Sedalia Dental | Logo" />
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="featuredFiller">
			<div class="featuredContent fossum">
				<div class="featuredTestInfo">Over 400 new patients.<br />$481,000 in revenue.</div>
				<div class="featuredTestimonial">"This is our biggest refexxal source."</div>
				<div class="featuredTestBy">- Dr. Richard Fossum</div>
				<a href="" class="button large"><span class="icon icon-stats"></span>See how they did it.</a>
				<div class="featuredLogo">
					<img src="<?php echo THEME_IMAGES ?>/banners/assets/fossum_logo.png" alt="Dr. Richard Fossum | Logo" />
				</div>
			</div>
		</div>
		<div class="clear"></div>
	</div><!-- /.sectionwrapper-->
<!-- </div> -->
</div>



<!--====== INTRO SECTION ======-->

<div class="section darkGrayBG darkBG introVideoSection">
<div class="center">
	<div class="sectionWrapper">
		<h2 class="sectionHeader">See how our program works.</h2>
			<div class="introVideoLeft">
				<div class="introVideoWrapper">
					<div class="introVideo">
						<iframe src="//www.youtube.com/embed/8qHhHjFH6Fs?rel=0" frameborder="0" allowfullscreen></iframe>
					</div>
				</div>
	
				<div class="clear"></div>	
				
				<div class="introVideoButtons">
					<a href="dental-practice-marketing-pricing/" class="button large first"><span class="icon icon-coin"></span><span class="btnText">Get Pricing Info</span></a>
					<a href="dental-postcard-gallery/" class="button large second"><span class="icon icon-gallery"></span><span class="btnText">View Postcard Gallery</span></a>
				</div>
				<div class="clear"></div>
			</div>
		
		<!-- home form -->
		<div class="contactForm homeForm">
			<?php include 'inc/form_content.php'; ?>
		</div>
		<div class="clear"></div>
		<!-- end home form -->
		
	</div><!-- /.sectionwrapper-->
</div><!-- /.center -->
</div><!-- /.section -->


<!--====== PROCESS SECTION ======-->

<div class="section ourProcessSection">
<div class="center">
	<div class="sectionWrapper">
		<h2 class="sectionHeader">Our Process</h2>
		<?php the_content(); ?>
		<div class="clear"></div>
	</div><!-- /.sectionwrapper-->
</div>
</div>

<div class="section homeTestimonialSection lightGrayBG">
<div class="center">
	<div class="sectionWrapper">
			<div class="testimonials">
			<!-- start Testimonial loop -->
				<?php
				if ( get_query_var('paged') ) { $paged = get_query_var('paged'); }
				elseif ( get_query_var('page') ) { $paged = get_query_var('page'); }
				else { $paged = 1; }
				
				if ($theme_meta == 'DentalMarketing') {
					$args = array(
						'posts_per_page' => 4,
						'post_type' => 'testimonials',
				    'paged' => $paged
					);
				}else{
					$args = array(
						'posts_per_page' => 4,
						'post_type' => 'testimonials',
						'meta_key' => 'ecpt_theme',
				    'meta_value' => $theme_meta,
				    'paged' => $paged
					);
				};
				$result = new WP_Query( $args );
				$tm = 0;
				
				if( $result->have_posts() ) : while ( $result->have_posts() ) : $result->the_post();
				
				$name	=	get_post_meta($post->ID, 'ecpt_tname', true);
				$loc 	=	get_post_meta($post->ID, 'ecpt_tloc', true);
				$exc	=	get_post_meta($post->ID, 'ecpt_texc', true);
				$auth	=	get_post_meta($post->ID, 'ecpt_tauth', true);
				$pos	=	get_post_meta($post->ID, 'ecpt_tpos', true);
				$stars	=	get_post_meta($post->ID, 'ecpt_tstar', true);
				$tm ++;
				?>
					<div class="archiveTestimonial<?php echo $list_class; ?> archive_<?php echo $tm; ?>" id="post-<?php the_ID(); ?>">
					
						<?php if ( has_post_thumbnail() ) { // check if the testimonial has a Post Thumbnail assigned to it. ?>
							
							<a href="<?php the_permalink(); ?>" class="testimonialImg">
								<span class="helper"></span>
								<?php the_post_thumbnail( 'testimonial-list' );	?>
							</a>
						
						<?php
						}; ?>
					
						<div class="testimonialInfo">
					
							<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
						
							<?php the_excerpt(); ?>
							
							<a href="<?php the_permalink(); ?>">Read More &raquo;</a>
							
							<div class="testimonialMeta"><?php echo $auth . ' | ' . $loc; ?></div>
							
						</div>
						
						<div class="clear"></div>
							
					</div>
				<?php
				endwhile;
				endif; 
				//wp_pagenavi();
				wp_reset_query(); ?>	
				<!-- end Testimonial loop -->
			
			</div><!-- /.testimonials -->
			<div class="clear"></div>
		  <div class="outer-center">
		  	<div class="inner-center">
					<a href="/testimonials" class="button large"><span class="icon icon-bubble"></span>View More Testimonials</a>
				</div>
		  </div>
		<div class="clear"></div>
	</div><!-- /.sectionwrapper-->
</div>
</div>


<!--====== GALLERY SECTION ======-->

<!--
<div class="section orangeBG darkBG gallerySection homeGallery">
<div class="center">
	<div class="sectionWrapper">
		<h2 class="sectionHeader">Take a look at some of our work.</h2>
		<div class="galleryThumbs">
	
			<!-- begin Video Post Type loop --	
			<?php
			$args = array(
				'post_type' => 'videos',
				'showposts' => 3
			);
			
			$v_query = new WP_Query( $args );
			if ($v_query->have_posts()) :	while ($v_query->have_posts()) : $v_query->the_post(); 
			
			$youtubeId = get_post_meta($post->ID, 'ecpt_youtubeid', true);
			$colorbox_url = "http://dmhome.tidesdev.net/wp-content/gallery/01070/campbell-dental-front.png";
			?>
			
				<div class="thumbWrapper">
					<div class="thumb1 thumb">
						<img src="http://dmhome.tidesdev.net/wp-content/gallery/01070/campbell-dental-front.png" />
					</div>
				</div>
							
			<?php endwhile; endif; 
			wp_reset_postdata(); ?>
			<div class="clear"></div>
			<!-- end Video Post Type loop --		
			
			
			<?php 
/* 				$shortcode = '[nggallery id=1 template="sliderview" direction_nav="0"]'; */
/*
				$shortcode = '[nggallery id=1 template="home" direction_nav="0"]';
				echo do_shortcode( $shortcode );
*/
			?>
			
			
    <div class="outer-center">
    	<div class="inner-center">
				<a href="/dental-postcard-gallery/" class="button large"><span class="icon icon-gallery"></span>View More Postcards</a>
			</div>
    </div>
		<div class="clear"></div>
			
		</div><!-- /.galleryThumbs --
		<div class="clear"></div>
		
	</div><!-- /.sectionwrapper--
</div><!-- /.center --
</div>
-->


<?php get_footer(); ?>