<?php 
/*
Template Name: Referral Program
*/
get_header(); ?>

<!--======= BEGIN SITE CONTAINER =======-->

<div id="container">

<!--======= BEGIN MAIN CONTENT AREA =======-->

<div id="content">

<!--======= BEGIN COLUMNS =======-->

<div id="columnWrapper">

<!--======= BEGIN MAIN COLUMN =======-->

<div id="mainColumn" class="mainColumnStyle">
	
<!--======= BEGIN MAIN COLUMN CONTENT =======-->
	
	<div class="columnBody">

	<h1>123 Postcards Referral Program</h1>
	
	<p><strong>THANK YOU</strong> for using 123 Postcards. We are dedicated to you and your dental office; anything that we can do to get you more patients is our goal. Below is a token of our appreciation for your excitement to share our product with those in your circle of friends.</p>
	
	<div class="offer">
		<div class="referralBox">
			<h2>Refer 1 Friend</h2>
			<img class="giftcard" src="<?php echo THEME_IMAGES; ?>/gift-card.png" alt="Dental Marketing Referral Giftcard" />
		</div>
		<h2 class="or">OR</h2>
		<div class="referralBox last">
			<h2>Refer 4 Friends</h2>
			<img src="<?php echo THEME_IMAGES; ?>/ipad.png" alt="Dental Marketing Referral iPad2" />
		</div>
	</div>
	<p>Choose your "Thank You!" gift. It's as easy as 1-2-3! You simply inform us of the offices that would want to experience the same great results from 123 Postcards. These offices can be <strong>FRIENDS</strong> or <strong>FAMILY</strong> in the dental field, <strong>CLASSMATES</strong> from dental school, or <strong>DENTISTS</strong> that need more patients. (We'll make sure they're always outside your exclusive market area.)</p>
	
	<form id="referralSend" action="<?php echo THEME_URI; ?>/inc/forms/referral-send.php">
		<div id="formCover"><img src="<?php echo THEME_IMAGES; ?>/roi-ajax-loader.gif" alt="Loading" /></div>
		
			<h2>Step 1: Enter Your Contact Information</h2>
		
			<div class="formLabels">
				<div class="first">First Name:</div>
				<div class="last">Last Name:</div>
				<div class="phoneNumber">Phone Number:</div>
				<div class="email">Email Address:</div>
			</div>
			<div class="formFields">
				<div class="first"><input type="text" class="firstName" name="firstName" value="Enter your first name (required)" /></div>
				<div class="last"><input type="text" class="lastName" name="lastName" value="Enter your last name (required)" /></div>
				<div class="phoneNumber">(<input type="text" class="phoneArea" name="phoneArea" value="XXX" size="3" />) <input type="text" class="phonePrefix" name="phonePrefix" value="XXX" size="3" />-<input type="text" class="phoneExchange" name="phoneExchange" value="XXXX" size="4" /></div>
				<div class="email"><input type="text" class="emailAddress" name="emailAddress" value="Enter your email (required)" /></div>
			</div>
			<div class="clear hr2"></div>
			
			<h2>Step 2: Enter Referral(s) Contact Information</h2>
			<table id="referralsTable" cellspacing="0">
				<thead>
					<tr>
						<td>First Name</td>
						<td>Last Name</td>
						<td>Phone</td>
						<td>Email</td>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td><input type="text" class="refFirstName" name="refFirstName[]" value="First name" /></td>
						<td><input type="text" class="refLastName" name="refLastName[]" value="Last name" /></td>
						<td>(<input type="text" class="refPhoneArea" name="refPhoneArea[]" value="XXX" size="3" />) <input type="text" class="refPhonePrefix" name="refPhonePrefix[]" value="XXX" size="3" />-<input type="text" class="refPhoneExchange" name="refPhoneExchange[]" value="XXXX" size="4" /></td>
						<td><input type="text" class="refEmail" name="refEmail[]" value="Email address" /></td>
					</tr>
					<tr>
						<td><input type="text" class="refFirstName" name="refFirstName[]" value="First name" /></td>
						<td><input type="text" class="refLastName" name="refLastName[]" value="Last name" /></td>
						<td>(<input type="text" class="refPhoneArea" name="refPhoneArea[]" value="XXX" size="3" />) <input type="text" class="refPhonePrefix" name="refPhonePrefix[]" value="XXX" size="3" />-<input type="text" class="refPhoneExchange" name="refPhoneExchange[]" value="XXXX" size="4" /></td>
						<td><input type="text" class="refEmail" name="refEmail[]" value="Email address" /></td>
					</tr>
				</tbody>
			</table>
			
			<p class="alignStyle1"><a href="#" class="g-button ctaButton medium" id="moreReferrals">Add Another Row</a></p>
			
			<div class="clear hr2"></div>
			
			<h2>Step 3: Prove You Are Human</h2>
			
			<div class="formLabels refValidate">
				<div class="validate">Are you a robot? What is <span class="validate1"><script type="text/javascript">document.write(formnumber.x);</script>&nbsp;+&nbsp;</span><span class="validate2"><script type="text/javascript">document.write(formnumber.y);</script>&nbsp;+&nbsp;</span><span class="validate3"><script type="text/javascript">document.write(formnumber.z);</script></span>?
				</div>
			</div>
			<div class="formFields refValidate">
				<div class="validate"><input id="refValidate" name="refValidate" size="3" /><span class="check-status">Enter a value</span></div>
			</div>
			<p class="alignStyle1"><button type="submit" value="submit" class="g-button secondary large">Submit Referrals</button></p>
			<div id="refSendResults"></div>
		</form>
	
	<div class="clear"></div>
	
	<p>All rewards are distributed once one or more referrals are using 123 Postcards at a minimum of 10K mailers per month.</p>
	
	<p>Feel free to follow up with us on the status of your friends that you send to us at 877-319-7772 or by emailing info@123postcards.com.</p>
	
	</div>
	
	<!--======= END MAIN COLUMN CONTENT =======-->

</div>

<!--======= END MAIN COLUMN =======-->

<div class="clear"></div>
</div>

<!--======= END COLUMNS =======-->

<div class="clear"></div>
</div>

<!--======= END MAIN CONTENT AREA =======-->

<script type="text/javascript">
function refSendSubmit( formData, jqForm, options ) {

	if( jQuery('#refValidate').val() != (formnumber.x + formnumber.z) ) {
		alert('Looks like you might be a robot, in which case, my Shaolin Temple style defeat your Monkey style. And if you\'re not a robot, just try the math problem again.');
		jQuery('#refValidate').addClass('check-error');
		return false;
	}

	jQuery('#refSendResults').removeClass('error').addClass('loading').animate({ 'opacity': 'show', 'height': 'show' }).html('Loading...');
	
	var valErrors = new Array();
	
	if( jQuery('#referralSend .practiceName').val() == '' || jQuery('#referralSend .practiceName').val() == 'Enter practice name' ) {
		valErrors.push('Practice Name');
		jQuery('#referralSend .practiceName').addClass('error');
	}
	if( 
		jQuery('#referralSend .firstName').val() == '' || 
		jQuery('#referralSend .firstName').val() == 'Enter first' ||
		jQuery('#referralSend .lastName').val() == '' || 
		jQuery('#referralSend .lastName').val() == 'Enter last'
	) {
		valErrors.push('First and Last Name');
		jQuery('#referralSend .firstName').addClass('error');
		jQuery('#referralSend .lastName').addClass('error');
	}
	if( 
		jQuery('#referralSend .phoneArea').val() == '' || 
		jQuery('#referralSend .phoneArea').val() == 'XXX' ||
		jQuery('#referralSend .phonePrefix').val() == '' ||
		jQuery('#referralSend .phonePrefix').val() == 'XXX' ||
		jQuery('#referralSend .phoneExchange').val() == '' ||
		jQuery('#referralSend .phoneExchange').val() == 'XXXX'
	) {
		valErrors.push('Phone Number');
		jQuery('#referralSend .phoneArea, #referralSend .phonePrefix, #referralSend .phoneExchange').addClass('error');
	}
	if( jQuery('#referralSend .emailAddress').val() == '' || 
		jQuery('#referralSend .emailAddress').val() == 'Enter email address' 
	) {
		valErrors.push('Email Address');
		jQuery('#referralSend .emailAddress').addClass('error');
	}
	var emailAddress = jQuery('#referralSend .emailAddress').val();
	if( !emailReg.test(emailAddress) ) {
		valErrors.push('Email Address (Appears Invalid)');
		jQuery('#referralSend .emailAddress').addClass('error');
	}
	if( valErrors.length > 0 ) {
		var valMessage = 'Please fill out the following required fields: ';
		if( valErrors.length > 1 ) {
			for( i = 0; i < valErrors.length; i++ ) {
				if( i < ( valErrors.length - 1 ) ) {
					valMessage += valErrors[i] + ', ';
				} else {
					valMessage += 'and ' + valErrors[i] + '.';
				}
			}
		} else {
			valMessage += valErrors[0];
		}
		jQuery('#refSendResults').removeClass('loading').addClass('error').html(valMessage).animate({ 'opacity': 'show', 'height': 'show' });
		
		_gaq.push(['_trackPageview', '/virtual/ref-form/validation-errors']);
		
		return false;
	} else {
	}
	
}
function refSendResponse( data ) {

	console.log(data);

	_gaq.push(['_trackPageview', '/virtual/ref-form/success']);
	
	jQuery('#refSendResults').removeClass('loading').addClass('success').html(data.message);
	setTimeout(function(){
		jQuery('#refSendResults').animate({ 'height': 'hide', 'opacity': 'hide' }, 
			function(){
				jQuery(this).removeClass('success').html('Loading...');
			}
		);
	}, 4000);
	
}

jQuery(document).ready(function($){
	// Referrals table
	$('#moreReferrals').click(function(){
		var newRow = $('#referralsTable tbody tr:last-child').clone();
		console.log(newRow);
		newRow.find('.refFirstName').val('First name').attr('rel','First Name');
		newRow.find('.refLastName').val('Last name').attr('rel','Last Name');
		newRow.find('.refPhoneArea,.refPhonePrefix').val('XXX').attr('rel','XXX');
		newRow.find('.refPhoneExchange').val('XXXX').attr('rel','XXXX');
		newRow.find('.refEmail').val('Email address').attr('rel','Email Address');
		newRow.appendTo('#referralsTable tbody');
		
		$('#referralSend').deloClear({
			fieldDef: refDefaults,
			preColor: '#CECECE'
		});
		
		return false;
	});
	var refDefaults = [
		'Enter your first name (required)',
		'Enter your last name (required)',
		'Enter phone number (required)',
		'Enter your email (required)',
		'XXX',
		'XXXX',
		'First name',
		'Last name',
		'Email address'
	];
	$('#referralSend').deloClear({
		fieldDef: refDefaults,
		preColor: '#CECECE'
	});
	// Referral request form from sidebar
	var refOptions = {
		dataType: 'json',
		beforeSubmit: refSendSubmit,
		success: refSendResponse
	};
	
	$('#referralSend').ajaxForm(refOptions);
	checkForm('refValidate');
});
</script>

<?php get_footer(); ?>