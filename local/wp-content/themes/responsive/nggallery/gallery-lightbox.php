<?php 
/**

@description Galleriffic (http://www.twospy.com/galleriffic/) template for Nextgen Gallery Wordpress plugin
@author Kaarel Sikk

Template Page for the gallery overview

Follow variables are useable :

	$gallery     : Contain all about the gallery
	$images      : Contain all images, path, title
	$pagination  : Contain the pagination content

 You can check the content when you insert the tag <?php var_dump($variable) ?>
 If you would like to show the timestamp of the image ,you can use <?php echo $exif['created_timestamp'] ?>
**/
?>
<?php if (!defined ('ABSPATH')) die ('No direct access allowed'); ?>
<?php if (!empty ($gallery)) : ?>

<div class="ngg-galleryoverview" id="<?php echo $gallery->anchor ?>">		

		<?php 
		
		$write = false;
		
		$count = 0;
		foreach ($images as $image) :
			$count++;
		endforeach;
		
		$i = 0;
		foreach ($images as $image) : 
		
		if( $write ) {
		
			$load_url	= admin_url( 'admin-ajax.php' ) . '?action=cbGalImage&front=' . $image_id . '&back=' . $image->pid;
		
			$list_class = '';
			
			?>
			
				<a id="gallery<?php echo $galleryID; ?>" class="galleryLink gallery<?php echo $galleryID; echo $list_class; ?>" href="<?php echo $load_url; ?>" rel="gallery<?php echo $galleryID; ?>" title="<?php echo $alt; ?>">
					<img alt="<?php echo $alt; ?>" src="<?php echo $thumb_url; ?>" width="195" height="113" />
				</a>
				
			<?php
			$i++;
		
		} else {
			$thumb_url	= $image->thumbURL;
			$image_id	= $image->pid;
			$galleryID	= $gallery->ID;
			$alt		= $image->alttext;
		}
		$write = !$write;
		endforeach;
		?>
		
	</a>
 	<div style="clear: both;"></div>
 	
 	<!-- Place this render call where appropriate -->

 	
 	<script type="text/javascript">
		jQuery(document).ready(function($) {
			$('a.gallery<?php echo $gallery->ID; ?>').colorbox({
				photo: false,
				rel: 'gallery<?php echo $gallery->ID; ?>',
				height: 660,
				width: 875,
				top: 25,
				fixed: false,
				arrowKey: false,
				onComplete: function(){
								
								if( favs.hasOwnProperty(this_card_id) ) {
									$('.favoriteCard').attr('checked','checked');
								}
				
								$('.cardflip').cardflip();
								
								$('.favoriteCard').click(function(){
									
									var	input_arr	=	$(this).val().split('|');
									
									var	card_id	=	input_arr[0].toString();
									
									var card_meta	=	{	
											name:	input_arr[1],
											front:	input_arr[2],
											back:	input_arr[3]
									};
									
									if( $(this).is(':checked') ) {
										favs[ card_id ]	=	card_meta;
									} else {
										delete favs[ card_id ];
									}
									
									$.cookie('fav_cards', favs, { expires: 14, path: '/' });
									
								});
							}
			});
		});
	</script>

	<!-- Pagination -->
 	<?php echo $pagination ?>
 	
</div>

<?php endif; ?>