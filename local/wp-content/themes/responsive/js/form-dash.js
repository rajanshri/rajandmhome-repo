function funnelSendSubmit( formData, jqForm, options ) {

	console.log(formData);
	console.log(jqForm);
	console.log(options);
	
	// Honeypot spam filter
	if( jQuery('.funnelContact input.sticky').is(':checked') ) {
		alert('Are you a robot? I think so... ');
		return false;
	}
	
	jQuery('#funnelSendResults').removeClass('error').addClass('loading').animate({ 'opacity': 'show', 'height': 'show' }).html('Loading...');
	
	var valErrors = new Array();
	
	if( jQuery('.funnelContact .practiceName').val() == '' || jQuery('.funnelContact .practiceName').val() == 'Enter practice name' ) {
		valErrors.push('Practice Name');
		jQuery('.funnelContact .practiceName').addClass('error');
	}
	if( 
		jQuery('.funnelContact .firstName').val() == '' || 
		jQuery('.funnelContact .firstName').val() == 'Enter first' ||
		jQuery('.funnelContact .lastName').val() == '' || 
		jQuery('.funnelContact .lastName').val() == 'and last name'
	) {
		valErrors.push('First and Last Name');
		jQuery('.funnelContact .firstName').addClass('error');
		jQuery('.funnelContact .lastName').addClass('error');
	}
	if( 
		jQuery('.funnelContact .phoneArea').val() == '' || 
		jQuery('.funnelContact .phoneArea').val() == 'XXX' ||
		jQuery('.funnelContact .phonePrefix').val() == '' ||
		jQuery('.funnelContact .phonePrefix').val() == 'XXX' ||
		jQuery('.funnelContact .phoneExchange').val() == '' ||
		jQuery('.funnelContact .phoneExchange').val() == 'XXXX'
	) {
		valErrors.push('Phone Number');
		jQuery('.funnelContact .phoneArea, .funnelContact .phonePrefix, .funnelContact .phoneExchange').addClass('error');
	}
	if( jQuery('.funnelContact .phoneArea').val().length != 3 ||
		jQuery('.funnelContact .phonePrefix').val().length != 3 ||
		jQuery('.funnelContact .phoneExchange').val().length != 4 
	
	) {
		valErrors.push('Phone Number (Check Length)');
		jQuery('.funnelContact .phoneArea, .funnelContact .phonePrefix, .funnelContact .phoneExchange').addClass('error');
	}
	if( jQuery('.funnelContact .emailAddress').val() == '' || 
		jQuery('.funnelContact .emailAddress').val() == 'Enter email address'
	) {
		valErrors.push('Email Address');
		jQuery('.funnelContact .emailAddress').addClass('error');
	}
	var emailAddress = jQuery('.funnelContact .emailAddress').val();
	if( !emailReg.test(emailAddress) ) {
		valErrors.push('Email Address (Appears Invalid)');
		jQuery('.funnelContact .emailAddress').addClass('error');
	}
	if( jQuery('.funnelContact input.reserve').is(':checked') ) {
		if( jQuery('.funnelContact input.street').val() == '' ||
			jQuery('.funnelContact input.street').val() == 'Enter street address'
		) {
			valErrors.push('Street Address');
			jQuery('.funnelContact input.street').addClass('error');
		}
		if( jQuery('.funnelContact input.city').val() == '' ||
			jQuery('.funnelContact input.city').val() == 'Enter city'
		) {
			valErrors.push('City');
			jQuery('.funnelContact input.city').addClass('error');
		}
		if( jQuery('.funnelContact input.zip').val() == '' ||
			jQuery('.funnelContact input.zip').val() == 'Enter zip'
		) {
			valErrors.push('Zip');
			jQuery('.funnelContact input.zip').addClass('error');
		}
	}
	
	if( valErrors.length > 0 ) {
		var valMessage = 'Please fill out the following required fields: ';
		if( valErrors.length > 1 ) {
			for( i = 0; i < valErrors.length; i++ ) {
				if( i < ( valErrors.length - 1 ) ) {
					valMessage += valErrors[i] + ', ';
				} else {
					valMessage += 'and ' + valErrors[i] + '.';
				}
			}
		} else {
			valMessage += valErrors[0];
		}
		jQuery('#funnelSendResults').removeClass('loading').addClass('error').html(valMessage);
		
		_gaq.push(['_trackPageview', '/virtual/lead-form/validation-errors/'+ section +'/funnel']);
		
		return false;
	} else {
	}
	
}

function funnelSendResponse( data ) {

	console.info('data',data);
	
	var section	=	jQuery('#interest').val()

	_gaq.push(['_trackPageview', '/virtual/lead-form/success/'+ section +'/dashboard']);
	
	jQuery('#funnelSendResults').removeClass('loading').addClass(data.status).html(data.message);
	setTimeout(function(){
		jQuery('#funnelSendResults').animate({ 'height': 'hide', 'opacity': 'hide' }, 
			function(){
				jQuery(this).removeClass('success').html('Loading...');
			}
		);
	}, 4000);
	
}

jQuery(document).ready(function($){
	
	// Free Consultation Form Defaults
	var funnelDefaults = [
		'Enter practice name',
		'Enter first',
		'and last name',
		'XXX',
		'XXXX',
		'Enter email address',
		'Enter Comments/Questions (optional)',
		'Enter street address',
		'Enter street address (cont.)',
		'Enter suite number',
		'Enter city',
		'Enter zip'
	];
	$('.funnelContact').deloClear({
		fieldDef: funnelDefaults,
		preColor: '#CECECE'
	});
	
	// Free Consultation request form from sidebar
	var funnelOptions = {
		dataType: 'json',
		beforeSubmit: funnelSendSubmit,
		success: funnelSendResponse
	};
	
	$('.funnelContact').ajaxForm(funnelOptions);
	
	$('input.reserve').change(function(){
		if($(this).is(':checked')) {
			$(this).parents('p').siblings('.reserve-show').slideDown();
		} else {
			$(this).parents('p').siblings('.reserve-show').slideUp();
		}
	});
	
});