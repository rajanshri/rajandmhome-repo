jQuery.cookie.json = true;


var favs	=	jQuery.cookie('fav_cards');
if( favs === undefined ) {
	console.log(favs);
	favs	=	{};
	jQuery.cookie('fav_cards', favs, { expires: 14, path: '/' });
}

function uppercaseFirst(s) {
	return s.charAt(0).toUpperCase() + s.slice(1);
}

function trackNavClick(location) {
	
	var action	=	uppercaseFirst(section);
	
	_gaq.push(['_trackEvent', location +' Click', action, jQuery(this).html() ]);
	
}

function initPromotion() {
	
	// Make it snow
	snowStorm.start();
	
	// Make it show
	jQuery('#specialPromotionHero').fadeIn(1000,promotionBanner);
	
	function promotionBanner() {
		jQuery('#specialPromotionBanner').slideDown(1000,paintArrow);
	}
	
	function paintArrow() {
		jQuery('.paintArrow').fadeIn(500);
	};
	
}

//Resize elements to window size on page load and resize
$(window).load(function() {
	var a = $('.galleryLink');
	var width = a.width();
	var height = width*0.58585858586;
	a.css('height', height);
});

$(window).resize(function() {
	var a = $('.galleryLink');
	var width = a.width();
	var height = width*0.58585858586;
	a.css('height', height);
});


/*
$('.cardflip').load(function() {
	var div = $('.cardflip');
	var width = div.width();
	var height = width*0.18585858586;
	div.css('height', height);
});
*/

// Contact Form Spam Filter

var formx = Math.floor(Math.random()*5 + 1);
var formy = Math.floor(Math.random()*5 + 1);
var formz = Math.floor(Math.random()*5 + 1);

var formnumber = {
	x: formx,
	y: formy,
	z: formz
}

var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;

function checkForm(input,brief){

	var checkOpts = {
		input: input
	};
	checkOpts.brief = (typeof brief === 'undefined') ? false : brief;
	
	if( checkOpts.brief == true ){
		var errorMessage = 'Incorrect.';
		var successMessage = 'Bingo!';
	} else {
		var errorMessage = 'Incorrect. Please try again.';
		var successMessage = 'Bingo! You\'re a math whiz!';
	}

	jQuery('#' + input).blur(function(){
		var inputVal = jQuery('#' + input).val();
		if( inputVal != (formnumber.x + formnumber.z) ) {
			jQuery(this).removeClass('check-success').addClass('check-error');
			jQuery(this).siblings('.check-status').removeClass('check-status-success').html(errorMessage).addClass('check-status-error');	
		} else {
			jQuery(this).removeClass('check-error').addClass('check-success');
			jQuery(this).siblings('.check-status').removeClass('check-status-error').html(successMessage).addClass('check-status-success');
		}
	});
	jQuery('#' + input).focus(function(){
		origVal = jQuery(this).val();
		jQuery(this).val('');
	});
}

function asMoney(str){
	newVal = '$' + addCommas(str);
	
	return newVal;
}

function clone(obj) {
    // Handle the 3 simple types, and null or undefined
    if (null == obj || "object" != typeof obj) return obj;

    // Handle Date
    if (obj instanceof Date) {
        var copy = new Date();
        copy.setTime(obj.getTime());
        return copy;
    }

    // Handle Object
    if (obj instanceof Object) {
        var copy = {};
        for (var attr in obj) {
            if (obj.hasOwnProperty(attr)) copy[attr] = clone(obj[attr]);
        }
        return copy;
    }

    throw new Error("Unable to copy obj! Its type isn't supported.");
}

// Add commas to dollar amounts
function addCommas(nStr){
	nStr += '';
	x = nStr.split('.');
	x1 = x[0];
	x2 = x.length > 1 ? '.' + x[1] : '';
	var rgx = /(\d+)(\d{3})/;
	while (rgx.test(x1)) {
		x1 = x1.replace(rgx, '$1' + ',' + '$2');
	}
	return x1 + x2;
}

jQuery(document).ready(function($){

/* PROMOTION ENDED
	// Only do the snowing on the homepage and in the videos section
	if( $('body').hasClass('page-id-1605') || $('body').hasClass('page-id-1520') || $('body').hasClass('page-id-2251') ) {
		setTimeout(initPromotion,1500);
	}
	else if( $('body').hasClass('theme-videos') && !$('body').hasClass('page-id-1605') ) {
		$('#specialPromotionHero').show();
	}
*/
	
	
// Nav functions
// Dropdown for main and top nav
$('#navigation .menu > li > .sub-menu, #topNav .menu > li > .sub-menu').parent('li').hover(
	function(){ // mouse over
		
		basetime = 50;
		perchild = 50;
		childcount = $(this).find('.sub-menu').children().length;
		
		changetime = basetime + (childcount * perchild);
		
		if( changetime > 250 ) {
			changetime = 250;
		};
		
		$(this).children('.sub-menu').stop(true, true).animate({ 'opacity': 'show', 'height': 'show' }, changetime );
	}, 
	function(){ // mouse out
		$(this).children('.sub-menu').stop(true, true).animate({ 'opacity': 'hide'  }, 200 );
	}
);

// Third level for main
$('#navigation .sub-menu > li > .sub-menu').parent('li').hover(
	function(){ // mouse over
		
		$(this).children('.sub-menu > li > .sub-menu').stop(true, true).animate({ 'opacity': 'show', 'width': 'show' }, 100 );
	}, 
	function(){ // mouse out
		$(this).children('.sub-menu > li > .sub-menu').stop(true, true).animate({ 'opacity': 'hide'  }, 100 );
	}
);

var dropdowns = $('#navigation .menu > li > .sub-menu');

$('.sub-menu').parent('li').addClass('has-children');

// Footer
$('#siteLinks .menu>li:odd').addClass('secondRow');
$('#siteLinks .menu>li:even').addClass('firstRow');

// Top
$('#topNav .menu>li:first-child').addClass('first');
$('#topNav .menu>li:last-child').addClass('last');
	
$('input,textarea').focus(function(){
	$(this).addClass('focused');
});

$('input,textarea').focusout(function(){
	$(this).removeClass('focused');
	if( $(this).hasClass('error') ) {
		$(this).removeClass('error');
	};
	if( 
		$(this).hasClass('phoneArea') || 
		$(this).hasClass('phonePrefix') || 
		$(this).hasClass('phoneExchange') 
	) {
		$(this).siblings('input').removeClass('error');
	}
});

$('tbody tr:first-child').addClass('firstRow');
$('tbody tr:last-child').addClass('lastRow');
$('tbody tr:even').addClass('even');
$('tbody tr:odd').addClass('odd');
$('td:first-child').addClass('firstCol');
$('td:last-child').addClass('lastCol');
$('td').each(function(){
	var the_pos = $(this).getNonColSpanIndex();
	the_pos	= the_pos + 1;
	var the_class = 'col' + the_pos;
	
	$(this).addClass(the_class);
});
$('tr').each(function(){
	var the_pos = $(this).parent('tbody').children().index($(this));
	if( the_pos != -1 ) {
		the_pos	= the_pos + 1;
		var the_class = 'row' + the_pos;
		
		$(this).addClass(the_class);
	}
});

// mobileMenuWrapper toggle on mobileMenuBtn click
$('.mobileMenuBtn').click(function() {
	$('.mobileMenuWrapper').show();
});
$('.mobileMenu .close').click(function() {
	$('.mobileMenuWrapper').hide();
});

// Phone numbers auto-tab on length
$('.phoneArea,.phonePrefix,.phoneExchange').numeric();

$( '.ignore-lazy-load' ).each( function() {
	$( this ).find( 'img[data-lazy-src]' ).each( function() {
		lazy_load_image( this );
	});		
});

$('#navWrap a').click(trackNavClick('Main Nav Click'));

$('#topNav a').click(trackNavClick('Top Nav Click'));

$('a.toggleFull').click(function() {
    $(this).parents('.testimonial').children('.testimonialContent').toggle();
});

/*
$('div.showMore').click(function(){
	$(this).parent('.testimonial').children('.testimonialContent').toggle();
	$(this).toggle(function() {
    $(this).html('Show Less...');
	}, function() {
	  $(this).html('Show More2...');
	});
});
*/

$('.noLink').click(function(e) {
	e.stopPropogation();
	e.preventDefault();
});


// Nivo Slider
if($('#slider').length >0 ){
$(window).load(function() {
  $('#slider').nivoSlider({
    effect: 'random',               // Specify sets like: 'fold,fade,sliceDown'
    animSpeed: 500,                 // Slide transition speed
    pauseTime: 6000,                // How long each slide will show
    pauseOnHover: true,             // Stop animation while hovering
    manualAdvance: false,           // Force manual transitions
    afterLoad: function(){
    	$('.featuredFiller').hide();
    	$('.featuredSection .sectionWrapper').css('background','none');
/* 			$('.nivoSlider').hide(); */
    }
	});
});

	//Resize .nivo-caption to window size on page load
	$(window).load(function() {
		var windowSize = $(window).width();
		var sliderWidth = $('#slider').width();
		$('.nivo-caption').css('width', windowSize);
		$('.nivo-directionNav').css('width', windowSize);
		$('.nivo-controlNav').css('width', windowSize);
		$('.nivo-controlNav-wrapper').css('width', sliderWidth);
	});
	
	//Resize .nivo-caption to window size on window resize
	$(window).resize(function() {
		var windowSize = $(window).width();
		$('.nivo-caption').css('width', windowSize);
		$('.nivo-directionNav').css('width', windowSize);
		$('.nivo-controlNav').css('width', windowSize);
	});
	
};
});
