function formSendSubmit( formData, jqForm, options ) {

	console.log(formData);
	console.log(jqForm);
	console.log(options);
	
	// Honeypot spam filter
	if( jQuery('input.sticky').is(':checked') ) {
		alert('Are you a robot? I think so... ');
		return false;
	}
	
	jQuery('.response-output').removeClass('error').addClass('loading').animate({ 'opacity': 'show', 'height': 'show' }).html('Loading...');
	
	var valErrors = new Array();
	
	if( jQuery('.practiceName').val() == '' || jQuery('.practiceName').val() == 'Enter practice name' ) {
		valErrors.push('Practice Name');
		jQuery('.practiceName').addClass('error');
	}
	if( 
		jQuery('.firstName').val() == '' || 
		jQuery('.firstName').val() == 'Enter first' ||
		jQuery('.lastName').val() == '' || 
		jQuery('.lastName').val() == 'and last name'
	) {
		valErrors.push('First and Last Name');
		jQuery('.firstName').addClass('error');
		jQuery('.lastName').addClass('error');
	}
	if( 
		jQuery('.phoneArea').val() == '' || 
		jQuery('.phoneArea').val() == 'XXX' ||
		jQuery('.phonePrefix').val() == '' ||
		jQuery('.phonePrefix').val() == 'XXX' ||
		jQuery('.phoneExchange').val() == '' ||
		jQuery('.phoneExchange').val() == 'XXXX'
	) {
		valErrors.push('Phone Number');
		jQuery('.phoneArea, .phonePrefix, .phoneExchange').addClass('error');
	}
	if( jQuery('.phoneArea').val().length != 3 ||
		jQuery('.phonePrefix').val().length != 3 ||
		jQuery('.phoneExchange').val().length != 4 
	
	) {
		valErrors.push('Phone Number (Check Length)');
		jQuery('.phoneArea, .phonePrefix, .phoneExchange').addClass('error');
	}
	if( jQuery('.emailAddress').val() == '' || 
		jQuery('.emailAddress').val() == 'Enter email address'
	) {
		valErrors.push('Email Address');
		jQuery('.emailAddress').addClass('error');
	}
	var emailAddress = jQuery('.emailAddress').val();
	if( !emailReg.test(emailAddress) ) {
		valErrors.push('Email Address (Appears Invalid)');
		jQuery('.emailAddress').addClass('error');
	}
	if( jQuery('input.reserve').is(':checked') ) {
		if( jQuery('input.street').val() == '' ||
			jQuery('input.street').val() == 'Enter street address'
		) {
			valErrors.push('Street Address');
			jQuery('input.street').addClass('error');
		}
		if( jQuery('input.city').val() == '' ||
			jQuery('input.city').val() == 'Enter city'
		) {
			valErrors.push('City');
			jQuery('input.city').addClass('error');
		}
		if( jQuery('input.zip').val() == '' ||
			jQuery('input.zip').val() == 'Enter zip'
		) {
			valErrors.push('Zip');
			jQuery('input.zip').addClass('error');
		}
	}
	
	if( valErrors.length > 0 ) {
		var valMessage = 'Please fill out the following required fields: ';
		if( valErrors.length > 1 ) {
			for( i = 0; i < valErrors.length; i++ ) {
				if( i < ( valErrors.length - 1 ) ) {
					valMessage += valErrors[i] + ', ';
				} else {
					valMessage += 'and ' + valErrors[i] + '.';
				}
			}
		} else {
			valMessage += valErrors[0];
		}
		jQuery('.response-output').removeClass('loading').addClass('error').html(valMessage);
		console.info('errorMessage',valMessage);
	
		var section	=	jQuery('#interest').val()
		var location	=	jQuery('#formLocation').html()
		var fullurl = window.location.pathname
		var slug = "/" + fullurl.split("/").slice(-2)[0]
		
		//alert('/virtual/lead-form/validation-errors/'+ section +'/'+ location + slug);
		
		_gaq.push(['_trackPageview', '/virtual/lead-form/validation-errors/'+ section +'/'+ location + slug]);
		
		return false;
	} else {
	}
	
}

function formSendResponse( data ) {

	console.info('data',data);
	
	var section	=	jQuery('#interest').val()
	var location	=	jQuery('#formLocation').html()
	var fullurl = window.location.pathname
	var slug = "/" + fullurl.split("/").slice(-2)[0]
	
	//alert('/virtual/lead-form/success/'+ section +'/'+ location + slug);

	_gaq.push(['_trackPageview', '/virtual/lead-form/success/'+ section +'/'+ location + slug]);
	
	jQuery('.response-output').removeClass('loading').addClass(data.status).html(data.message);
	setTimeout(function(){
		jQuery('.response-output').animate({ 'height': 'hide', 'opacity': 'hide' }, 
			function(){
				jQuery(this).removeClass('success').html('Loading...');
			}
		);
	}, 4000);
	
}

jQuery(document).ready(function($){
	
	// Free Consultation Form Defaults
	var formDefaults = [
		'Enter practice name',
		'Enter first',
		'and last name',
		'XXX',
		'XXXX',
		'Enter email address',
		'Enter Comments/Questions (optional)',
		'Enter street address',
		'Enter street address (cont.)',
		'Enter suite number',
		'Enter city',
		'Enter zip'
	];
	$('.contactForm').deloClear({
		fieldDef: formDefaults,
		preColor: '#CECECE'
	});
	
	// Free Consultation request
	var formOptions = {
		dataType: 'json',
		beforeSubmit: formSendSubmit,
		success: formSendResponse
	};
	
	$('.contactForm').ajaxForm(formOptions);
	
	$('input.reserve').change(function(){
		if($(this).is(':checked')) {
			$(this).parents('p').siblings('.reserve-show').slideDown();
		} else {
			$(this).parents('p').siblings('.reserve-show').slideUp();
		}
	});
	
});