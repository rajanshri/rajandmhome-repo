function traySendSubmit( formData, jqForm, options ) {
	
	jQuery('#traySendResults').removeClass('error').addClass('loading').animate({ 'opacity': 'show', 'height': 'show' }).html('Loading...');
	
	var valErrors = new Array();
	
	if( jQuery('.trayContact .practiceName').val() == '' || jQuery('.trayContact .practiceName').val() == 'Enter practice name' ) {
		valErrors.push('Practice Name');
		jQuery('.trayContact .practiceName').addClass('error');
	}
	if( 
		jQuery('.trayContact .firstName').val() == '' || 
		jQuery('.trayContact .firstName').val() == 'Enter first' ||
		jQuery('.trayContact .lastName').val() == '' || 
		jQuery('.trayContact .lastName').val() == 'Enter last'
	) {
		valErrors.push('First and Last Name');
		jQuery('.trayContact .firstName').addClass('error');
		jQuery('.trayContact .lastName').addClass('error');
	}
	if( 
		jQuery('.trayContact .phoneArea').val() == '' || 
		jQuery('.trayContact .phoneArea').val() == 'XXX' ||
		jQuery('.trayContact .phonePrefix').val() == '' ||
		jQuery('.trayContact .phonePrefix').val() == 'XXX' ||
		jQuery('.trayContact .phoneExchange').val() == '' ||
		jQuery('.trayContact .phoneExchange').val() == 'XXXX'
	) {
		valErrors.push('Phone Number');
		jQuery('.trayContact .phoneArea, .trayContact .phonePrefix, .trayContact .phoneExchange').addClass('error');
	}
	if( jQuery('.trayContact .phoneArea').val().length != 3 ||
		jQuery('.trayContact .phonePrefix').val().length != 3 ||
		jQuery('.trayContact .phoneExchange').val().length != 4 
	
	) {
		valErrors.push('Phone Number (Check Length)');
		jQuery('.trayContact .phoneArea, .trayContact .phonePrefix, .trayContact .phoneExchange').addClass('error');
	}
	if( jQuery('.trayContact .emailAddress').val() == '' || 
		jQuery('.trayContact .emailAddress').val() == 'Enter email address'
	) {
		valErrors.push('Email Address');
		jQuery('.trayContact .emailAddress').addClass('error');
	}
	var emailAddress = jQuery('.trayContact .emailAddress').val();
	if( !emailReg.test(emailAddress) ) {
		valErrors.push('Email Address (Appears Invalid)');
		jQuery('.trayContact .emailAddress').addClass('error');
	}
	if( jQuery('.trayContact input.reserve').is(':checked') ) {
		if( jQuery('.trayContact input.street').val() == '' ||
			jQuery('.trayContact input.street').val() == 'Enter street address'
		) {
			valErrors.push('Street Address');
			jQuery('.trayContact input.street').addClass('error');
		}
		if( jQuery('.trayContact input.city').val() == '' ||
			jQuery('.trayContact input.city').val() == 'Enter city'
		) {
			valErrors.push('City');
			jQuery('.trayContact input.city').addClass('error');
		}
		if( jQuery('.trayContact input.zip').val() == '' ||
			jQuery('.trayContact input.zip').val() == 'Enter zip'
		) {
			valErrors.push('Zip');
			jQuery('.trayContact input.zip').addClass('error');
		}
	}
	
	if( valErrors.length > 0 ) {
		var valMessage = 'Please fill out the following required fields: ';
		if( valErrors.length > 1 ) {
			for( i = 0; i < valErrors.length; i++ ) {
				if( i < ( valErrors.length - 1 ) ) {
					valMessage += valErrors[i] + ', ';
				} else {
					valMessage += 'and ' + valErrors[i] + '.';
				}
			}
		} else {
			valMessage += valErrors[0];
		}
		jQuery('#traySendResults').removeClass('loading').addClass('error').html(valMessage);
		console.info('errorMessage',valMessage);
		
		_gaq.push(['_trackPageview', '/virtual/lead-form/validation-errors/postcards/gallery-lightbox-tray']);
		
		return false;
	} else {
		jQuery('.traySubmit input.submit').attr('disabled','disabled').addClass('disabled');
		console.log('Gallery tray form submitted successfully');
	}
	
}

function traySendResponse( data ) {

	console.info('response data',data);

	jQuery('.sideContact input.submit').removeAttr('disabled').removeClass('disabled');

	_gaq.push(['_trackPageview', '/virtual/lead-form/success/postcards/gallery-lightbox-tray']);
	
	jQuery('.response-output').removeClass('loading').addClass(data.status).html(data.message);
	setTimeout(function(){
		jQuery('.response-output').animate({ 'height': 'hide', 'opacity': 'hide' }, 
			function(){
				jQuery(this).removeClass('success').html('Loading...');
			}
		);
	}, 4000);
	
}
jQuery(document).ready(function($){
	// Free Consultation Form Defaults
	var trayDefaults = [
		'Enter practice name',
		'Enter first',
		'and last name',
		'XXX',
		'XXXX',
		'Enter email address',
		'Enter Comments/Questions (optional)',
		'Enter street address',
		'Enter street address (cont.)',
		'Enter suite number',
		'Enter city',
		'Enter zip'
	];
	$('.trayContact').deloClear({
		fieldDef: trayDefaults,
		preColor: '#CECECE'
	});
	
	// Free Consultaion request form from design page lightbox
	var trayOptions = {
		dataType: 'json',
		beforeSubmit: traySendSubmit,
		success: traySendResponse
	};
	
	$('.trayForm form').ajaxForm(trayOptions);

});