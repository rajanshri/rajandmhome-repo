<?php get_header(); ?>

<div class="container">

<?php
$headerblock = get_post_meta($post->ID, 'ecpt_headerblock', true);
if ($headerblock != '') : ?>
<div class="subHeader">
	<div class="center sub">
		<?php echo $headerblock; ?>
	</div><!-- /center -->
</div>
<?php endif; ?>

<?php
	
	$args = array(
		'post_type' => 'jobs'
	);
	
	$result = new WP_Query( $args );
	
	if( $result->have_posts() ) : ?>
	<div class="section">
		<div class="center sub">
			<div class="sectionWrapper">
				<div class="mainColumn">
	
					<table class="jobsTable" cellspacing="0">
						<thead>
							<tr>
								<td>Title</td>
								<td>Department</td>
								<td>Status</td>
								<td>Salary</td>
								<td>Location</td>
							</tr>
						</thead>
						<tbody>
					
					<?php while ( $result->have_posts() ) : $result->the_post();
					
					$headline = get_post_meta($post->ID, 'ecpt_headline', true);
					$sal_low = get_post_meta($post->ID, 'ecpt_sal_low', true);
					$sal_high = get_post_meta($post->ID, 'ecpt_sal_high', true);
					$location = get_post_meta($post->ID, 'ecpt_location', true);
					$loc = get_post_meta($post->ID, 'ecpt_loc', true);
					
					?>
							<tr>
								<td><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></td>
								<td class="brownLink"><?php echo get_the_term_list( $post->ID, 'field'); ?></td>
								<td class="brownLink"><?php echo get_the_term_list( $post->ID, 'jobstatus'); ?></td>
								<td><?php echo $sal_low . ' - ' . $sal_high; ?></td>								
								<td><?php echo $loc; ?></td>
							</tr>
					<?php endwhile; ?>
					
						</tbody>
					</table>
				</div><!-- /.mainColumn -->
				<div class="clear"></div>
			</div><!-- /.sectionWrapper -->
		</div><!-- /center -->
	</div><!-- /.section -->
	
	<?php
	wp_pagenavi();
	
	else: 
	?>
	
	<p>Sorry, there are no current positions at this time. Please feel free to submit your resume to <a href="mailto:jobs@dentalmarketing.net">jobs@dentalmarketing.net</a> for future consideration.</p>
	
<?php endif; ?>

<!--====== CTA SECTION ======-->
<?php include('inc/sections/cta_section.php'); ?>

<!------ FORM ------>
<div class="section subFormSection lightGrayBG">
	<div class="center">
		<div class="sectionWrapper">
			<!-- sub form -->
			<div class="contactForm subForm">
				<?php include 'inc/form.php'; ?>
			</div>
			<div class="clear"></div>
			<!-- end sub form -->
		</div>
	</div><!-- /center -->
</div>

<div class="clear"></div>

<?php get_footer(); ?>

