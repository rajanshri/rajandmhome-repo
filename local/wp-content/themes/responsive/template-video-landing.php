<?php
/*
Template Name:Video landing page
*/
get_header();

?>
<div class="container">

<?php
$headerblock = get_post_meta($post->ID, 'ecpt_headerblock', true);
if ($headerblock != '') : ?>
<div class="subHeader">
	<div class="center sub">
		<?php echo $headerblock; ?>
	</div><!-- /center -->
</div>

<?php endif; ?>

<div class="section">
	<div class="center sub">
		<div class="sectionWrapper">
			<div class="mainColumn">
			
			<?php	if (have_posts()) :	while (have_posts()) : the_post(); ?>
				<?php if($post->post_content != "") : ?>
					<?php the_content(); ?>
				<?php endif; ?>
			<?php endwhile; endif; wp_reset_postdata(); ?>

			<div class="productsContent">
            
            	<!--Your content will go here-->
				
					<h2>Engage, Inform,and Inspire Your Patients </h2><!--Headeing-->
                    
                    <iframe width="560" height="315" src="//www.youtube.com/embed/RgN9l1zYVJk?rel=0" frameborder="0" allowfullscreen></iframe>
                
                
				<!--End of conntent-->
				
			</div>
		
			</div><!-- /.mainColumn -->
			<div class="clear"></div>
		</div><!-- /.sectionWrapper -->
	</div><!-- /center -->
</div>

<div id="specialPromotionHero"></div>

<?php
get_footer();
?>