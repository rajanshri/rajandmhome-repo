<?php
/*
Template Name: Front Page - Sub
*/
get_header();

global $theme_meta;
global $my_theme;
global $buttons;

if ($theme_meta == 'Postcards'){
	$headerCTA = 'brownorange';
}elseif($theme_meta == 'Videos'){
	$headerCTA = 'video-banner';
};

$themeClass = "theme" . str_replace(' ', '', $theme_meta);
?>

<!--======= BEGIN SITE CONTAINER =======-->

<div id="container" class="fullWidth subHome <?php echo $themeClass; ?>">

	<!--======= BEGIN MAIN CONTENT AREA =======-->

 <div id="content">
    <div id="content-newBanner">
			<div class="subSlider">
       <div class="main-quote">
	       <img src="<?php echo THEME_IMAGES; ?>/guarantee/<?php echo $headerCTA; ?>.png" />
       </div>
       
      <?php if( count($buttons) > 0 ) : ?>
      <div class="bannerBoxWrapper">
	      <?php foreach( $buttons as $key => $values ) : ?>
		      <div class="bannerBox">
						<a href="<?php echo $values['link'] ?>">
							<div class="bBoxImg"><img src="<?php echo $values['image'] ?>" alt="<?php echo $values['alt'] ?>" /></div>
							<h2 class="bBoxHeading"><?php echo $values['title'] ?></h2>
							<div class="bBoxSub"><?php echo $values['content'] ?></div>
						</a>
		      </div>
	      <?php endforeach; ?>
      </div>
      <?php endif; ?>
	  
	  
		</div><!-- /.subSlider -->
  </div><!-- /#content-newBanner -->
  <div id="specialPromotionHero"></div>
        <!--======= (END BANNER TRY) CONTENT AREA =======-->
        
	<!-- Testimonials -->
	<?php 

$GLOBALS['box1title'] = get_post_meta($post->ID, 'ecpt_homebox1title', true);
	$args = array(
		'post_type' => 'testimonials',
		'orderby' => 'rand',
		'posts_per_page' => '1',
		'meta_key' => 'ecpt_theme',
    'meta_value' => $theme_meta
	);
	
	$result = new WP_Query( $args );
	
	if( $result->have_posts() ) : $result->the_post();
		
		$name	=	get_post_meta($post->ID, 'ecpt_tname', true);
		$exc	=	get_post_meta($post->ID, 'ecpt_texc', true);
		if( strlen( $exc ) > 250 ) {
			$exc = myTruncate( $exc, 250, ' ');
		}
		$auth	=	get_post_meta($post->ID, 'ecpt_tauth', true);
		$pos	=	get_post_meta($post->ID, 'ecpt_tpos', true);
		//$date	=	date( 'F j, Y', get_post_meta($post->ID, 'ecpt_tdate', true) );
		$stars	=	get_post_meta($post->ID, 'ecpt_tstar', true);
		
		?>
				
		<div class="homeTestimonial">
			<?php 
			if ( has_post_thumbnail() ) { // check if the testimonial has a Post Thumbnail assigned to it.
				?><a href="<?php the_permalink(); ?> " class="subHomeTestImg"><?php
				the_post_thumbnail( 'home-testimonial' );
				?></a><?php
			} 
			?>
			<div class="testimonialContent">
				<?php echo $exc; ?>
				<div class="testimonialMeta">
					<span class="testimonialName">- <?php echo $name; ?>, <?php echo $auth; ?></span>
					<a class="testLink" href="<?php echo HOME_URI; echo $my_theme['testimonials'] ?>">More Testimonials &raquo;</a>
				</div>
			</div>
			<div class="clear"></div>	
		</div>
			
	<?php
	
	endif; 
	wp_reset_postdata();
	
	?>
	<!-- end Testimonials -->
	
	
	<!--======= BEGIN COLUMNS =======-->
	
	<div id="columnWrapper">
		
		<!--======= BEGIN HOME BLOCKS =======-->
		
		<div id="subHomeBlocks">
			
			<div class="subHomeBlock formBlock">
				
				<form id="funnelContact" class="funnelContact contactForm" action="<?php echo HOME_URI; ?>/wp-admin/admin-ajax.php">
					<h2 class="formTitle">Request a Free Consultation</h2>
					<div class="formBody">
						<div class="subHomeBlockContent">
							<p class="practice"><label for="practice">Practice Name*</label>:<br /> 
								<input type="text" name="practice" class="text practiceName" value="Enter practice name" /></p>
							<p class="name"><label for="firstName">First and Last Name*</label>:<br />
								<input type="text" name="firstName" class="text firstName" value="Enter first"  />
								<input type="text" name="lastName" class="text lastName" value="and last name"  /></p>
							<p class="phone"><label for="phone">Phone Number*</label>:<br /> 
								<input type="text" class="phoneArea" name="phoneArea" value="XXX" size="3" />  <input type="text" class="phonePrefix" name="phonePrefix" value="XXX" size="3" />  <input type="text" class="phoneExchange" name="phoneExchange" value="XXXX" size="4" /></p>
							<p class="sticky"><label for="sticky">Do you want more information?</label><br />
								<input type="checkbox" class="sticky" name="sticky" /></p>
							<p class="email"><label>Email Address*</label>:<br /> 
								<input type="text" name="emailAddress" class="text emailAddress" value="Enter email address" /></p>
							<p class="practice"><label for="comments">Comments/Questions:</label><br />
								<textarea name="comments" cols="34" rows="2">Enter Comments/Questions (optional)</textarea></p>
								<?php if ($theme_meta == 'Postcards') :?>
									<p class="reserve"><label for="reserve">Check area availability: <input type="checkbox" name="reserve" class="reserve" value="checked" /></label></p>
									<div class="reserve-show" style="display:none;">
									
										<p class="street"><label for="street">Street Address:</label><br />
											<input type="text" name="street" class="text street " value="Enter street address" /></p>
											
										<p class="street2"><label for="street2">Street Address (cont.):</label><br />
											<input type="text" name="street2" class="text street2" value="Enter street address (cont.)" /></p>
											
										<p class="suite"><label for="suite">Suite:</label><br />
											<input type="text" name="suite" class="text suite" value="Enter suite number" /></p>
											
										<p class="city"><label for="city">City, State, Zip:</label><br />
											<input type="text" name="city" class="text city" value="Enter city" />
										</p>
										<p class="stateZip">
											<?php statesDropdown(); ?>
											<input type="text" name="zip" class="text zip" value="Enter zip" />
										</p>
										
									</div>
								<?php endif; ?>
							<input type="hidden" id="interest" id="interest" name="interest" value="<?php echo $my_theme['interest']; ?>" />
							<input type="hidden" name="action" value="sendContactForm" />
							<input id="source" type="hidden" name="source" value="<?php echo $source; ?>" />
						</div>
						<div id="subHomeSendResults" class="submitWrapper">
							<input type="submit" value="Submit" class="g-button large secondary" />
							<div class="response-output"></div>
						</div>
					</div>
				</form>
				
			</div>
			
				<?php
					$title_1 = get_post_meta($post->ID, 'ecpt_homebox1title', true);
					$image_1 = get_post_meta($post->ID, 'ecpt_homebox1img', true);
					$alt_1 = get_post_meta($post->ID, 'ecpt_homebox1alt', true);
					$content_1 = get_post_meta($post->ID, 'ecpt_homebox1content', true);
					$button_1 = get_post_meta($post->ID, 'ecpt_homebox1btntxt', true);
					$link_1 = get_post_meta($post->ID, 'ecpt_homebox1btndst', true);
					
					$title_2 = get_post_meta($post->ID, 'ecpt_homebox2title', true);
					$image_2 = get_post_meta($post->ID, 'ecpt_homebox2img', true);
					$alt_2 = get_post_meta($post->ID, 'ecpt_homebox2alt', true);
					$content_2 = get_post_meta($post->ID, 'ecpt_homebox2content', true);
					$button_2 = get_post_meta($post->ID, 'ecpt_homebox2btntxt', true);
					$link_2 = get_post_meta($post->ID, 'ecpt_homebox2btndst', true);
				?>
				
			<div class="subHomeBlock">
				<h2><?php echo $title_1; ?></h2>
				<div class="subHomeBlockContent">
					<a href="<?php echo $link_1; ?>" class="homeLinkImg">
						<img src="<?php echo $image_1; ?>" alt="<?php echo $alt_1; ?>" />
					</a>
					<p><?php echo $content_1; ?></p>
				</div>
				<a href="<?php echo $link_1; ?>" class="g-button large secondary"><?php echo $button_1; ?> &raquo;</a>
			</div>
			
			<div class="subHomeBlock">
				<h2><?php echo $title_2; ?></h2>
				<div class="subHomeBlockContent">
					<a href="<?php echo $link_2; ?>" class="homeLinkImg">
						<img src="<?php echo $image_2; ?>" alt="<?php echo $alt_2; ?>" />
					</a>
					<p><?php echo $content_2; ?></p>
				</div>
				<a href="<?php echo $link_2; ?>" class="g-button large secondary"><?php echo $button_2; ?> &raquo;</a>
			</div>
		
		<div class="clear"></div>
		</div>
		
		<!--======= END HOME BLOCKS =======-->
	
	</div>
	
	<!--======= END COLUMNS =======-->
	
	<div class="clear"></div>
	</div>

<!--======= END MAIN CONTENT AREA =======-->

<div id="formLocation" style="display:none;">sub-home</div>
<script type='text/javascript' src='<?php echo THEME_JS; ?>/form.js'></script>

<?php get_footer(); ?>