<?php get_header(); ?>

<!--======= BEGIN SITE CONTAINER =======-->

<div id="container">

<!--======= BEGIN MAIN CONTENT AREA =======-->

<div id="content">

<!--======= BEGIN COLUMNS =======-->

<div id="columnWrapper">

<!--======= BEGIN MAIN COLUMN =======-->

<div id="mainColumn" class="mainColumnStyle">
	
<!--======= BEGIN MAIN COLUMN CONTENT =======-->
	
	<div class="columnBody">

	<?php if (have_posts()) : 
	
	$headline 	= get_post_meta($post->ID, 'ecpt_headline', true);
	$sal_low 	= get_post_meta($post->ID, 'ecpt_sal_low', true);
	$sal_high	= get_post_meta($post->ID, 'ecpt_sal_high', true);
	$headline	= str_replace('<p>', '<p>HEADLINE: ', $headline );
	$location	= get_post_meta($post->ID, 'ecpt_location', true);
	$loc		= get_post_meta($post->ID, 'ecpt_loc', true);
	$minqual	= get_post_meta($post->ID, 'ecpt_min_qual', true);
	$prefqual	= get_post_meta($post->ID, 'ecpt_pref_qual', true);	
	$benefits	= get_post_meta($post->ID, 'ecpt_benefits', true);	
	
	while (have_posts()) : the_post(); ?>
		<div class="pr" id="post-<?php the_ID(); ?>">
			
			<h1><?php the_title(); ?></h1>
			
			<h3 class="dateMetaSingle"><?php the_time('F j, Y'); ?></h3>	
			<div class="titleSingle"></div>					
				<table class="jobsTableSingle" cellspacing="0">
					<thead>
						<tr>
							<td>Department:</td>
							<td>Status:</td>
							<td>Salary:</td>
							<td>Location:</td>
							<td>Contact:</td>	
						</tr>
					</thead>
					<tbody>
						<tr>
							<td><?php echo strip_tags(get_the_term_list( $post->ID, 'field') ); ?></td>
							<td><?php echo strip_tags(get_the_term_list( $post->ID, 'jobstatus') ); ?></td>
							<td><?php echo $sal_low . ' - ' . $sal_high; ?></td>
							<td><?php echo $loc; ?></td>
							<td>jobs@dentalmarketing.net</td>					
						</tr>
					</tbody>
				</table>
			
			<h2>Job Description</h2>
			<?php the_content(); ?>
			
			<h2>Minimum Qualifications</h2>
			
			<ul>
				<?php foreach($minqual as $value) {
					echo '<li>' . $value . '</li>';
				} ?>
			</ul>
			
			<h2>Preferred Qualifications</h2>
			
			<ul>
				<?php foreach($prefqual as $value) {
					echo '<li>' . $value . '</li>';
				} ?>
			</ul>
			
			<h2>Position Benefits</h2>
			
			<ul>
				<?php foreach($benefits as $value) {
					echo '<li>' . $value . '</li>';
				} ?>
			</ul>
			
			<h2>About DentalMarketing.net</h2>

			<p>DentalMarketing.net is a Utah entity located in Heber City, Utah. DentalMarketing.net is a full spectrum dental marketing company providing offline dental marketing services to dentists in the United States of America. We create and deliver high quality direct mail marketing campaigns, very quickly, while competing to be the low price industry leader.</p>
			
			<?php
			$jobLink = get_post_meta($post->ID, 'ecpt_applicationlink', true);
			if ($jobLink == '') {
				$jobLink = 'https://tidesarerising.bamboohr.com/jobs/';
			}
			?>
			<a class="g-button ctaButton large" title="Apply for This Position" href="<?php echo $jobLink; ?>" target="_blank">Apply for This Position</a> 
			
		</div>
	<?php endwhile; endif; ?>	
	
	</div>
	
	<!--======= END MAIN COLUMN CONTENT =======-->

</div>

<!--======= END MAIN COLUMN =======-->

<!--======= BEGIN SIDEBAR CONTENT AREA =======-->
<?php get_sidebar(); ?>
<!--======= END SIDEBAR CONTENT AREA =======-->

<div class="clear"></div>
</div>

<!--======= END COLUMNS =======-->

<div class="clear"></div>
</div>

<!--======= END MAIN CONTENT AREA =======-->

<?php get_footer(); ?>