<?php get_header(); ?>

<div class="container">

<?php
$headerblock = get_post_meta($post->ID, 'ecpt_headerblock', true);
if ($headerblock != '') : ?>
<div class="subHeader">
	<div class="center sub">
		<?php echo $headerblock; ?>
	</div><!-- /center -->
</div>

<?php endif; ?>

<?php	if (have_posts()) :	while (have_posts()) : the_post(); ?>
	<?php if($post->post_content != "") : ?>
		<div class="section">
			<div class="center sub">
				<div class="sectionWrapper">
					<div class="mainColumn">
						<?php if ( has_post_thumbnail() ) { // check if the page has a featured image assigned to it.
							the_post_thumbnail();
						} ?>
						<?php the_content(); ?>
			
					</div><!-- /.mainColumn -->
					<div class="clear"></div>
				</div><!-- /.sectionWrapper -->
			</div><!-- /center -->
		</div><!-- /.section -->
	<?php endif; ?>
<?php endwhile; endif; wp_reset_postdata(); ?>

<div id="specialPromotionHero"></div>

<?php
	// Include post-content template to match category, if applicable
	if( checkCategories($post->ID, 'gallery')) {
		include 'inc/video_gallery.php';
	} elseif( checkCategories($post->ID, 'contact')) {
		include 'inc/contact.php';
	};
?>
<!--====== TESTIMONIALS ======-->
<div class="testimonialFull">
	<div class="testimonialText">
		<div class="icon-openq quotes"></div>
		<div class="betweenQuotes"><?php the_content(); ?></div>
		<div class="icon-closeq quotes"></div>
	</div>
	<div class="testimonialInfo">
		<h3 class="testimonialMeta">
			- <?php echo $auth; 
					if ($pos) {
						echo ', ' . $pos;
					};
				?>
		</h3>
	</div>
</div>

<!--====== CTA SECTION ======-->
<?php include('inc/sections/cta_section.php'); ?>

<!------ FORM ------>
<?php include('inc/sections/sub_form.php'); ?>


<div class="clear"></div>

<?php get_footer(); ?>

