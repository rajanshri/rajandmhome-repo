<?php 
/*
Template Name: Testimonials
*/
get_header(); 
global $theme_meta;?>

<div class="container">

<?php
$headerblock = get_post_meta($post->ID, 'ecpt_headerblock', true);
if ($headerblock != '') : ?>
<div class="subHeader">
	<div class="center sub">
		<?php echo $headerblock; ?>
	</div><!-- /center -->
</div>
<?php endif; ?>

<div class="section">
	<div class="center sub wide">
		<div class="sectionWrapper">
			<div class="mainColumn">
				<?php
				if ( get_query_var('paged') ) { $paged = get_query_var('paged'); }
				elseif ( get_query_var('page') ) { $paged = get_query_var('page'); }
				else { $paged = 1; }
				
				if ($theme_meta == 'DentalMarketing') {
					$args = array(
						'posts_per_page' => -1,
						'post_type' => 'testimonials',
				    'paged' => $paged
					);
				}else{
					$args = array(
						'posts_per_page' => -1,
						'post_type' => 'testimonials',
						'meta_key' => 'ecpt_theme',
				    'meta_value' => $theme_meta,
				    'paged' => $paged
					);
				};
				$result = new WP_Query( $args );
				
				if( $result->have_posts() ) : while ( $result->have_posts() ) : $result->the_post();
				
				$name	=	get_post_meta($post->ID, 'ecpt_tname', true);
				$loc 	=	get_post_meta($post->ID, 'ecpt_tloc', true);
				$exc	=	get_post_meta($post->ID, 'ecpt_texc', true);
				$auth	=	get_post_meta($post->ID, 'ecpt_tauth', true);
				$pos	=	get_post_meta($post->ID, 'ecpt_tpos', true);
				$is_cs = get_post_meta($post->ID, 'ecpt_iscs', true);
				?>
					<div class="archiveTestimonial <?php if ($is_cs) { echo 'lightGrayBG'; }; ?>" id="post-<?php the_ID(); ?>">
					
						<?php if ( has_post_thumbnail() ) { // check if the testimonial has a Post Thumbnail assigned to it. ?>
							
							<a href="<?php the_permalink(); ?>" class="testimonialImg">
								<span class="helper"></span>
								<?php the_post_thumbnail( 'testimonial-list' );	?>
							</a>
						
						<?php
						}; ?>
					
						<div class="testimonialInfo">
					
							<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
						
							<?php the_excerpt(); ?>
							
							<a href="<?php the_permalink(); ?>">Read More &raquo;</a>
							<div class="testimonialMeta"><?php echo $auth . ' | ' . $loc; ?></div>
							<?php if ($is_cs) { ?>
								<div class="csLinkSpacer">&nbsp;</div>
								<div class="csLinkWrapper">
									<a href="<?php the_permalink(); ?>" class="csLink">View Case Study</a>
								</div>
							<?php } ?>
							
						</div>
						
						<div class="clear"></div>
							
					</div>
				<?php
				
				endwhile;
				endif; 
				//wp_pagenavi();
				wp_reset_query(); ?>	
			
			</div><!-- /.mainColumn -->
			<div class="clear"></div>
		</div><!-- /.sectionWrapper -->
	</div><!-- /center -->
</div><!-- /.section -->

<div id="specialPromotionHero"></div>

<?php
	// Include post-content template to match category, if applicable
	if( checkCategories($post->ID, 'gallery')) {
		include 'inc/video_gallery.php';
	} elseif( checkCategories($post->ID, 'contact')) {
		include 'inc/contact.php';
	};
?>

<!--====== CTA SECTION ======-->
<?php include('inc/sections/cta_section.php'); ?>

<!------ FORM ------>
<?php include('inc/sections/sub_form.php'); ?>

<div class="clear"></div>

<?php get_footer(); ?>

