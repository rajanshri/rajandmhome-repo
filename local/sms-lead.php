<?php

$leadIn = $_REQUEST['Body'];

preg_match('/[\\w\\.\\-+=*_]*@[\\w\\.\\-+=*_]*/', $leadIn, $matches);

$email		= $matches[0];

$event['phone']	= $_REQUEST['To'];

$phone		= $_REQUEST['From'];
$fullName	= str_replace( $email, '', $leadIn );
$firstName	= $fullName;
$lastName	= $fullName;
$practice	= $fullName;

// Get details about the event like name, message, sms
require_once( 'wp-content/themes/invisible-hand/inc/Twilio_lib/event-details.php' );

// Send emails, create contact in SF
require_once( 'wp-content/themes/invisible-hand/inc/forms/sms-lead-send.php' );


// set XML header for Twilio
header('HTTP/1.1 200 OK');
header('content-type: text/xml');
echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
?>
<Response>
	<Sms><?php echo $event['sms']; ?></Sms>
	<?php if( $event['smssecond'] != '' ) { ?>
	<Sms><?php echo $event['smssecond']; ?></Sms>
	<?php } ?>
</Response>